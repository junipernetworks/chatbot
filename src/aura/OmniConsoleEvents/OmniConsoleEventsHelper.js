({
  sendNotification: function(body, title, shouldRequireInteraction) {
    var options = {
      body: body,
      icon: 'https://chatbotdev-juniper.cs4.force.com/liveagent/resource/NF_ccare_bot/img/cta-icon.png',
//      actions: [
//        {
//          action: 'coffee-action',
//          title: 'Coffee',
//          icon: 'https://chatbotdev-juniper.cs4.force.com/liveagent/resource/NF_ccare_bot/img/cta-icon.png'
//        }
//      ],
      requireInteraction: shouldRequireInteraction
    };
    var n = new Notification(title, options);
  }
});