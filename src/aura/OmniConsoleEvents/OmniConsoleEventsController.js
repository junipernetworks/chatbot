({
  doInit: function(component, event, helper) {
    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
      alert("This browser does not support desktop notification");
    }

    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === "granted") {
    }
    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === "default") {
      console.log("Notification Permission Default");
    }

    // Otherwise, we need to ask the user for permission
    else if (Notification.permission === "denied") {
      Notification.requestPermission().then(function(permission) {
        // If the user accepts, let's create a notification
        if (permission === "granted") {
          console.log("Notification Permission Granted");
        }
      });
    }
  },

  /* Link to current chat*/
  gotoURL : function (component, event, helper) {
    var recordId = component.get("v.recordId");
    var urlEvent = $A.get("e.force:force:navigateToSObject");
    navEvt.setParams({
      "recordId": recordId,
      "slideDevName": "detail"
    });
    urlEvent.fire();
  },

  /* Omni toolkit Api */
  onLoginSuccess: function(component, event, helper) {
//    console.log('login')
//    var statusId = event.getParam("statusId");
//    helper.sendNotification('You are successfully Login in Omni ', 'Omni Login Success', true);
  },

  onStatusChanged: function(component, event, helper) {
//    var statusId = event.getParam("statusId");
//    var channels = event.getParam("channels");
//    var statusName = event.getParam("statusName");
//    var statusApiName = event.getParam("statusApiName");
//    helper.sendNotification('User Changed Status to : ' + statusName, 'Omni Status Changed', true);
  },

  onLogout: function(component, event, helper) {
//    helper.sendNotification('You are successfully Logout from Omni' , 'Omni Logout', true);
  },

  onWorkAssigned: function(component, event, helper) {
    var workItemId = event.getParam("workItemId");
    var workId = event.getParam("workId");
    helper.sendNotification('A new chat is available!', 'Incoming Chat', true);
  },

  onWorkAccepted: function(component, event, helper) {
//    var workAcceptTimestamp = new Date();
//    component.set("v.workAcceptTimestamp", workAcceptTimestamp);
//    var workItemId = event.getParam("workItemId");
//    var workId = event.getParam("workId");
//    helper.sendNotification('This workitem has been accepted : ' + workItemId, 'Omni Work Accepted', true);
  },

  onWorkDeclined: function(component, event, helper) {
//    var workItemId = event.getParam("workItemId");
//    var workId = event.getParam("workId");
//    helper.sendNotification('This workitem has been declined : ' + workItemId, 'Omni Work Declined', true);
  },

  onWorkClosed: function(component, event, helper) {
//    var workItemId = event.getParam("workItemId");
//    var workId = event.getParam("workId");
//    helper.sendNotification( 'This workitem has been closed : ' +workItemId+ ' Omni Work Closed', true);
  },

  onWorkloadChanged: function(component, event, helper) {
//    var configuredCapacity = event.getParam("configuredCapacity");
//    var previousWorkload = event.getParam("previousWorkload");
//    var newWorkload = event.getParam("newWorkload");
//    helper.sendNotification('Total Capacity : ' + configuredCapacity +' \nPrevious Workload : ' + previousWorkload+' \nNew Workload : ' + newWorkload, 'Omni WorkLoad Changed', true);
  },
  /* END Omni toolkit Api */
  onRecordIdChange: function (component, event, helper) {
    var newRecordId = component.get("v.recordId");
    //helper.logger('onRecordIdChange > newRecordId', newRecordId, 'Info');
    },
    
   onAgentSend: function(component, event, helper) {
    //if we can determine the tab has focus AND the tab does not have focus, fire a notification
    /*if(typeof document.hasFocus === 'function' && document.hasFocus() === false){
        var onAgentSendTimestamp = new Date;
        var workAcceptAgentSendTimeDiff = (onAgentSendTimestamp.getTime() - component.get('v.workAcceptTimestamp').getTime()) /1000;
        component.set("v.agentFirstReponseTime", new Date());
		var recordId = event.getParam("recordId");
        var content = event.getParam("content");
        var name = event.getParam("name");
        var type = event.getParam("type");
        var timestamp = event.getParam("timestamp");
        helper.sendNotification('' + content, 'New Chat Message', true);
    }  */
  },

  onNewMessage: function(component, event, helper) {
    //if we can determine the tab has focus AND the tab does not have focus, fire a notification
    if(typeof document.hasFocus === 'function' && document.hasFocus() === false){
        var recordId = event.getParam("recordId");
        var content = event.getParam("content");
        var name = event.getParam("name");
        var type = event.getParam("type");
        var timestamp = event.getParam("timestamp");
        helper.sendNotification('' + content, 'New Chat Message', true);
    }
  }
});