({
	doInit : function(component, event, helper) {
        var action = component.get("c.getchatButtonName");
        action.setParams({
            botTransferbutton :'CCARE'
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.chatButtonId",response.getReturnValue());
            }
            else if (state === "ERROR") {
                console.log('errors ' +JSON.stringyfy(response.getError()));
            }
        });

        $A.enqueueAction(action);
    }
})