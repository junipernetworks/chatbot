({
    
    handleLogin : function(component, event, helper) {
        window.location.replace('/support/login.page?bot=open');
    },
    resetPassword : function(component, event, helper) {
        window.open("https://userregistration.juniper.net/resetpassword");
    },
    changePassword : function(component, event, helper) {
        window.open("https://userregistration.juniper.net/changepassword");
    },
    createAccount : function(component, event, helper) {
        window.open("https://userregistration.juniper.net/");
    }
})