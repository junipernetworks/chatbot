({
	doInit : function(component, event, helper) {
        var chatButtonId = component.get("v.chatButtonId");
		var prechatFieldComponents = component.find("prechatAPI").getPrechatFields();

        //grab the first name and capitalize the first letter, tried using regex /\w\S*/g but Lightning JS mangled it
		var firstName = (prechatFieldComponents[0].value || (userInfo && userInfo.firstName) || '');
		if(firstName.length > 1){
		    firstName =  firstName.charAt(0).toUpperCase() + firstName.substr(1).toLowerCase()
        }

        component.set("v.userFirstName", firstName);
        
        var action = component.get("c.getButtonAvailability");
        action.setParams({
            botDeployment : 'CCARE',
            botTransferbutton :'CCARE'
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.agentNotAvailable",response.getReturnValue());
            }
            else if (state === "ERROR") {
                console.log('errors ' +JSON.stringyfy(response.getError()));
            }
        });

        $A.enqueueAction(action);
    },
    
    navigateToKnowlegdeBase : function(component, event, helper) {
         window.open("https://kb.juniper.net/InfoCenter/index?page=home");
    },
    navigateToLibrary : function(component, event, helper) {
         window.open("https://www.juniper.net/documentation/");
    },
     navigateToJNetCommunity : function(component, event, helper) {
         window.open("https://forums.juniper.net/");
    },
     navigateToLabsInfo : function(component, event, helper) {
         window.open("https://www.juniper.net/us/en/try/");
    }, 
    phoneSupport : function(component, event, helper) {
         window.open("https://support.juniper.net/support/requesting-support/");
    },
    chatWithAgent : function(component, event, helper) {
        helper.onStartButtonClick(component, event, helper);
    }
})