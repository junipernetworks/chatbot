({
	/**
	 * Map of pre-chat field label to pre-chat field name (can be found in Setup)
	 */
	fieldLabelToName: {
        "First Name": "FirstName",
        "Last Name": "LastName",
        "DirectLiveAgent" : true
    },
    /**
	 * Event which fires the function to start a chat request (by accessing the chat API component)
	 * @param component - The component for this state.
	 */
	onStartButtonClick: function(component) {
        
		var prechatFieldComponents = component.find("prechatAPI").getPrechatFields();
		if(component.find("prechatAPI").validateFields(prechatFieldComponents).valid) {
            component.find("prechatAPI").startChat(prechatFieldComponents);
        } else {
            console.warn("Prechat fields did not pass validation!");
        }
	}
})