({
	doInit : function(component, event, helper) {
        var action = component.get("c.getButtonAvailability");
        action.setParams({
            botDeployment : 'CCARE',
            botTransferbutton :'CCARE'
            
        });
		
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.agentNotAvailable",response.getReturnValue());
            }
            else if (state === "ERROR") {
                console.log('errors ' +JSON.stringyfy(response.getError()));
            }
        });

        $A.enqueueAction(action);
    },
    navigateToKnowlegdeBase : function(component, event, helper) {
         window.open("https://kb.juniper.net/InfoCenter/index?page=home");
    },
    navigateToLibrary : function(component, event, helper) {
         window.open("https://www.juniper.net/documentation/");
    },
     createCase : function(component, event, helper) {
         window.open("https://casemanager.juniper.net/casemanager/");
    },
     PhoneSupport : function(component, event, helper) {
         window.open("https://support.juniper.net/support/requesting-support/");
    }, 
    chatWithAgent : function(component, event, helper) {
        helper.onStartButtonClick(component);
       
    }
})