<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Telco Cloud</label>
    <protected>false</protected>
    <values>
        <field>Suite_Url__c</field>
        <value xsi:type="xsd:string">https://www.juniper.net/us/en/solutions/nfv/telco-cloud/</value>
    </values>
</CustomMetadata>
