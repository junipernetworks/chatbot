<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Open - Awaiting Customer Input</label>
    <protected>false</protected>
    <values>
        <field>IsRMA__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsSR__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Status_from_API__c</field>
        <value xsi:type="xsd:string">Open - Awaiting Customer Input</value>
    </values>
    <values>
        <field>Status_in_JDA__c</field>
        <value xsi:type="xsd:string">Awaiting Customer Input</value>
    </values>
</CustomMetadata>
