<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>social_hello_direct 3</label>
    <protected>false</protected>
    <values>
        <field>Bot_Name__c</field>
        <value xsi:type="xsd:string">JuniBot</value>
    </values>
    <values>
        <field>Intent__c</field>
        <value xsi:type="xsd:string">social_hello_direct</value>
    </values>
    <values>
        <field>Response__c</field>
        <value xsi:type="xsd:string">Hey! Let me know what I can do to help.</value>
    </values>
</CustomMetadata>
