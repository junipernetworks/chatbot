<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>QueryRMAList</label>
    <protected>false</protected>
    <values>
        <field>ebcm__Bot_Name__c</field>
        <value xsi:type="xsd:string">JuniBot</value>
    </values>
    <values>
        <field>ebcm__Dialog_Request_Payload__c</field>
        <value xsi:type="xsd:string">{&quot;home&quot;:1,&quot;cards&quot;:[&quot;&lt;div class = &apos;carousel&apos;&gt;&lt;h1&gt;RMA # R200239781&lt;/h1&gt;&lt;h2&gt;Delivery Address&lt;/h2&gt;&lt;div class=&apos;slds-text-align_left slds-m-left_small&apos;&gt;&lt;p&gt;&lt;span class =&apos;capping&apos;&gt;&lt;b&gt;Company: &lt;/b&gt;ORANGE UI NORMANDIE&lt;br&gt;&lt;b&gt;Address: &lt;/b&gt;29 RUE DE L&apos;AVENIR&lt;br&gt;&lt;b&gt;City: &lt;/b&gt;CARPIQUET&lt;br&gt;&lt;b&gt;Country: &lt;/b&gt;France&lt;br&gt;&lt;b&gt;Postal/Zip Code: &lt;/b&gt;14650&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;&lt;div&gt;&lt;a href=&apos;https://my.juniper.net/#dashboard/rmadetails/R200239781&apos; target=&apos;_blank&apos; class =&apos;slds-button slds-button_neutral carousel-detail-button&apos;&gt;View full RMA details&lt;/a&gt;&lt;/div&gt;&quot;,&quot;&lt;div class = &apos;carousel&apos;&gt;&lt;h1&gt;RMA # R200239781&lt;/h1&gt;&lt;h2&gt;Defective Part Information&lt;/h2&gt;&lt;div class=&apos;slds-text-align_left slds-m-left_small&apos;&gt;&lt;p&gt;&lt;span class =&apos;capping&apos;&gt;&lt;b&gt;Type #: &lt;/b&gt;Advance Exchange&lt;br&gt;&lt;b&gt;Item #: &lt;/b&gt;100&lt;br&gt;&lt;b&gt;Serial #: &lt;/b&gt;CABG1832&lt;br&gt;&lt;b&gt;Product ID: &lt;/b&gt;MPC4E-3D-2CGE-8XGE&lt;br&gt;&lt;b&gt;Status: &lt;/b&gt;Awaiting Return&lt;br&gt;&lt;b&gt;Date Received: &lt;/b&gt;N/A&lt;/span&gt;&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;&lt;div&gt;&lt;a href=&apos;https://my.juniper.net/#dashboard/rmadetails/R200239781&apos; target=&apos;_blank&apos; class =&apos;slds-button slds-button_neutral carousel-detail-button &apos;&gt;View full RMA details&lt;/a&gt;&lt;/div&gt;&quot;,&quot;&lt;div class = &apos;carousel&apos;&gt;&lt;h1&gt;RMA # R200239781&lt;/h1&gt;&lt;h2&gt;Replacement Part Information&lt;/h2&gt;&lt;div class=&apos;slds-text-align_left slds-m-left_small&apos;&gt;&lt;p&gt;&lt;span class =&apos;capping&apos;&gt;&lt;b&gt;Item #: &lt;/b&gt;110&lt;br&gt;&lt;b&gt;Serial #: &lt;/b&gt;CAEJ1514&lt;br&gt;&lt;b&gt;Product ID: &lt;/b&gt;MPC4E-3D-2CGE-8XGE&lt;br&gt;&lt;b&gt;Status: &lt;/b&gt;Delivered - Closed&lt;br&gt;&lt;b&gt;Carrier: &lt;/b&gt;UPS&lt;br&gt;&lt;b&gt;Tracking #: &lt;/b&gt;&lt;a href=&apos;http://wwwapps.ups.com/WebTracking/track?track=yes&amp;trackNums=1ZR263W30403715687&apos; target=&apos;_blank&apos;&gt;1ZR263W30403715687&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;&lt;div&gt;&lt;a href=&apos;https://my.juniper.net/#dashboard/rmadetails/R200239781&apos; target=&apos;_blank&apos; class =&apos;slds-button slds-button_neutral carousel-detail-button&apos;&gt;View full RMA details&lt;/a&gt;&lt;/div&gt;&quot;]}</value>
    </values>
    <values>
        <field>ebcm__Type__c</field>
        <value xsi:type="xsd:string">Carousel</value>
    </values>
</CustomMetadata>
