<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>bot_fallback_first_noMatch_noContact 9</label>
    <protected>false</protected>
    <values>
        <field>Bot_Name__c</field>
        <value xsi:type="xsd:string">JuniBot</value>
    </values>
    <values>
        <field>Intent__c</field>
        <value xsi:type="xsd:string">bot_fallback_first_noMatch_noContact</value>
    </values>
    <values>
        <field>Response__c</field>
        <value xsi:type="xsd:string">Can you ask that again a different way?</value>
    </values>
</CustomMetadata>
