<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>bot_preLeadCreationInvalidEmail_end</label>
    <protected>false</protected>
    <values>
        <field>ebotdata__Dialog_Api_Name__c</field>
        <value xsi:type="xsd:string">bot_preLeadCreationInvalidEmail_end</value>
    </values>
    <values>
        <field>ebotdata__Escalation_Reason__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ebotdata__Metric__c</field>
        <value xsi:type="xsd:string">Escalation End</value>
    </values>
    <values>
        <field>ebotdata__Topic_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ebotdata__Type__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
