<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>WelcomeLetterForm</label>
    <protected>false</protected>
    <values>
        <field>ebcm__Bot_Name__c</field>
        <value xsi:type="xsd:string">JuniBot</value>
    </values>
    <values>
        <field>ebcm__Dialog_Request_Payload__c</field>
        <value xsi:type="xsd:string">{
	&quot;title&quot;: &quot;Enter your Contract ID or Sales Order Number&quot;,
	&quot;subtitle&quot;: &quot;&quot;,
	&quot;inputs&quot;: [{
		&quot;sfIcon&quot;: &quot;standard:people&quot;,
		&quot;label&quot;: &quot;Contract ID/Sales Order #&quot;,
        &quot;pattern&quot; :&quot;^1\\d{7}$|^6\\d{7}$&quot;,
		&quot;type&quot;: &quot;text&quot;,
		&quot;placeholder&quot;: &quot;10000000&quot;,
		&quot;value&quot;: &quot;&quot;,
		&quot;imgURL&quot;: &quot;&quot;,
		&quot;isRequired&quot;: &quot;true&quot;,
		&quot;fApi&quot;: &quot;CIDorSON__c&quot;,
        &quot;errMessages&quot;:{
                &quot;badInput&quot;:&quot;Bad input error message&quot;,
&quot;patternMismatch&quot;:&quot;this is pattern mismatch &gt; Phone&quot;
            }
	}]
}</value>
    </values>
    <values>
        <field>ebcm__Type__c</field>
        <value xsi:type="xsd:string">Form</value>
    </values>
</CustomMetadata>
