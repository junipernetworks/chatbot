<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>QuerySRList</label>
    <protected>false</protected>
    <values>
        <field>ebcm__Bot_Name__c</field>
        <value xsi:type="xsd:string">JuniBot</value>
    </values>
    <values>
        <field>ebcm__Dialog_Request_Payload__c</field>
        <value xsi:type="xsd:string">{&quot;home&quot;:1,&quot;cards&quot;:[&quot;&lt;div class = &apos;carousel&apos;&gt;&lt;h1&gt;Case 2020-0130-T-3012&lt;/h1&gt;&lt;h2&gt;Details&lt;/h2&gt;&lt;div class=&apos;slds-text-align_left slds-m-left_small&apos;&gt;&lt;p&gt;&lt;span class =&apos;capping&apos;&gt;&lt;b&gt;Synopsis: &lt;/b&gt;Agent&lt;br&gt;&lt;b&gt;Serial #/SSRN: &lt;/b&gt;null&lt;br&gt;&lt;b&gt;Priority: &lt;/b&gt;P3 - Medium&lt;br&gt;&lt;b&gt;Status: &lt;/b&gt;Open&lt;br&gt;&lt;/div&gt;&lt;/div&gt;&lt;div&gt;&lt;a href=&apos;https://my.juniper.net/#dashboard/products/2020-0130-T-3012&apos; target=&apos;_blank&apos; class =&apos;slds-button slds-button_neutral carousel-payload-casedetail carousel-detail-button&apos; &gt;View full case details&lt;/a&gt;&lt;/div&gt;&quot;]}</value>
    </values>
    <values>
        <field>ebcm__Type__c</field>
        <value xsi:type="xsd:string">Carousel</value>
    </values>
</CustomMetadata>
