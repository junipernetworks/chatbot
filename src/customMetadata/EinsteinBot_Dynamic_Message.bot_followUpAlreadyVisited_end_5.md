<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>bot_followUpAlreadyVisited_end 5</label>
    <protected>false</protected>
    <values>
        <field>Bot_Name__c</field>
        <value xsi:type="xsd:string">JuniBot</value>
    </values>
    <values>
        <field>Intent__c</field>
        <value xsi:type="xsd:string">bot_followUpAlreadyVisited_end</value>
    </values>
    <values>
        <field>Response__c</field>
        <value xsi:type="xsd:string">How else can I help today?</value>
    </values>
</CustomMetadata>
