<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>QuerySN</label>
    <protected>false</protected>
    <values>
        <field>ebcm__Bot_Name__c</field>
        <value xsi:type="xsd:string">JuniBot</value>
    </values>
    <values>
        <field>ebcm__Dialog_Request_Payload__c</field>
        <value xsi:type="xsd:string">{&quot;home&quot;:1,&quot;cards&quot;:[&quot;&lt;div class = &apos;carousel&apos;&gt;&lt;h1&gt;Product Information&lt;/h1&gt;&lt;div class=&apos;slds-text-align_left slds-m-left_small&apos;&gt;&lt;p&gt;&lt;span class =&apos;capping&apos;&gt;&lt;b&gt;Serial #/SSRN: &lt;/b&gt;154967111000&lt;br&gt;&lt;b&gt;Product ID: &lt;/b&gt;VSRX-100M-STD&lt;br&gt;&lt;b&gt;Registration Date: &lt;/b&gt;24-APR-2018&lt;br&gt;&lt;b&gt;Warranty Expiry: &lt;/b&gt;30-NOV-0002&lt;/span&gt;&lt;/p&gt;&lt;/div&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;a href=&apos;https://my.juniper.net/#dashboard/products/&apos; target=&apos;_blank&apos; class =&apos;slds-button slds-button_neutral carousel-detail-button&apos;&gt;View full product details&lt;/a&gt;&lt;/div&gt;&quot;,&quot;&lt;div class = &apos;carousel&apos;&gt;&lt;h1&gt;Installation Information&lt;/h1&gt;&lt;div class=&apos;slds-text-align_left slds-m-left_small&apos;&gt;&lt;p&gt;&lt;span class =&apos;capping&apos;&gt;&lt;b&gt;Company: &lt;/b&gt;MILKY WAY MODULES&lt;br&gt;&lt;b&gt;Address: &lt;/b&gt;BOEINGAVENUE 240&lt;br&gt;&lt;b&gt;City: &lt;/b&gt;Schiphol-Rijk&lt;br&gt;&lt;b&gt;State: &lt;/b&gt;Noord-Holland&lt;br&gt;&lt;b&gt;Country: &lt;/b&gt;Netherlands&lt;/span&gt;&lt;/p&gt;&lt;/div&gt;&lt;/div&gt;&lt;div&gt;&lt;a href=&apos;https://my.juniper.net/#dashboard/products/&apos; target=&apos;_blank&apos; class =&apos;slds-button slds-button_neutral carousel-detail-button&apos; style= &apos;margin-top: 5.75rem &apos; &gt;View full product details&lt;/a&gt;&lt;/div&gt;&quot;,&quot;&lt;div class = &apos;carousel&apos;&gt;&lt;h1&gt;Service Contract&lt;/h1&gt;&lt;div class=&apos;slds-text-align_center&apos;&gt;&lt;p&gt;Our records dont show an active J-Care service contract for this asset&lt;/p&gt;&lt;a href = &apos;https://www.juniper.net/us/en/how-to-buy/&apos; target =&apos;_blank&apos;&gt;Purchase J-Care&lt;/a&gt;&lt;br&gt;&lt;br&gt;&lt;a href = &apos;https://www.juniper.net/assets/us/en/local/pdf/datasheets/1000326-en.pdf &apos; target =&apos;_blank&apos;&gt;Learn about J-Care&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;div&gt;&lt;a href=&apos;https://my.juniper.net/#dashboard/products/&apos; target=&apos;_blank&apos; class =&apos;slds-button slds-button_neutral carousel-detail-button&apos; &gt;View full product details&lt;/a&gt;&lt;/div&gt;&quot;]}</value>
    </values>
    <values>
        <field>ebcm__Type__c</field>
        <value xsi:type="xsd:string">Carousel</value>
    </values>
</CustomMetadata>
