public with sharing  class NF_QuerySR_RMAListResponse extends NF_ApiResponse {
    public QuerySRRMAListDetailsResponse querySRRMAListDetailsResponse{ get;set; }

    public class QuerySRRMAListDetailsResponse extends ApiResponse{
        public String customerUniqueTransactionID { get; set; }
        public String responseDateTime { get; set; }
        public String serviceRequestNumber { get; set; }
        public String customerSourceID { get; set; }
        public String customerCaseNumber { get; set; }
        public SRDetails[] srDetails { get; set; }
        public RMADetails[] rmaDetails { get; set; }

        public Boolean isEmpty(){
            return (srDetails == null && rmaDetails == null) ||
                    (srDetails != null && rmaDetails != null && srDetails.size() > 0 && rmaDetails.size() > 0);
        }
    }

    public override Boolean isError(){
        //System.debug('querySRRMAListDetailsResponse resonse class' + querySRRMAListDetailsResponse.isError());
        return querySRRMAListDetailsResponse == null || querySRRMAListDetailsResponse.isError();
    }

    public Boolean isNotFound(){
        return querySRRMAListDetailsResponse == null || querySRRMAListDetailsResponse.isEmpty();
    }

    public override Integer getStatusCode(){
        return querySRRMAListDetailsResponse != null ? querySRRMAListDetailsResponse.getStatusCode() : -1;
    }

    public override Integer getErrorCode(){
        return querySRRMAListDetailsResponse != null ? querySRRMAListDetailsResponse.getErrorCode() : -1;
    }

    public override String getDescription(){
        return querySRRMAListDetailsResponse != null ? querySRRMAListDetailsResponse.getDescription() : error;
    }

    public class SRDetails{
        public String serviceRequestNumber { get; set; }
        public String customerCaseNumber { get; set; }
        public CaseType caseType { get; set; }
        public String synopsis { get; set; }
        public String problemDescription { get; set; }
        public String srStatus { get; set; }
        public String priority { get; set; }
        public String adminSRType { get; set; }
        public String adminSRSubType { get; set; }
        public String ccEmail { get; set; }
        public String addSenderToEmail { get; set; }
        public String followUpMethod { get; set; }
        public String srOwnerFullName { get; set; }
        public String srOwnerEmailAddress { get; set; }
        public Contact contact { get; set; }
        public RMA[] rma {get; set; }
        public Product product {get; set; }
        public Escalate[] escalate {get; set; }
        public Attachment[] attachments {get; set; }
        public LinkedReference[] linkedReferences {get; set; }
    }
    public class CaseType {
        public String caseTypeCode { get; set; }
        public String caseTypeDescription { get; set; }
    }
    public class Contact {
        public String accountID { get; set; }
        public String accountName { get; set; }
        public String contactName { get; set; }
        public String contactEmail { get; set; }
        public String preferredTelephoneCountryCode { get; set; }
        public String preferredTelephoneNumber { get; set; }
        public String preferredTelephoneExtension { get; set; }
        
    }
    public class RMA {
        public String rmaNumber { get; set; }
        public String itemNumber { get; set; }
        public String itemType { get; set; }
        public String itemStatus { get; set; }
    }
    public class Product {
        public String serialNumber { get; set; }
        public String productID { get; set; }
        public String productSeries { get; set; }
        public String platform { get; set; }
        public String version { get; set; }
        public String release { get; set; }
        public String software { get; set; }
        public String specialRelease { get; set; }
        public String routerName { get; set; }
        
    }
    public class Escalate {
        public String description { get; set; }
        public String escalatedBy { get; set; }
        public String status { get; set; }
    }
    public class Attachment {
        public String sequenceNumber { get; set; }
        public String title { get; set; }
        public String sizeInBytes { get; set; }
        public String path { get; set; }
        public String typeDescription { get; set; }
        public String uploadedDateTime { get; set; }
        public String uploadedBy { get; set; }
    }
    public class LinkedReference {
        public String url { get; set; }
        public String referenceId { get; set; }
        public String referenceType { get; set; }
    }

    public class RMADetails {
        public String rmaNumber { get; set; }
        public String rmaHeaderStatus { get; set; }
        public String rmaHeaderCreatedDate { get; set; }
        public RMAContact rmaContact { get; set; }
        public DefectiveItem[] defectiveItems { get; set; }
        public CEItem[] ceItems { get; set; }
        public ReplacementItem[] replacementItems {get; set; }
    }
    public class RMAContact{
        public String companyName { get; set; }
        public String contactName { get; set; }
        public String contactEmail { get; set; }
        public String telephoneCountryCode { get; set; }
        public String telephoneNumber { get; set; }
        public Address address { get; set; }
    }
    public class Address{
        public String address1 { get; set; }
        public String address2 { get; set; }
        public String city { get; set; }
        public String stateCode { get; set; }
        public String state { get; set; }
        public String countryCode { get; set; }
        public String country { get; set; }
        public String postalCode { get; set; }
    }

    public class DefectiveItem{
        public String itemNumber { get; set; }
        public String dateTime_c { get; set; }
        public String productID { get; set; }
        public String serialNumber { get; set; }
        public String trackingNumber { get; set; }
        public String trackingNumberUrl { get; set; }
        public String carrierDescription { get; set; }
        public String receivedDate { get; set; }
        public String defectiveItemStatus { get; set; }
        public String defectiveRmaType { get; set; }
        public String defectiveRmaSubType { get; set; }
    }

    public class CEItem{
        public String defectiveItemNumber { get; set; }
        public String productID { get; set; }
        public String engineerStatus { get; set; }
        public String vendorName { get; set; }
        public String vendorPhone { get; set; }
        public String serviceRequestedDate { get; set; }
        public String actualServicedDate { get; set; }
        public String contactedDate { get; set; }
        public String estimatedArrivalDate { get; set; }
    }
    
    public class ReplacementItem {
        public String itemNumber { get; set; }
        public String defectiveItemNumber { get; set; }
        public String productID { get; set; }
        public String serialNumber { get; set; }
        public String carrierDescription { get; set; }
        public String shipDate { get; set; }
        public String shipmentServiceLevel { get; set; }
        public String trackingNumber { get; set; }
        public String replacementStatus { get; set; }
        public String receivedDate { get; set; }
        public String receivedBy { get; set; }
        public String replacementRmaType { get; set; }
        public String replacementRmaSubType { get; set; }
        public String trackingNumberUrl { get; set; }
    }
        
}