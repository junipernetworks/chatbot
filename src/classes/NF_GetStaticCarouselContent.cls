/**
 * @File Name          : NF_GetStaticCarouselContent.cls
 * @Description        : This class is used to update the static payload on LiveChatTranscript Object.
 * @Author             : Manishi Kalra
 * @Created By         : NeuraFlash, LLC
**/
public with sharing class NF_GetStaticCarouselContent {

    /* 
    * method name: getStaticContent
    * description: this method will update the live chat transcript's field CarouselContent__c
    * input : Wrapper list containing transcript id and payload generated from API response.
    */
    @InvocableMethod(label='Neuraflash - Get Static Carousel Content')
    public static void getStaticContent(List<InputWrapper> inputList) {
        List<String> outputList = new List<String>();

        if(!inputList.isEmpty()) {
            /*List<LiveChatTranscript> transcriptList = [SELECT ID, CarouselContent__c 
                                                      FROM LiveChatTranscript where Id =: inputList[0].routableID];
            if(!transcriptList.isEmpty()) {
                transcriptList[0].CarouselContent__c = inputList[0].payload;
                update transcriptList[0];
            }*/
        }
    }

    public class InputWrapper {
        @InvocableVariable
        public String routableID;
        @InvocableVariable
        public String payload;
    }
}