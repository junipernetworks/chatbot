/**
 * Created by mcasella on 2019-11-17.
 */

public with sharing class NF_QueryRMA {
    public static String ENDPOINT = 'callout:Juniper_API_Gateway/v01/queryrma';

    public class Input{
        @InvocableVariable(required=true)
        public String chatKey;

        @InvocableVariable(required=false)
        public String email;

        @InvocableVariable(required=true)
        public String rmaNumber;

        @InvocableVariable(required=false)
        public String contactId;
    }
    public class Output{
        @InvocableVariable
        public String response;

        @invocableVariable
        public Integer returnCode;
        
        @InvocableVariable
        public String rmaType;
        
        @InvocableVariable
        public String replacementLineItemStatus;
        
        @InvocableVariable
        public String inTransitComment;
        
        @InvocableVariable
        public String carouselPayload;
    }

    @InvocableMethod(label='Neuraflash - Query RMA')
    public static List<Output> queryRMA(List<Input> input) {
        Integer returnCode = nf_ApiController.API_RESULT.FAILURE;

        List<Output> output = new List<Output>();
        Output out = new Output();
        out.rmaType = 'generic';

        try {
            String chatkey = input[0].chatKey;
            String email   = input[0].email;
            String rmaNumber = input[0].rmaNumber;
            String contactId = input[0].contactId;

            NF_ApiController apiController = new NF_ApiController();
            apiController.initialize(chatKey);

            NF_QueryRMARequest apiRequest = new NF_QueryRMARequest(chatKey);
            apiRequest.queryRMARequest.rmaNumber = rmaNumber;
            apiRequest.queryRMARequest.caseInformation.serviceRequestNumber = '';
            apiRequest.queryRMARequest.caseInformation.customerCaseNumber = '';

            apiRequest.setEmail(email);
            apiRequest.setContactId(contactId);
            apiRequest.setAccountId(contactId);
            /*apiRequest.createSRRequest.caseInformation.synopsis = firstUserInput;
            apiRequest.createSRRequest.caseInformation.problemDescription = firstUserInput;
            apiRequest.createSRRequest.caseInformation.customerCaseNumber = caseNumber;*/

            System.debug('>>NF_QueryRMA apiRequest=' + apiRequest);

            NF_ApiController.ToReplace unreserve = new NF_ApiController.ToReplace();
            unreserve.replaced = 'dateTime';
            unreserve.replacedWith = 'dateTime_c';

            apiController.toReplaces.add(unreserve);

            NF_QueryRMAResponse apiResponse = (NF_QueryRMAResponse) apiController.sendApiRequest(apiRequest, NF_QueryRMAResponse.class, true);
            System.debug('>>NF_QueryRMA response=' + apiResponse);

            if (apiResponse != null) {
                try {
                    if (apiResponse.isError() == false) {
                        out.response = JSON.serialize(apiResponse.queryRMAResponse);
                        System.debug('>>NF_QueryRMA out.response=' + out.response);
                        List<NF_QueryRMAResponse.ReplacementItem> replacementItemList = apiResponse.QueryRMAResponse.rmaInformation[0].replacementItems;
                        System.debug('QueryRMA>>> replacementItemList'+replacementItemList);
                        //added by Manishi for dynamic carousel
                        String finalPayload = NF_DynamicCarouselContentFactory.createNF_QueryRMACarousel(apiResponse,rmaNumber);
                        out.carouselPayload = NF_DynamicCarouselContentFactory.createNF_QueryRMACarousel(apiResponse,rmaNumber);
                        System.debug('carouselPayload>>>'+out.carouselPayload);
                        
                        //added by Manishi
                        if(apiResponse.QueryRMAResponse.rmaInformation[0].defectiveItems.size() >0){
                            if(apiResponse.QueryRMAResponse.rmaInformation[0].defectiveItems[0].defectiveRmaType == 'Advance Exchange'){
                                out.rmaType = 'AE';
                            }
                            if(apiResponse.QueryRMAResponse.rmaInformation[0].defectiveItems[0].defectiveRmaType == 'Depot Repair') {
                                out.rmaType = 'DR';
                            }
                            
                        }
                        if(replacementItemList != null) {
                            
                            out.replacementLineItemStatus = apiResponse.QueryRMAResponse.rmaInformation[0].replacementItems[0].replacementStatus;
                            out.inTransitComment = 'Replacement Line Item Status - '+out.replacementLineItemStatus + '; RMA Number - '+rmaNumber;
                        }
                        ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatkey);
                        if(session != null){
                            session.CarouselContent__c = finalPayload;
                            update session;
                            System.debug('Updated field>>>'+session.CarouselContent__c);
                        }
                        returnCode = nf_ApiController.API_RESULT.SUCCESS;
                    }
                    else{
                        returnCode = nf_ApiController.API_RESULT.FAILURE;
                    }
                    
                    
                }catch(Exception e){
                    System.debug('NF_QueryRMA Exception>>>>'+e.getMessage() + e.getStackTraceString()); 
                    returnCode = nf_ApiController.API_RESULT.FAILURE;
                }
            }
            else{
                System.debug('Entered in else');
                returnCode = nf_ApiController.API_RESULT.FAILURE;
            }
            
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
        }

        out.returnCode = returnCode;

        output.add(out);
        return output;
    }
}