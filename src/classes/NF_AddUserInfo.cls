/**
 * Created by mcurry on 2019-11-21.
 */

public with sharing class NF_AddUserInfo {
    public class Input{
        @InvocableVariable(required=true)
        public String FirstName;

        @InvocableVariable(required=true)
        public String LastName;

        @InvocableVariable(required=true)
        public String Email;

        @InvocableVariable(required=true)
        public String chatKey;

    }
    public class Output{
        @InvocableVariable(required=false)
        public Boolean success = null;
    }



    @InvocableMethod(label='Neuraflash - Add User Info to Transcript')
    public static List<Output> addUserInfo(List<Input> input){
        List<Output> result = new List<Output>();
        Output out = new Output();

        LiveChatTranscript transcript = NF_Util.getLiveChatTranscript(input[0].chatKey);



        if (transcript != null){
            transcript.C2CEmail__c = input[0].Email;
            transcript.C2CFirstName__c = input[0].FirstName;
            if (input[0].LastName != '{{undefined}}') {
                transcript.C2CLastName__c = input[0].LastName;
            }
            update transcript;
            out.success = true;
            result.add(out);
        }
        else {
            // there was an error updating the transcript
//            if(out != null) {
//                if (out.success == true) {
//                    out.success = false;
//                }
//            }
//            if (result != null) {
//                result.add(out);
//            }

            out.success = false;
            result.add(out);

        }

        return result;

    }
}