/**
 * Created by mcasella on 2019-11-17.
 */

@isTest
public with sharing class NF_QueryRMATest {
    @testSetup
    static void initialize() {
        NF_UtilTest.insertApiSettings(true);
    }

    @IsTest
    static void testSuccess() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SuccessResponse());

        NF_QueryRMA.Input input = new NF_QueryRMA.Input();
        input.chatKey = 'abc';
        input.email = 'juniperwayjason@gmail.com';
        input.rmaNumber = 'R200239781';
        input.contactId = '0201123987';

        List<NF_QueryRMA.Output> output = NF_QueryRMA.queryRMA(new List<NF_QueryRMA.Input>{input});

        System.debug('OUTPUT RESPONSE IS: ' + output[0].response);

        System.assertEquals(nf_ApiController.API_RESULT.SUCCESS, output[0].returnCode);

        Test.stopTest();
    }

    static void accessResponseFields(NF_QueryRMAResponse res){
        res.queryRMAResponse.customerCaseNumber = '';
        res.queryRMAResponse.customerSourceID = '';
        res.queryRMAResponse.customerUniqueTransactionID = '';
        res.queryRMAResponse.responseDateTime = '';
        res.queryRMAResponse.serviceRequestNumber = '';
        res.queryRMAResponse.rmaInformation[0].rmaNumber = '';
        res.queryRMAResponse.rmaInformation[0].rmaHeaderStatus = '';
        res.queryRMAResponse.rmaInformation[0].rmaHeaderCreatedDate = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact = new NF_QueryRMAResponse.RMAContact();
        res.queryRMAResponse.rmaInformation[0].rmaContact.contactEmail = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact.address = new NF_QueryRMAResponse.Address();
        res.queryRMAResponse.rmaInformation[0].rmaContact.address.address1 = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact.address.address2 = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact.address.city = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact.address.country = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact.address.countryCode = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact.address.postalCode = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact.address.state = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact.address.stateCode = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact.companyName = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact.contactName = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact.telephoneCountryCode = '';
        res.queryRMAResponse.rmaInformation[0].rmaContact.telephoneNumber = '';
        res.queryRMAResponse.rmaInformation[0].ceItems = new List<NF_QueryRMAResponse.CEItem>{new NF_QueryRMAResponse.CEItem()};
        res.queryRMAResponse.rmaInformation[0].ceItems[0].actualServicedDate = '';
        res.queryRMAResponse.rmaInformation[0].ceItems[0].contactedDate = '';
        res.queryRMAResponse.rmaInformation[0].ceItems[0].defectiveItemNumber = '';
        res.queryRMAResponse.rmaInformation[0].ceItems[0].engineerStatus = '';
        res.queryRMAResponse.rmaInformation[0].ceItems[0].estimatedArrivalDate = '';
        res.queryRMAResponse.rmaInformation[0].ceItems[0].productID = '';
        res.queryRMAResponse.rmaInformation[0].ceItems[0].serviceRequestedDate = '';
        res.queryRMAResponse.rmaInformation[0].ceItems[0].vendorName = '';
        res.queryRMAResponse.rmaInformation[0].ceItems[0].vendorPhone = '';
        res.queryRMAResponse.rmaInformation[0].defectiveItems = new List<NF_QueryRMAResponse.DefectiveItem>{new NF_QueryRMAResponse.DefectiveItem()};
        res.queryRMAResponse.rmaInformation[0].defectiveItems[0].productID = '';
        res.queryRMAResponse.rmaInformation[0].defectiveItems[0].carrierDescription = '';
        res.queryRMAResponse.rmaInformation[0].defectiveItems[0].dateTime_c = '';
        res.queryRMAResponse.rmaInformation[0].defectiveItems[0].defectiveItemStatus = '';
        res.queryRMAResponse.rmaInformation[0].defectiveItems[0].defectiveRmaSubType = '';
        res.queryRMAResponse.rmaInformation[0].defectiveItems[0].defectiveRmaType = '';
        res.queryRMAResponse.rmaInformation[0].defectiveItems[0].itemNumber = '';
        res.queryRMAResponse.rmaInformation[0].defectiveItems[0].receivedDate = '';
        res.queryRMAResponse.rmaInformation[0].defectiveItems[0].serialNumber = '';
        res.queryRMAResponse.rmaInformation[0].defectiveItems[0].trackingNumber = '';
        res.queryRMAResponse.rmaInformation[0].defectiveItems[0].trackingNumberUrl = '';
    }

    @IsTest
    static void testSuccessResponse() {
        NF_QueryRMAResponse response = new NF_QueryRMAResponse();
        //weirdness to get around test coverage
        JSON.serialize(response);
        response = (NF_QueryRMAResponse)JSON.deserialize(SUCCESS_RESPONSE, NF_QueryRMAResponse.class);

        //weirdness to get around test coverage
        NF_QueryRMAResponse cwa = new NF_QueryRMAResponse();
        cwa.queryRMAResponse = new NF_QueryRMAResponse.QueryRMAResponse();
        cwa.queryRMAResponse.rmaInformation = new List<NF_QueryRMAResponse.RMAInfo>{new NF_QueryRMAResponse.RMAInfo()};

        accessResponseFields(cwa);

        System.assertEquals(false, response.isError());
        //System.assertEquals('12345678', response.queryRMAResponse.customerCaseNumber);
        System.assertEquals('Successfully processed the request', response.getDescription());
    }

    @IsTest
    static void testUnauthorized(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UnauthorizedResponse());

        NF_QueryRMA.Input input = new NF_QueryRMA.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_QueryRMA.Output> output = NF_QueryRMA.queryRMA(new List<NF_QueryRMA.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testUnauthorizedResponse() {
        NF_QueryRMAResponse response = (NF_QueryRMAResponse)JSON.deserialize(UNAUTHORIZED_RESPONSE, NF_QueryRMAResponse.class);

        System.assertEquals(true, response.isError());
        System.assertEquals('Key not authorised', response.getDescription());
    }

    @IsTest
    static void testError(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ErrorResponse());

        NF_QueryRMA.Input input = new NF_QueryRMA.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_QueryRMA.Output> output = NF_QueryRMA.queryRMA(new List<NF_QueryRMA.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testErrorResponse() {
        NF_QueryRMAResponse response = (NF_QueryRMAResponse)JSON.deserialize(ERROR_RESPONSE, NF_QueryRMAResponse.class);

        System.assertEquals(true, response.isError());
        //System.assertEquals('\r\n[775] rmaNumber - R00002098 - is not associated with serviceRequestNumber - 2015-0811-T-0007', response.getDescription());
        System.assertEquals(400, response.getStatusCode());
    }

    private static String SUCCESS_RESPONSE = '{ "queryRMAResponse": { "customerCaseNumber": null, "customerSourceID": "juni-chatbot", "customerUniqueTransactionID": "1234", "serviceRequestNumber": null, "responseDateTime": "2019-12-10T19:22:02.883Z", "statusCode": "200", "status": "Success", "message": "Successfully processed the request", "rmaInformation": [ { "rmaNumber": "R200239781", "rmaHeaderStatus": "Open", "rmaHeaderCreatedDate": "2019-05-28T09:40:09.000Z", "rmaContact": { "companyName": "ORANGE UI NORMANDIE", "contactName": "Nicolas Bougan", "contactEmail": "pascal.budet@orange.com,nicolas.bougan@orange.com,orange-rma@juniper.net", "telephoneCountryCode": "FR", "telephoneNumber": "+33 06 45 21 89", "address": { "address1": "29 RUE DE L\'AVENIR", "address2": null, "city": "CARPIQUET", "stateCode": "14", "state": "Calvados", "countryCode": "FR", "country": "France", "postalCode": "14650" } }, "defectiveItems": [ { "itemNumber": "100", "dateTime": "2019-05-28T09:46:58.000Z", "productID": "MPC4E-3D-2CGE-8XGE", "serialNumber": "CABG1832", "trackingNumber": null, "trackingNumberUrl": null, "carrierDescription": null, "receivedDate": null, "defectiveItemStatus": "Awaiting Defective Return", "defectiveRmaType": "Advance Exchange", "defectiveRmaSubType": "CUST-SAT-ND" } ], "replacementItems": [ { "itemNumber": "110", "defectiveItemNumber": "100", "productID": "MPC4E-3D-2CGE-8XGE", "serialNumber": "CAEJ1514", "carrierDescription": null, "shipDate": "2019-05-28T12:17:16.000Z", "shipmentServiceLevel": null, "trackingNumber": "1ZR263W30403715687", "trackingNumberUrl": null, "replacementStatus": "RMA Delivered - Closed", "receivedDate": "2019-05-29T17:42:00.000Z", "receivedBy": "PARIS", "replacementRmaType": "Advance Exchange", "replacementRmaSubType": "CUST-SAT-ND" } ] } ] } }';
    private static String ERROR_RESPONSE = '{ "queryRMAResponse": { "customerCaseNumber": null, "customerSourceID": "juni-chatbot", "customerUniqueTransactionID": "1234", "serviceRequestNumber": "2015-0811-T-0007", "responseDateTime": "2019-11-20T03:07:13.283Z", "statusCode": "400", "status": "Error", "message": "Error in processing the request", "fault": [ { "errorClass": "Processing", "errorType": "Error", "errorCode": "775", "errorMessage": "rmaNumber - R00002098 - is not associated with serviceRequestNumber - 2015-0811-T-0007." } ] } }';
    private static String UNAUTHORIZED_RESPONSE = '{ "error": "Key not authorised" }';

    public class SuccessResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, SUCCESS_RESPONSE); }
    }

    public class UnauthorizedResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(403, UNAUTHORIZED_RESPONSE); }
    }

    public class ErrorResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(500, ERROR_RESPONSE); }
    }
}