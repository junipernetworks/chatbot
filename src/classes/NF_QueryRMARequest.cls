/**
 * Created by mcasella on 2019-11-17.
 */

public with sharing class NF_QueryRMARequest extends NF_ApiRequest {
    public NF_QueryRMARequest(String chatKey){
        queryRMARequest = new QueryRMARequest();
        queryRMARequest.caseInformation = new CaseInformation();
        queryRMARequest.caseInformation.customerUniqueTransactionID = NF_Util.generateTransactionId(chatKey);
        queryRMARequest.contact = new Contact();
    }

    public void setContactId(String id){
        queryRMARequest.contact.contactId = id;
    }

    public void setEmail(String email){
        queryRMARequest.contact.contactEmail = email;
    }

    public void setAccountId(String id){
        queryRMARequest.contact.accountID = id;
    }

    public override String getApiName(){
        return 'Query RMA';
    }

    public override String getEndpoint(){
        return 'callout:Juniper_API_Gateway/v01/queryrma';
    }

    public override String getTransactionId(){
        return queryRMARequest != null ? queryRMARequest.caseInformation.customerUniqueTransactionID : '';
    }

    public QueryRMARequest queryRMARequest;

    public class QueryRMARequest extends AbstractRequest{
        public CaseInformation caseInformation { get; set; }
        public String rmaNumber { get; set; }
        public Contact contact { get; set; }
    }

    public class CaseInformation{
        public String customerSourceID = 'juni-chatbot';
        public String customerUniqueTransactionID { get; set; }
        public String customerCaseNumber { get; set; }
        public String serviceRequestNumber { get; set; }
    }
}