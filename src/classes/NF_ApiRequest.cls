/**
 * Created by mcasella on 2019-11-17.
 */

public with sharing abstract class NF_ApiRequest {
    public virtual class AbstractRequest {
        public String appId = NF_ApiController.appId;
        public String userId = 'chatbot-support@juniper.net';
        public String requestDateTime = NF_Util.getDateTime();
    }

    public abstract String getApiName();
    public abstract String getEndpoint();
    public abstract String getTransactionId();
    
    public virtual String getMethod(){
        return 'POST';
    }

    public virtual String getBody(){
        return JSON.serialize(this);
    }

    public virtual Map<String, String> getHeaders(){
        Map<String, String> headers = new Map<String, String>();

        headers.put('Authorization', NF_ApiController.authorization);
        headers.put('Content-Type', 'application/json');
        headers.put('Accept', 'application/json');

        return headers;
    }

    public class Contact{
        public String accountID = '';
        public String contactId = '';
        public String contactEmail = '';
    }
}