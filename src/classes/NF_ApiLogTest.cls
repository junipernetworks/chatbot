/**
 * Created by mcasella on 2/22/20.
 */

@IsTest
public with sharing class NF_ApiLogTest {

    @IsTest
    static void createLog(){
        NF_GetUserProfileRequest request = new NF_GetUserProfileRequest('test@nf.com');
        NF_ApiLog log = new NF_ApiLog(request);
        log.setResponse(new NF_GetUserProfileResponse());
        System.assertEquals(log.name, request.getApiName());
    }

    @IsTest
    static void testProcessingTime(){
        NF_GetUserProfileRequest request = new NF_GetUserProfileRequest('test@nf.com');
        NF_ApiLog log = new NF_ApiLog(request);
        log.setResponse(new NF_GetUserProfileResponse());
        Long processingTime = log.endTime.getTime() - log.startTime.getTime();
        System.assertEquals(processingTime, log.processingTimeMs);
    }
}