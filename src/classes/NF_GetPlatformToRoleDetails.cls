public with sharing class NF_GetPlatformToRoleDetails {
    public static String ENDPOINT = 'callout:Juniper_API_Gateway/v01/getplatformtoroledetails';

    public class Input{
        @InvocableVariable(required=true)
        public String chatKey;

        @InvocableVariable(required=false)
        public String platformName;

        @InvocableVariable(required=false)
        public String osId;

        @InvocableVariable
        public String emailId;
        
    }
    public class Output{
        @InvocableVariable
        public String response;

        @invocableVariable
        public Integer returnCode;

        @invocableVariable
        public String softwareDownloadRole;
    }
    @InvocableMethod(label='Neuraflash - Get Platform To Role Details')
    public static List<Output> getPlatformToRoleDetails(List<Input> input) {
        Integer returnCode = nf_ApiController.API_RESULT.FAILURE;

        List<Output> output = new List<Output>();
        Output out = new Output();

        try {
            String chatkey = input[0].chatKey;
            String osId   = input[0].osId;
            String platformName = input[0].platformName;
            String email = input[0].emailId;

            NF_ApiController apiController = new NF_ApiController();
            apiController.initialize(chatKey);
            apiController.email = email;

            NF_GetPlatformToRoleDetailsRequest apiRequest = new NF_GetPlatformToRoleDetailsRequest(chatKey, 'b');
            apiRequest.setPlatformShortName(platformName);
            apiRequest.setOSId(osId);
            System.debug('>>NF_GetPlatformToRoleDetailsRequest apiRequest=' + apiRequest);

            NF_ApiController.ToReplace unreserve = new NF_ApiController.ToReplace();
            unreserve.replaced = 'dateTime';
            unreserve.replacedWith = 'dateTime_c';

            apiController.toReplaces.add(unreserve);

            NF_GetPlatformToRoleDetailsResponse apiResponse = (NF_GetPlatformToRoleDetailsResponse) apiController.sendApiRequest(apiRequest, NF_GetPlatformToRoleDetailsResponse.class, true);
            System.debug('>>NF_GetPlatformToRoleDetailsResponse response=' + apiResponse);

            if (apiResponse != null) {
                try {
                    out.response = JSON.serialize(apiResponse.queryPlatformToRoleDetailsResponse);
                    if (apiResponse.isError() == false) {
                        //out.response = JSON.serialize(apiResponse.queryPlatformToRoleDetailsResponse);
                        System.debug('>>NF_GetPlatformToRoleDetailsResponse out.response=' + out.response);
                        if(apiResponse.queryPlatformToRoleDetailsResponse.roles.size() >0) {
                            for(integer i = 0; i<apiResponse.queryPlatformToRoleDetailsResponse.roles.size(); i++) {
                                if(String.isNotBlank(out.softwareDownloadRole)) {
                                    out.softwareDownloadRole = out.softwareDownloadRole + ', ' + apiResponse.queryPlatformToRoleDetailsResponse.roles[i].roleName;
                                }
                                else {
                                    out.softwareDownloadRole = apiResponse.queryPlatformToRoleDetailsResponse.roles[i].roleName;
                                }
                            }
                        }
                        
                        returnCode = nf_ApiController.API_RESULT.SUCCESS;
                    }
                    else{
                        returnCode = nf_ApiController.API_RESULT.FAILURE;
                    }
                    
                    
                }catch(Exception e){
                    System.debug('NF_GetPlatformToRoleDetailsResponse Exception>>>>'+e.getMessage() + e.getStackTraceString()); 
                    returnCode = nf_ApiController.API_RESULT.FAILURE;
                }
            }
            else{
                returnCode = nf_ApiController.API_RESULT.FAILURE;
            }
            
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
        }

        out.returnCode = returnCode;

        output.add(out);
        return output;
    }
   
}