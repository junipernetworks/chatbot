/**
 * Created by mcasella on 2019-11-18.
 */

public with sharing  class NF_CreateServiceRequestResponse extends NF_ApiResponse {
    public CreateSRResponse createSRResponse{get;set;}

    public class CreateSRResponse extends ApiResponse{
        public String customerUniqueTransactionID { get; set; }
        public String responseDateTime { get; set; }
        public String serviceRequestNumber { get; set; }
        public String customerCaseNumber { get; set; }
    }

    public override Boolean isError(){
        return createSRResponse == null || createSRResponse.isError();
    }

    public override Integer getStatusCode(){
        return createSRResponse != null ? createSRResponse.getStatusCode() : -1;
    }

    public override Integer getErrorCode(){
        return createSRResponse != null ? createSRResponse.getErrorCode() : -1;
    }

    public override String getDescription(){
        return createSRResponse != null ? createSRResponse.getDescription() : error;
    }
}