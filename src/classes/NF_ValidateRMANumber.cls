/**
 * @File Name          : NF_CheckRMANumber.cls
 * @Description        : This class is to validate the RMA number against the regex.
 * @Author             : Manishi Kalra
 * @Created By         : NeuraFlash, LLC
**/
public with sharing class NF_ValidateRMANumber {
 
    public class Input{
        @InvocableVariable(required=true)
        public String rmaNumber;
    }
    
    public class Output{
        @InvocableVariable
        public Boolean isValid;

        @InvocableVariable(required=false)
        public String rmaNumber;
    }

    /* 
    * method name: validateRMANumber
    * description: this method will form the dynamic carousel using html tags.
    * input : response from Product Details API.
    * output: validated RMA number
    * updated: [Juni CECCB-49] : Chatbot not handling invalidly formatted RMA #s correctly. Regex has been changed. 
    */
    @InvocableMethod(label='Neuraflash - Validate RMA Number')
    public static List<Output> validateRMANumber(List<Input> input) {
        List<Output> output = new List<Output>();

        Output out = new Output();
        out.isValid = false;
        String userInput = input[0].rmaNumber;

        try{
            String[] userInputArray = userInput.split(' ');
            Pattern pattern = Pattern.compile('[R]{1}\\d{9}|\\d{9}');
            for(integer i = 0; i< userInputArray.size(); i++) {
                Matcher matcher = pattern.matcher(userInputArray[i]);
                if(matcher.matches()) {
                    out.isValid = matcher.matches();
                    out.rmaNumber = out.isValid ? matcher.group(0) : '';
                    break;
                }
            }
        }
        catch(Exception e){
            NF_EinsteinBotLogger.logException(e);
        }
        output.add(out);
        return output;
    }

}