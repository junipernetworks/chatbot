public class NF_EinsteinBotLogger {

    public static boolean populatedLog(Object o){
        try {
            NF_Exception_Event__e exceptionEvent = new NF_Exception_Event__e();
            if (o InstanceOf Exception) {
                Exception ex = (Exception) o;
               	exceptionEvent.Error_Message__c = ex.getMessage();
                exceptionEvent.Exception_Data__c = getExceptionData(ex);
                exceptionEvent.Stack_Trace__c = ex.getStackTraceString();
                exceptionEvent.User_Name__c = UserInfo.getName();
                exceptionEvent.User_Id__c = UserInfo.getUserId();
                exceptionEvent.Type__c = ex.getTypeName();
           } else {
                exceptionEvent.Exception_Data__c = String.ValueOf(o);
                exceptionEvent.User_Name__c = UserInfo.getName();
                exceptionEvent.User_Id__c = UserInfo.getUserId();
            }
            EventBus.publish(exceptionEvent);
        }
        catch(Exception e){}
        return true;
    }

    public static boolean logException(Exception e){
        return populatedLog(e);
    }
        
        /*public static void populatedDebugLog(String ObjJson){
        Einstein_Bot_Debug_Log__c debugLogs = new Einstein_Bot_Debug_Log__c();
        if(o InstanceOf Exception){
            Exception ex=(Exception) o;
            debugLogs.Error_Message__c = ex.getMessage();
            debugLogs.Exception_Data__c = getExceptionData(ex);
            debugLogs.Stack_Trace__c = ex.getStackTraceString();
            debugLogs.User_Name__c = UserInfo.getName();
            debugLogs.User_Id__c = UserInfo.getUserId();
            debugLogs.Type__c = ex.getTypeName();
            System.debug('Exception:'+ex.getMessage());
        }
        else{
            debugLogs.Exception_Data__c = String.ValueOf(o);
            debugLogs.User_Name__c = UserInfo.getName();
            debugLogs.User_Id__c = UserInfo.getUserId();
        }
        try {
            insert debugLogs;
        }catch(Exception e){
            return false;
        }
        return true;
    }"*/

    public static String getExceptionData(Exception ex) {
        String exceptionTrace = '====== ** SYSTEM HANDLED EXCEPTION ** ======';
        exceptionTrace += '\n';
        exceptionTrace += 'Code Line #: ' + ex.getLineNumber();
        exceptionTrace += '\n';
        exceptionTrace += '==== ** Exception Message ** ====';
        exceptionTrace += '\n';
		exceptionTrace += ex.getMessage();
        exceptionTrace += '\n';
        exceptionTrace += '==== ** Salesforce Stack Trace ** ====';
        exceptionTrace += '\n';
        exceptionTrace += ex.getStackTraceString();
        exceptionTrace += '\n=== ** Current Log ** ===\n';
        return exceptionTrace;
    }
    public class errorWrapper{
        public string errorMessage;
    }
	
    @future
    public static void logAPIException(String errorMessage){
         Einstein_Bot_Debug_Log__c debugLogs = new Einstein_Bot_Debug_Log__c();
         debugLogs.Type__c = 'API errors';
         debugLogs.Error_Message__c = errorMessage;
        try {
            insert debugLogs;
        }catch(Exception e){
            
        }
    }
}