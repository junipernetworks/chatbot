/**
 * Created by mcasella on 2019-12-05.
 */

public with sharing class NF_CheckForBusinessIntent {

    public class Input {
        @InvocableVariable(required=true)
        public String chatKey;
    }

    public class Output {
        @InvocableVariable(required=true)
        public String lastBusinessIntent;

        @InvocableVariable(required=true)
        public Boolean hasBusinessIntent;
    }

    @InvocableMethod(label='NeuraFlash - Check For Business Intent')
    public static List<NF_CheckForBusinessIntent.Output> getIntents(List<Input> inputs){
        System.debug('in check for business intent class');
        Output output = new Output();
        output.hasBusinessIntent = false;
        output.lastBusinessIntent = '';

        try {
            //query for business intents
            ebotdata__Bot_Chat_Log__c[] businessChatLogs = getBusinessChatLogs(inputs[0].chatKey);

            //if anything was returned, we have a business intent
            if(businessChatLogs.size() > 0){
                System.debug('>>NF_CheckForBusinessIntent: found a business intent');
                output.hasBusinessIntent = true;

                //grab the last business intent name
                output.lastBusinessIntent = businessChatLogs[businessChatLogs.size()-1].ebotdata__Current_Dialog_Name__c;
            }
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
        }

        System.debug('Does the user have business intent:' + output.hasBusinessIntent);

        return new List<Output>{
                output
        };
    }

    /*
     * Returns business chat logs for the current session, in the order they were created
     */
    public static ebotdata__Bot_Chat_Log__c[] getBusinessChatLogs(String chatKey){
        return [SELECT ebotdata__Current_Dialog_Name__c, ebotdata__Intent_Name__c, ebotdata__Live_Agent_Session_Id__c
                FROM ebotdata__Bot_Chat_Log__c
                WHERE ebotdata__Live_Agent_Session_Id__c = :chatKey AND
                     (ebotdata__Intent_Name__c LIKE 'bus_%' OR ebotdata__Current_Dialog_Name__c LIKE 'bus_%')
                ORDER BY CreatedDate ASC];
    }
}