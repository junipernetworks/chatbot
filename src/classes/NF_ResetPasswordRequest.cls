/**
 * Created by mcasella on 2019-11-21.
 */

public with sharing class NF_ResetPasswordRequest extends NF_ApiRequest {

    public NF_ResetPasswordRequest(String chatKey, String email){
        resetPasswordRequest = new ResetPasswordRequest(chatKey, email);
    }

    public override String getApiName(){
        return 'Reset Password';
    }

    public override String getEndpoint(){
        return 'callout:Juniper_API_Gateway/v01/resetpassword';
    }

    public override String getTransactionId(){
        return resetPasswordRequest != null ? resetPasswordRequest.caseInformation.customerUniqueTransactionID : '';
    }

    public ResetPasswordRequest resetPasswordRequest {get; set;}

    public class ResetPasswordRequest extends AbstractRequest {
        public ResetPasswordRequest(String chatKey, String email) {
            caseInformation = new CaseInformation(chatKey);
            contact = new Contact();
            contact.contactEmail = email;
        }

        public Contact contact { get; set; }
        public CaseInformation caseInformation {get; set;}
    }

    public class CaseInformation{
        public CaseInformation(String chatKey){
            this.customerUniqueTransactionID = NF_Util.generateTransactionId(chatKey);
        }

        public String customerSourceID = 'juni-chatbot';
        public String customerUniqueTransactionID {get; set;}
    }
}

/*
{
    "resetPasswordRequest": {
        "appId": "URJxgQGSBrm8JuQ0eGPKSt1UqqF13D8eONChGlc8",
        "userId": "test@qwest.com",
        "requestDateTime": "2008-09-28T18:49:45",
        "caseInformation": {
            "customerSourceID": "juni-chatbot",
            "customerUniqueTransactionID": "1234abcd"
        },
        "contact": {
            "contactEmail": ""
        }
    }
}
 */