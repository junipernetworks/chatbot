/**
 * Created by mcasella on 2019-11-06.
 */

@isTest
public with sharing class NF_LiveChatTranscriptReassignBotCaseTest {

    static testMethod void updateLiveChatTranscript() {
        String chatKey = 'asdf';
        Test.startTest();

        //Contact con = NF_UtilTest.insertContact();
        LiveChatTranscript transcript = NF_UtilTest.insertLiveChatTranscript(chatKey, 'https://www.neuraflash.com', null);

        String firstName = 'Matt';

        transcript.C2CFirstName__c = firstName;
        update transcript;

        LiveChatTranscript updatedTranscript = [SELECT Id, C2CFirstName__c
                                                FROM LiveChatTranscript
                                                WHERE C2CFirstName__c = :firstName];

        System.assertNotEquals(null, transcript.Id);
        //System.assertNotEquals('Matt', transcript.C2CFirstName__c);
        //TODO: update event
        //ebotsms__Bot_Session__c session = NF_UtilTest.insertBotSession(chatKey,transcript.Id);
        //NF_EinsteinBotEventTriggerHandler.updateSession(chatKey);
        Test.stopTest();
    }
}