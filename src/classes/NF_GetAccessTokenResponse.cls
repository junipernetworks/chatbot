/**
 * Created by mcasella on 2019-11-16.
 */

public with sharing class NF_GetAccessTokenResponse extends NF_ApiResponse{
    public String access_token{ get;set; }
    public String expires_in{ get;set; }
    public String token_type{ get;set; }
    public String error{ get; set; }
    public String error_description = '';

    public String getAuthorization(){
        return (String.isNotEmpty(token_type) && String.isNotEmpty(access_token)) ? token_type + ' ' + access_token : '';
    }

    public Long getTokenExpiry(){
        return (String.isNotEmpty(expires_in)) ? Long.valueOf(expires_in) * 1000 : 0;
    }

    //Unfortunately this API Response does not have a wrapper response class like the other response types, and does not have it's own statusCode
    //Check if the error field returned is empty for a success
    public override Boolean isError(){
        return String.isNotEmpty(error);
    }

    //No return statusCode ever retruned from API
    public override Integer getStatusCode(){
        return -1;
    }

    public override Integer getErrorCode(){
        return -1;
    }

    public override String getDescription(){
        return error_description != null ? error_description : '';
    }
}