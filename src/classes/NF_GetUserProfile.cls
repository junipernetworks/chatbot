/**
 * Created by mcasella on 2019-11-21.
 */

public with sharing class NF_GetUserProfile {
    public static String ENDPOINT = 'callout:Juniper_API_Gateway/v01/queryuserprofile';

    public class Input{
        @InvocableVariable(required=true)
        public String chatKey;

        @InvocableVariable(required=false)
        public String email;
    }
    public class Output{
        @invocableVariable
        public Integer returnCode;

        @InvocableVariable
        public String response;

        @InvocableVariable
        public String contactId;

        @InvocableVariable
        public String accountId;
    }

    @InvocableMethod(label='Neuraflash - Call Get User Profile')
    public static List<Output> getProfile(List<Input> input) {
        Integer returnCode = nf_ApiController.API_RESULT.FAILURE;

        List<Output> output = new List<Output>();
        Output out = new Output();

        String chatkey = input[0].chatKey;
        String email   = input[0].email;

        try {
            System.debug('>>>>chatkey'+chatkey);
            NF_ApiController apiController = new NF_ApiController();
            apiController.initialize(chatKey);

            NF_GetUserProfileRequest apiRequest = new NF_GetUserProfileRequest(email);
            System.debug('>>NF_GetUserProfile apiRequest=' + apiRequest);

            NF_GetUserProfileResponse apiResponse = (NF_GetUserProfileResponse) apiController.sendApiRequest(apiRequest, NF_GetUserProfileResponse.class, true);
            System.debug('>>NF_GetUserProfile apiResponse=' + apiResponse);

            if (apiResponse != null) {
                out.response = JSON.serialize(apiResponse);
                System.debug('>>NF_GetUserProfile out.response=' + out.response);

                if (apiResponse.isError() == false) {
                    out.contactId = apiResponse.getContactId();
                    out.accountId = apiResponse.getAccountId();
                    returnCode = nf_ApiController.API_RESULT.SUCCESS;
                }
            }
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
        }

        out.returnCode = returnCode;

        if(out.contactId == null){
            out.contactId = 'null';
        }

        output.add(out);
        return output;
    }
}