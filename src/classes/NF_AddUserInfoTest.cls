/**
 * Created by mcurry on 2019-11-21.
 */
@IsTest
public with sharing class NF_AddUserInfoTest {
    @IsTest
    static void test1(){
        Test.startTest();

        NF_AddUserInfo.Input input = new NF_AddUserInfo.Input();
        input.FirstName = 'Michael';
        input.LastName = 'Curry';
        input.Email = 'michael.curry@neuraflash.com';
        input.chatKey = '';

        List<NF_AddUserInfo.Output> output = NF_AddUserInfo.addUserInfo(new List<NF_AddUserInfo.Input>{input});

        System.debug('>> Output test success: ' + output[0].success);
        System.assert(output[0].success == false);

        String chatkey = createLiveChatTranscript();
        NF_AddUserInfo.Input input2 = new NF_AddUserInfo.Input();
        input2.FirstName = 'Michael';
        input2.LastName = 'Curry';
        input2.Email = 'michael.curry@neuraflash.com';
        input2.chatKey = chatkey;

        List<NF_AddUserInfo.Output> output2 = NF_AddUserInfo.addUserInfo(new List<NF_AddUserInfo.Input>{input2});
        System.assert(output2[0].success == true);

        Test.stopTest();
    }


    static String createLiveChatTranscript(){
        LiveChatTranscript t1 = new LiveChatTranscript();
        LiveChatVisitor lcv = new LiveChatVisitor();
        try{
            insert lcv;
        }catch(Exception e){

        }
        t1.LiveChatVisitorId = lcv.Id;
        t1.C2CEmail__c = '';
        t1.C2CFirstName__c = '';
        t1.C2CLastName__c = '';
        t1.ChatKey = 'somechatkey';
        insert t1;

        return t1.ChatKey;
    }
}