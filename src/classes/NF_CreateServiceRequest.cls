/**
 * Created by mcasella on 2019-11-17.
 */

public with sharing class NF_CreateServiceRequest {
    public static String ENDPOINT = 'callout:Juniper_API_Gateway/v01/createsr';

    public class Input{
        @InvocableVariable(required=true)
        public String chatKey;

        @InvocableVariable(required=false)
        public String email;

        @InvocableVariable(required=false)
        public String accountId;
    }
    public class Output{
        @InvocableVariable
        public String response;

        @invocableVariable
        public Integer returnCode;
    }

    @InvocableMethod(label='Neuraflash - Call Create Service Request')
    public static List<Output> createServiceRequest(List<Input> input) {
        Integer returnCode = nf_ApiController.API_RESULT.FAILURE;

        List<Output> output = new List<Output>();
        Output out = new Output();

        String chatkey   = input[0].chatKey;
        String email     = input[0].email;
        String accountId = input[0].accountId;

        try {
            if(String.isEmpty(email) || String.isEmpty(accountId)){
                String error = 'Create Service Request: Missing required parameters';
                NF_ApiController.addApiError(chatkey, error);
                throw new  NF_ApiController.ConfigurationException(error);
            }

            String firstUserInput = '';
            String caseNumber = getCaseNumber(chatKey);

            firstUserInput = NF_Util.getFirstUserInput(chatkey);
            if (String.isEmpty(firstUserInput)) {
                firstUserInput = 'Chatbot Case';
            }

            String saltedId = '';
            LiveChatTranscript transcript = NF_Util.getLiveChatTranscript(chatKey);
            if(transcript != null) {
                saltedId = transcript.Salted_ID__c;
            }

            //call queueable class
            System.enqueueJob(new NF_CreateServiceRequestQueueable(accountId, email, firstUserInput, caseNumber, chatKey, saltedId));
            returnCode = nf_ApiController.API_RESULT.SUCCESS;
            /*NF_ApiController apiController = new NF_ApiController();
            apiController.initialize(chatKey);

            NF_CreateServiceRequestRequest apiRequest = new NF_CreateServiceRequestRequest(chatKey);
            apiRequest.createSRRequest.contact.contactEmail = email;
            apiRequest.createSRRequest.contact.accountID = accountId;
            apiRequest.createSRRequest.caseInformation.synopsis = firstUserInput;
            apiRequest.createSRRequest.caseInformation.problemDescription = firstUserInput;
            apiRequest.createSRRequest.caseInformation.customerCaseNumber = caseNumber;

            System.debug('>>NF_CreateServiceRequest apiRequest=' + apiRequest);

            NF_CreateServiceRequestResponse apiResponse = (NF_CreateServiceRequestResponse) apiController.sendApiRequest(apiRequest, NF_CreateServiceRequestResponse.class, true);
            System.debug('>>NF_CreateServiceRequest response=' + apiResponse);

            if (apiResponse != null) {
                out.response = JSON.serialize(apiResponse.createSRResponse);
                System.debug('>>NF_CreateServiceRequest out.response=' + out.response);

                if (apiResponse.isError() == false) {
                    returnCode = nf_ApiController.API_RESULT.SUCCESS;
                }
            }*/
        } catch (Exception e) {
            NF_EinsteinBotLogger.populatedLog(e);
        }

        out.returnCode = returnCode;

        output.add(out);
        return output;
    }

    private static String getCaseNumber(String chatKey){
        String caseNumber = '';

        Case[] cases = [SELECT Id, CaseNumber
                        FROM Case
                        WHERE LCT_ChatKey__c = :chatkey
                        LIMIT 1];

        return cases.size() > 0 ? cases[0].CaseNumber : 'No Case #';
    }
}