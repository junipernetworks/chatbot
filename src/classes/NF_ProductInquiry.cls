/**
 * Created by Jyotika on 11/21/19.
 */
public class NF_ProductInquiry {
    
    public class Input{
        @InvocableVariable(required=true)
        public String productName;
    }
    public class Output{
        @InvocableVariable(required=true)
        public String productdetailLink;
    }

    public static String addUTMparams(String url, String product){
        // Add the Juniper UTM params into bot served urls
        // example url https://juniper.net/whatever?utm_medium=website&utm_source=chatbot&utm_campaign=Link_Name

        String campaign = product.replace(' ', '_');

        String urlOut = url + '?utm_medium=website&utm_source=chatbot&utm_campaign=' + campaign;

        return urlOut;

    }
    
    @InvocableMethod(label='Neuraflash - Product Detail Link')
    public static List<Output> getProductDetailLink(List<Input> params){
        System.debug('>>> DEBUG START');
        String selectedProduct = params[0].productName;
        System.debug('>>> ProductName =' + selectedProduct);
        List<Output> result = new List<Output>();
        Output out = new Output();
		try{
            NF_Bot_Product_Detail__mdt productdetailMetadata = [SELECT id, DeveloperName, MasterLabel, Suite_Url__c
                                                                FROM NF_Bot_Product_Detail__mdt
                                                                WHERE MasterLabel =: params[0].productName];
            if(productdetailMetadata != null){
                String productlink = productdetailMetadata.Suite_Url__c;
                productlink = addUTMparams(productlink, selectedProduct);
                out.productdetailLink = productlink;
            	result.add(out);
            }
            else {
                String productlink = 'NF_FAIL';
                out.productdetailLink = productlink;
                result.add(out);
            }
            
        }catch(Exception e){
            System.debug('Exception: ' + e);
            String productlink = 'NF_FAIL';
            out.productdetailLink = productlink;
            result.add(out);
        }
        return result;
    }

	

}