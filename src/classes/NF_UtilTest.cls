/**
 * Created by NeuraFlash LLC on 11/5/19.
 */

@IsTest
public class NF_UtilTest {
    @IsTest
    static void test1() {
        System.assert(NF_Util.getDateTime().length() > 0);
    }

    @IsTest
    static void test3(){
        System.assert(NF_Util.getGroup('none') == null);
    }

    @IsTest
    static void getTranscript(){
        String chatKey = '123';

        Test.startTest();
        LiveChatTranscript t1 = insertLiveChatTranscript(chatKey, 'https://support.juniper.net', null);
        Test.stopTest();

        LiveChatTranscript t2 = NF_Util.getLiveChatTranscript(chatKey);

        System.assertEquals(t1.Id, t2.Id);
    }

    @isTest
    static void padDTElement(){
        NF_Util.padDTElement(null, 5);
    }

    @isTest
    static void generateTransactionId(){
        //36 chars
        String chatKey = '689cdd09-a4b4-4c51-b399-e716b57e3457';
        ebotdata__Bot_Session__c session = insertBotSession(chatKey);

        session.API_Request_Index__c = 999;
        update session;

        String guid = NF_Util.generateTransactionId(chatKey);
        System.assert(String.isNotEmpty(guid));
        System.assert(guid.length() <= 40);
    }

    @IsTest
    static void testMisc(){
        NF_Util.getCcareRecordType();
        NF_Util.getPlatformIntegrationUser();
        NF_Util.getRecordTypeId('test');
    }

    @IsTest
    static void testChatLogRelated(){
        ebotdata__Bot_Session__c session = insertBotSession('abc');

        ebotdata__Bot_Chat_Log__c log = insertChatLog(session);

        String lbi = NF_Util.getLastBusinessIntent('abc');
        String ufi = NF_Util.getFirstUserInput('abc');

        NF_Util.getBusinessChatLogs('abc');

        System.assertEquals(log.ebotdata__Intent_Name__c, lbi);
        System.assertEquals(log.ebotdata__Current_Utterance__c, ufi);
    }

    @IsTest
    static void updateNewUniqueId(){
        Integer MAX_LENGTH = 40;
        String guid = 'b1f97db3-b179-411c-ac15-77a079ad17f8-02';
        String unique = 'abc';
        String expected = ('b1f97db3-b179-411c-ac15-77a079ad17f8-02' + unique).substring(0, MAX_LENGTH);

        String newGuid = NF_Util.updateUniqueId(guid, unique);
        System.assertEquals(expected, newGuid);
        System.assert(newGuid.length() <= MAX_LENGTH);
    }

    /*
      Added a helper method to make our 'unique' ID more unique
     */
    @IsTest
    static void noUpdateNewUniqueId(){
        System.assertEquals('', NF_Util.updateUniqueId(null, ''));

        String guid = 'abc';
        System.assertEquals(guid, NF_Util.updateUniqueId(guid, ''));
    }


        public static LiveChatTranscript insertLiveChatTranscript(String chatKey, String pageUrl, String contactId){
        LiveChatVisitor v = new LiveChatVisitor();
        insert v;
        LiveChatTranscript transcript = new LiveChatTranscript();
        transcript.LiveChatVisitorId= v.Id;

        if(String.isNotEmpty(chatKey)){
            transcript.ChatKey = chatKey;
        }

        if(String.isNotEmpty(pageUrl)){
            transcript.Page_URL__c = pageUrl;
        }

        if(String.isNotEmpty(contactId)){
            transcript.ContactId = contactId;
        }

        insert transcript;

        return transcript;
    }

    public static ebotdata__Bot_Session__c insertBotSession(String chatKey){
        ebotdata__Bot_Session__c session = new ebotdata__Bot_Session__c();
        session.ebotdata__Live_Agent_Session_Id__c = chatKey;
        insert session;
        return session;
    }

    public static ebotdata__Bot_Chat_Log__c insertChatLog(ebotdata__Bot_Session__c session){
        ebotdata__Bot_Chat_Log__c log = new ebotdata__Bot_Chat_Log__c();
        log.ebotdata__Bot_Session__c = session.Id;
        log.ebotdata__Intent_Name__c = 'bus_testIntent_end';
        log.ebotdata__Current_Utterance__c = 'firstUserInput';
        insert log;
        return log;
    }

    public static Bot_API_Settings__c insertApiSettings(boolean hasToken){
        Bot_API_Settings__c settings = new Bot_API_Settings__c();

        settings.Name = 'JuniBot CCare';
        settings.Client_Secret__c = 'secret';
        settings.Client_Id__c = 'clientId';

        if(hasToken){
            settings.Authentication_Token_Type__c = 'Bearer';
            settings.Access_Token__c = '1234';
        }

        insert settings;

        return settings;
    }

    public static HttpResponse getHttpResponse(Integer statusCode, String body){
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        if(body != null){
            response.setBody(body);
        }
        response.setStatusCode(statusCode);

        return response;
    }

//    public static Contact insertContact(){
//        Account a1 = new Account(SourceSystem__c = 'Siebel', PartyUid__c = 'PartyUID', Name = 'Test Account 1',
//                JUNSalesManagedUsageFlag__c = True, BillingAddress_country__c = 'India', BillingAddress_city__c = 'Pune',
//                BillingAddress_AddressLine1__c = 'Address Line 1', BillingAddressState__c = 'MH', BillingAddressPostalCode__c = '411001');
//        insert a1;
//
//        Contact c1 = new Contact(AccountId = a1.id, SourceSystem__c = 'Siebel', PartyUid__c = 'PartyUID', FirstName = 'Test ', LastName = 'Contact 1',
//        Phone = '1234567890', Email = 'abc@xyz.com', PersonalStreetAddress__c = 'Street', PersonalCity__c = 'Dallas',
//                PersonalCountry__c = 'United States', PersonalState__c = 'Texas', PersonalPostalCode__c = 'DLS144Z');
//
//        insert c1;
//
//        return c1;
//    }

}