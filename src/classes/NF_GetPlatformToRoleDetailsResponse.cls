public with sharing class NF_GetPlatformToRoleDetailsResponse extends NF_ApiResponse {

    public QueryPlatformToRoleDetailsResponse queryPlatformToRoleDetailsResponse{ get;set; }

    public class QueryPlatformToRoleDetailsResponse extends ApiResponse{
        public String customerUniqueTransactionID { get; set; }
        public String responseDateTime { get; set; }
        public String customerSourceID { get; set; }
        public String roleOperator { get; set; }
        public Roles[] roles { get; set; }
    }

    public class Roles{
        public String roleName { get; set; }
    }
    public override Boolean isError(){
        return queryPlatformToRoleDetailsResponse == null || queryPlatformToRoleDetailsResponse.isError();
    }

    public override Integer getStatusCode(){
        return queryPlatformToRoleDetailsResponse != null ? queryPlatformToRoleDetailsResponse.getStatusCode() : -1;
    }

    public override Integer getErrorCode(){
        return queryPlatformToRoleDetailsResponse != null ? queryPlatformToRoleDetailsResponse.getErrorCode() : -1;
    }

    public override String getDescription(){
        return queryPlatformToRoleDetailsResponse != null ? queryPlatformToRoleDetailsResponse.getDescription() : error;
    }

}