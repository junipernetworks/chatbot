/**
 * Created by NeuraFlash LLC on 11/5/19.
 */

@IsTest
private class NF_UpdateUserInfoTest {
    @IsTest
    static void test1() {
        Test.startTest();
        String transid = createLiveChatTranscript();

        updateUserInfo(transid);
        Test.stopTest();
    }

    static void updateUserInfo(String transid){
        NF_UpdateUserInfo.Input testInput = new NF_UpdateUserInfo.Input();
        //testInput.description = 'desc';
        //testInput.subject = 'subj';
        testInput.chatKey = transid;
        testInput.botName = 'botty';

        NF_UpdateUserInfo.Output result = NF_UpdateUserInfo.updateUserInfo(new List<NF_UpdateUserInfo.Input>{testInput})[0];

        System.assert(result != null);
    }
    static String createLiveChatTranscript(){
        LiveChatTranscript t1 = new LiveChatTranscript();
        LiveChatVisitor lcv = new LiveChatVisitor();
        try{
            insert lcv;
        }catch(Exception e){

        }
        t1.LiveChatVisitorId = lcv.Id;
        t1.C2CEmail__c = 'xxx@xxx.com';
        t1.C2CFirstName__c = 'xxx';
        t1.C2CLastName__c = 'xxx';
        t1.ChatKey = 'somechatkey';
        insert t1;

        return t1.ChatKey;
    }
}