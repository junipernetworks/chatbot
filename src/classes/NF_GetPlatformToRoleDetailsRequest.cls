public with sharing class NF_GetPlatformToRoleDetailsRequest extends NF_ApiRequest {
    public NF_GetPlatformToRoleDetailsRequest(String chatKey, String tracUniqueParam){
        queryPlatformToRoleDetailsRequest = new QueryPlatformToRoleDetailsRequest();
        queryPlatformToRoleDetailsRequest.caseInformation = new CaseInformation();
        //the second of 3 sequential API calls in Software Downloads, make sure the ID is unique
        queryPlatformToRoleDetailsRequest.caseInformation.customerUniqueTransactionID = NF_Util.updateUniqueId(NF_Util.generateTransactionId(chatKey), tracUniqueParam);
        queryPlatformToRoleDetailsRequest.platformDetails = new PlatformDetails();
    }

    public void setPlatformShortName(String platformName){
        queryPlatformToRoleDetailsRequest.platformDetails.platformShortName = platformName;
    }

    public void setOSId(String osId){
        queryPlatformToRoleDetailsRequest.platformDetails.osId = osId;
    }

    public override String getApiName(){
        return 'Platform to Role Details';
    }

    public override String getEndpoint(){
        return 'callout:Juniper_API_Gateway/v01/getplatformtoroledetails';
    }

    public override String getTransactionId(){
        return queryPlatformToRoleDetailsRequest != null ? queryPlatformToRoleDetailsRequest.caseInformation.customerUniqueTransactionID : '';
    }

    public QueryPlatformToRoleDetailsRequest queryPlatformToRoleDetailsRequest;

    public class QueryPlatformToRoleDetailsRequest extends AbstractRequest{
        public CaseInformation caseInformation { get; set; }
        public PlatformDetails platformDetails { get; set; }
    }
    public class CaseInformation{
        public String customerSourceID = 'juni-chatbot';
        public String customerUniqueTransactionID { get; set; }
    }
    public class PlatformDetails{
        public String platformShortName { get; set; }
        public String osId { get; set; }
    }

}