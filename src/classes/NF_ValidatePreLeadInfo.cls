/**
 * Created by mcurry on 2019-11-20.
 */

public with sharing class NF_ValidatePreLeadInfo {
    public class Input{
        @InvocableVariable(required=true)
        public String FullNameString;

        @InvocableVariable(required=true)
        public String EmailString;

    }
    public class Output{
        @InvocableVariable(required=true)
        public String FirstName = '';

        @InvocableVariable(required=true)
        public String LastName = '';

        @InvocableVariable(required=true)
        public String Email = '';

        @InvocableVariable(required=true)
        public Boolean isValid = null;
    }


    @InvocableMethod(label='Neuraflash - Validate PreLead Info')
    public static List<Output> validateInputs(List<Input> input){
        List<Output> result = new List<Output>();
        Output out = new Output();
        System.debug('>> NF_ValidatePreLeadInfo : Starting');
        System.debug('>> NF_ValidatePreLeadInfo : FullName Input: ' + input[0].FullNameString);
        System.debug('>> NF_ValidatePreLeadInfo : Email Input: ' + input[0].EmailString);
        if (String.isBlank(input[0].EmailString)){
            System.debug('>> NF_ValidatePreLeadInfo : Email is blank');
            out.isValid = false;
            result.add(out);
            return result;
        }
        if (!validateEmail(input[0].EmailString)){
            // invalid email address provided
            System.debug('>> NF_ValidatePreLeadInfo : Email Validation Fail');
            out.isValid = false;
            result.add(out);
            return result;
        } else {
            System.debug('>> NF_ValidatePreLeadInfo : Email Validation Passed');
            out.isValid = true;
            out.Email = input[0].EmailString;
        }

        String[] nameSplit = input[0].FullNameString.split(' ');
        if (nameSplit.size() == 0){
            // no name was given
            out.FirstName = 'Unknown';
        }
        else if (nameSplit.size() == 2){
            out.FirstName = nameSplit[0];
            out.LastName = nameSplit[1];
        }
        else if (nameSplit.size() == 1){
            out.FirstName = nameSplit[0];
            out.LastName = '{{undefined}}';
        }
        else {
            // we have more than two names
            out.FirstName = nameSplit[0];
            for (Integer i = 1; i < nameSplit.size(); i++){
                out.LastName = out.LastName + nameSplit[i];
            }
        }

        result.add(out);
        return result;

    }

    public static Boolean validateEmail(String email) {
        Boolean res = true;


        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: <a href="http://www.regular-expressions.info/email.html" target="_blank" rel="nofollow">http://www.regular-expressions.info/email.html</a>
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);

        if (!MyMatcher.matches())
            res = false;
        return res;
    }
}