/**
 * Created by mcasella on 2/22/20.
 */

public without sharing class NF_ApiLog {
    private static String RESULT_SUCCESS = 'Success';
    private static String RESULT_FAILURE = 'Failure';
    private static String RESULT_TIMEOUT = 'Timeout';

    public String name {get; set;}
    public String chatKey {get; set;}
    public String result {get; set;}
    public Integer timeout{get; set;}
    public Long processingTimeMs{get; set;}
    public DateTime startTime {get; set;}
    public DateTime endTime {get; set;}
    public String uniqueTransactionId;
    public String endpoint;
    public String errorMessage = '';
    public Boolean isGetUserProfileRequest = false;

    private NF_ApiRequest request {get; set;}
    private NF_ApiResponse response {get; set;}

    public String requestJSON {get; set;}
    public String responseJSON {get; set;}

    public NF_ApiLog(NF_ApiRequest request){
        if(request == null){
            return;
        }

        this.name = request.getApiName();
        this.isGetUserProfileRequest = request instanceof NF_GetUserProfileRequest;

        this.request = request;
        if(this.isGetUserProfileRequest == false){
            this.requestJSON = JSON.serialize(request);
        }

        this.startTime = System.now();

        this.uniqueTransactionId = request.getTransactionId();
        this.endpoint = request.getEndpoint();
    }

    public void setResponse(NF_ApiResponse response){
        if(response == null){
            return;
        }

        this.response = response;
        if(this.isGetUserProfileRequest == false) {
            this.responseJSON = JSON.serialize(response);
        }

        this.endTime = System.now();
        this.result = response.isError() == false ? RESULT_SUCCESS : RESULT_FAILURE;
        this.errorMessage = response.getDescription();

        if(this.startTime != null && this.endTime != null){
            this.processingTimeMs = this.endTime.getTime() - this.startTime.getTime();
        }
    }

    public void setException(Exception e){
        if(e == null){
            return;
        }

        this.result = 'Read timed out' == e.getMessage() ? RESULT_TIMEOUT : RESULT_FAILURE;
        this.errorMessage = e.getMessage();
    }
}