public class NF_LogAPICalls {

    public static void logApiCall(NF_ApiLog apiLog){
        if(apiLog == null || String.isEmpty(apiLog.chatKey)){
            System.debug('NF_LogApiCalls: unable to log apiLog='+apiLog);
            return;
        }

        logApiCallAsync(apiLog.chatKey, apiLog.isGetUserProfileRequest, JSON.serialize(apiLog));
    }

    @Future
    public static void logApiCallAsync(String chatKey, Boolean isGetUserProfileRequest, String apiLogString){
        try {
            ebotdata__Bot_Session__c[] botSessions = [
                    SELECT Id, Name, API_Log_Get_User_Profile__c, Api_Logs__c
                    FROM ebotdata__Bot_Session__c
                    WHERE ebotdata__Live_Agent_Session_Id__c = :chatKey
                    LIMIT 1
            ];

            System.debug('>>botSessions' + botSessions);
            if (botSessions.size() > 0 && String.isNotEmpty(apiLogString)) {
                System.debug('NF_LogApiCalls: apiLogString='+apiLogString);

                ebotdata__Bot_Session__c session = botSessions[0];

                //we want to log this API request specially, as it's the critical API call at from the start of the session
                if (isGetUserProfileRequest) {
                    session.API_Log_Get_User_Profile__c = apiLogString;
                }

                //and then log all other API calls in this list
                List<NF_ApiLog> logs = null;

                if (String.isEmpty(session.Api_Logs__c)) {
                    logs = new List<NF_ApiLog>();
                } else {
                    logs = (List<NF_ApiLog>) JSON.deserialize(session.Api_Logs__c, List<NF_ApiLog>.class);
                }

                NF_ApiLog apiLog = (NF_ApiLog) JSON.deserialize(apiLogString, NF_ApiLog.class);
                System.debug('NF_LogApiCalls: deserialized apiLog='+apiLog);
                logs.add(apiLog);
                session.Api_Logs__c = JSON.serialize(logs);

                update session;
            }
            else{
                System.debug('NF_LogApiCalls: unable to log apiLogString='+apiLogString);
            }
        }
        catch(Exception e){
            NF_EinsteinBotLogger.logException(e);
        }
    }
}