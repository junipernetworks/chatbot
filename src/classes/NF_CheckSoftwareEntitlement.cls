/**
 * @File Name          : NF_CheckSoftwareEntitlement.cls
 * @Description        : This class will call checksoftwareentitlement api as well as enqueue two api calls based on success code.
 * @Author             : Manishi Kalra
 * @Created By         : NeuraFlash, LLC
**/

public with sharing class NF_CheckSoftwareEntitlement {

    public static String ENDPOINT = 'callout:Juniper_API_Gateway/v01/checksoftwareentitlement';

    public class Input{
        @InvocableVariable(required=true)
        public String chatKey;

        @InvocableVariable(required=false)
        public String email;

        @InvocableVariable(required=true)
        public String ssrnOrserialnumber;

        @InvocableVariable(required=false)
        public String contactId;
    }
    public class Output{
        @InvocableVariable
        public String response;

        @InvocableVariable
        public Integer returnCode;

        @InvocableVariable
        public Boolean isMultipleOSReturned;

        @InvocableVariable
        public String failReason;

        @InvocableVariable
        public String osID;

        @InvocableVariable
        public String platformName;
    }

    @InvocableMethod(label='Neuraflash - Check Software Entitlement')
    public static List<Output> checkSoftwareEntitlement(List<Input> input) {
        Integer returnCode = nf_ApiController.API_RESULT.FAILURE;

        List<Output> output = new List<Output>();
        Output out = new Output();
        out.isMultipleOSReturned = false;
        out.failReason = 'generic';

        try {
            String chatkey = input[0].chatKey;
            String email   = input[0].email;
            String ssrnOrserialnumber = input[0].ssrnOrserialnumber.toUpperCase();
            String contactId = input[0].contactId;

            NF_ApiController apiController = new NF_ApiController();
            apiController.initialize(chatKey);

            NF_CheckSoftwareEntitlementRequest apiRequest = new NF_CheckSoftwareEntitlementRequest(chatKey);
            apiRequest.swEntitlementCheckRequest.ssrnOrserialnumber = ssrnOrserialnumber;

            apiRequest.setEmail(email);
            apiRequest.setContactId(contactId);

            System.debug('>>NF_CheckSoftwareEntitlementRequest apiRequest=' + apiRequest);

            NF_ApiController.ToReplace unreserve = new NF_ApiController.ToReplace();
            unreserve.replaced = 'dateTime';
            unreserve.replacedWith = 'dateTime_c';

            apiController.toReplaces.add(unreserve);

            NF_CheckSoftwareEntitlementResponse apiResponse = (NF_CheckSoftwareEntitlementResponse) apiController.sendApiRequest(apiRequest, NF_CheckSoftwareEntitlementResponse.class, true);
            System.debug('>>NF_CheckSoftwareEntitlementResponse response=' + apiResponse);

            if (apiResponse != null) {
                try {
                    out.response = JSON.serialize(apiResponse.swEntitlementCheckResponse);
                    if (apiResponse.isError() == false) {
                        //out.response = JSON.serialize(apiResponse.swEntitlementCheckResponse);
                        System.debug('>>NF_CheckSoftwareEntitlementResponse out.response=' + out.response);
                        List<NF_CheckSoftwareEntitlementResponse.OSDetails> osDetailsList = apiResponse.SWEntitlementCheckResponse.osDetails;
                        System.debug('NF_CheckSoftwareEntitlementResponse>>> osDetailsList'+osDetailsList);
                        if(osDetailsList.size() >1) {
                            //more than one OS Detail is returned, set this parameter to the bot.
                            out.isMultipleOSReturned = true;
                        }
                        if(!osDetailsList.isEmpty()) {
                            out.osID = osDetailsList[0].osId;
                            out.platformName = osDetailsList[0].modelShortName;
                            String saltedId = '';
                            LiveChatTranscript transcript = NF_Util.getLiveChatTranscript(chatKey);
                            if(transcript != null) {
                                saltedId = transcript.Salted_ID__c;
                            }
                            //call queueable class
                            if(!Test.isRunningTest()) {
                                System.enqueueJob(new NF_QueueablePlatformRoleAPI(chatKey, saltedId, out.platformName, out.osID, email));
                            }
                            
                        }
                        
                        returnCode = nf_ApiController.API_RESULT.SUCCESS;
                        out.failReason = 'success';
                        
                    }
                    else{
                        //error code = 814: IP
                        //error code = 815: DNE
                        //error code = 862: Not Entitled
                        System.debug(apiResponse.getErrorCode());
                        System.debug(apiResponse.getDescription());
                        if(apiResponse.getDescription().contains('862')) {
                            out.failReason = 'Not Entitled';
                        }
                        else if(apiResponse.getDescription().contains('815')) {
                            out.failReason = 'DNE';
                        }
                        else if(apiResponse.getDescription().contains('814')) {
                            out.failReason = 'IP';
                        }
                        returnCode = nf_ApiController.API_RESULT.FAILURE;
                    }
                    
                    
                }catch(Exception e){
                    System.debug('NF_CheckSoftwareEntitlementResponse Exception>>>>'+e.getMessage() + e.getStackTraceString()); 
                    returnCode = nf_ApiController.API_RESULT.FAILURE;
                }
            }
            else{
                System.debug('Entered in else');
                returnCode = nf_ApiController.API_RESULT.FAILURE;
            }
            
        }
        catch(Exception e){
            System.debug('NF_CheckSoftwareEntitlementResponse Exception>>>>'+e.getMessage() + e.getStackTraceString()); 
            NF_EinsteinBotLogger.populatedLog(e);
        }

        out.returnCode = returnCode;

        output.add(out);
        return output;
    }

}