public with sharing class NF_CheckSoftwareEntitlementRequest extends NF_ApiRequest {
    public NF_CheckSoftwareEntitlementRequest(String chatKey){
        swEntitlementCheckRequest = new SWEntitlementCheckRequest();
        swEntitlementCheckRequest.caseInformation = new CaseInformation();

        //the first of 3 sequential API calls in Software Downloads, make sure the ID is unique
        swEntitlementCheckRequest.caseInformation.customerUniqueTransactionID = NF_Util.updateUniqueId(NF_Util.generateTransactionId(chatKey), 'a');
        swEntitlementCheckRequest.contact = new Contact();
    }

    public void setContactId(String id){
        swEntitlementCheckRequest.contact.contactId = id;
    }

    public void setEmail(String email){
        swEntitlementCheckRequest.contact.contactEmail = email;
    }

    public override String getApiName(){
        return 'Check SoftwareEntitlement';
    }

    public override String getEndpoint(){
        return 'callout:Juniper_API_Gateway/v01/checksoftwareentitlement';
    }

    public override String getTransactionId(){
        return swEntitlementCheckRequest != null ? swEntitlementCheckRequest.caseInformation.customerUniqueTransactionID : '';
    }

    public SWEntitlementCheckRequest swEntitlementCheckRequest;

    public class Contact {
        public String contactId { get; set; }
        public String contactEmail { get; set; }
    }

    public class SWEntitlementCheckRequest extends AbstractRequest{
        public CaseInformation caseInformation { get; set; }
        public String ssrnOrserialnumber { get; set; }
        public Contact contact { get; set; }
    }
    public class CaseInformation{
        public String customerSourceID = 'juni-chatbot';
        public String customerUniqueTransactionID { get; set; }
    }

}