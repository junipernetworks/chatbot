public with sharing class NF_QuerySR_RMAList {
    public static String ENDPOINT = 'callout:Juniper_API_Gateway/v01/findrecentcases';

    public class Input{
        @InvocableVariable(required=false)
        public String chatKey;

        @InvocableVariable(required=false)
        public String email;

        @InvocableVariable(required=false)
        public String contactId;

        @InvocableVariable(required=false)
        public String isSRRequest;  //String so that we can pass in a static value

        @InvocableVariable(required=false)
        public String isRMARequest;  //String so that we can pass in a static value
    }

    public class Output{
        @InvocableVariable
        public String response;

        @invocableVariable
        public Integer returnCode;

        @InvocableVariable
        public String carouselPayload;
		
        /* added for [Juni CECCB-53] : need rma number on the bot side */
        @InvocableVariable
        public String returnRMANumber;
    }

    @InvocableMethod(label='Neuraflash - Query SR RMA List')
    public static List<Output> querySRRMAList(List<Input> input) {
        String carouselPayload;
        NF_QueryRMAResponse apiRMAResponse;
        NF_QuerySRResponse apiSRResponse;

        List<Output> output = new List<Output>();
        Output out = new Output();
        out.returnCode = nf_ApiController.API_RESULT.FAILURE;

        try {
            String chatkey = input[0].chatKey;
            String email   = input[0].email;
            Boolean isSR = Boolean.valueOf(input[0].isSRRequest);
            Boolean isRMA = Boolean.valueOf(input[0].isRMARequest);
            String contactId = input[0].contactId;

            NF_ApiController apiController = new NF_ApiController();
            apiController.initialize(chatKey);

            NF_QuerySR_RMAListRequest apiRequest = new NF_QuerySR_RMAListRequest(chatKey);
            apiRequest.querySRRMAListDetailsRequest.srFlag = isSR;
            apiRequest.querySRRMAListDetailsRequest.rmaFlag = isRMA;
            apiRequest.querySRRMAListDetailsRequest.caseInformation.serviceRequestNumber = '';
            apiRequest.querySRRMAListDetailsRequest.caseInformation.customerCaseNumber = '';

            apiRequest.setEmail(email);
            apiRequest.setContactId(contactId);
            apiRequest.setAccountId(contactId);

            System.debug('>>NF_QuerySR_RMAListRequest apiRequest=' + apiRequest);
            NF_QuerySR_RMAListResponse apiSRRmaResponse = (NF_QuerySR_RMAListResponse) apiController.sendApiRequest(apiRequest, NF_QuerySR_RMAListResponse.class, true);
            System.debug('>>NF_QuerySR_RMAListRequest response=' + apiSRRmaResponse);

            if (apiSRRmaResponse != null) {
                if (apiSRRmaResponse.isNotFound()){
                    out.returnCode = nf_ApiController.API_RESULT.NOT_FOUND;
                }
                else if (apiSRRmaResponse.isError() == false) {
                    out.returnCode = nf_ApiController.API_RESULT.SUCCESS;
                    if(isSR) {
                        out.response = JSON.serialize(apiSRRmaResponse.querySRRMAListDetailsResponse);
                        System.debug('>>NF_QuerySR out.response=' + out.response);
                        carouselPayload = NF_DynamicCarouselContentFactory.createNF_QuerySRListCarousel(apiSRRmaResponse);
                        out.carouselPayload = carouselPayload;
                        System.debug('>>>carouselPayload'+carouselPayload);
                        ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatkey);
                        if(session != null){
                            session.CarouselContent__c = carouselPayload;
                            update session;
                            System.debug('Updated field>>>'+session.CarouselContent__c);
                        }
                    }
                    if(isRMA) {
                        out.response = JSON.serialize(apiSRRmaResponse.querySRRMAListDetailsResponse);
                        System.debug('>>NF_QueryRMA out.response=' + out.response);
                        out.returnRMANumber = apiSRRmaResponse.QuerySRRMAListDetailsResponse.rmaDetails[0].rmaNumber;
                        carouselPayload = NF_DynamicCarouselContentFactory.createNF_QueryRMAListCarousel(apiSRRmaResponse);
                        out.carouselPayload = NF_DynamicCarouselContentFactory.createNF_QueryRMAListCarousel(apiSRRmaResponse);
                        System.debug('carouselPayload>>>'+out.carouselPayload);
                        ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatkey);
                        if(session != null){
                            session.CarouselContent__c = carouselPayload;
                            update session;
                            System.debug('Updated field>>>'+session.CarouselContent__c);
                        }
                    }
                }
            }
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
        }
        System.debug('>>NF_QuerySR_RMAListRequest out.returnCode=' + out.returnCode);
        System.debug('>>NF_QuerySR_RMAListRequest output=' + output);

        output.add(out);
        return output;
    }
}