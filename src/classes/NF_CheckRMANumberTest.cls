/**
 * Created by mcasella on 2019-12-20.
 */

@isTest
public with sharing class NF_CheckRMANumberTest {

    static testMethod void testValid() {
        NF_CheckRMANumber.Input input = new NF_CheckRMANumber.Input();
        input.rmaNumber = 'R123123123';
        List<NF_CheckRMANumber.Input> inputs = new List<NF_CheckRMANumber.Input>{ input };

        NF_CheckRMANumber.Output output = NF_CheckRMANumber.validateRMANumber(inputs)[0];
        System.assertEquals(true, output.isValid);
    }

    static testMethod void testNlp() {
        NF_CheckRMANumber.Input input = new NF_CheckRMANumber.Input();
        String rmaNumber = 'R123123123';
        input.rmaNumber = 'my RMA number is '+rmaNumber;
        List<NF_CheckRMANumber.Input> inputs = new List<NF_CheckRMANumber.Input>{ input };

        NF_CheckRMANumber.Output output = NF_CheckRMANumber.validateRMANumber(inputs)[0];
        System.assertEquals(true, output.isValid);
        System.assertEquals(rmaNumber, output.rmaNumber);
    }

    static testMethod void testInvalid() {
        NF_CheckRMANumber.Input input = new NF_CheckRMANumber.Input();
        input.rmaNumber = 'abc123123';
        List<NF_CheckRMANumber.Input> inputs = new List<NF_CheckRMANumber.Input>{ input };

        NF_CheckRMANumber.Output output = NF_CheckRMANumber.validateRMANumber(inputs)[0];
        System.assertEquals(false, output.isValid);
    }
}