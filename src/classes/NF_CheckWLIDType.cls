/**
 * Created by NeuraFlash LLC on 10/31/19.
 */

public with sharing class NF_CheckWLIDType {

    public class Output {
        @InvocableVariable(required=true)
        public String id;

        @InvocableVariable(required=true)
        public String type;

        @InvocableVariable(required=true)
        public Boolean isValid;
    }

    @InvocableMethod(label='Neuraflash - Validate Welcome ID')
    public static List<Output> checkType(List<String> input) {
        List<Output> output = new List<Output>();
        Output out = new Output();
        out.isValid = false;

        String id = input.get(0);
        if(String.isNotEmpty(id)) {

            id = NF_CheckForIDK.checkForIdk(new List<String>{id})[0];

            if(id == 'I don\'t know'){
                out.isValid = true;
                out.id = id;
            }else {
                Pattern pat = Pattern.compile('.*?((?:1|6)\\d{7}).*');
                Matcher matcher = pat.matcher(id);

                if (matcher.matches()) {
                    out.isValid = true;
                    out.id = matcher.group(1);
                    System.debug('>>NF_CheckWLIDType.checkType: parsed a valid id=' + id);

                    if (out.id.substring(0, 1) == '1') {
                        out.type = 'Sales Order Number';
                    } else if (out.id.substring(0, 1) == '6') {
                        out.type = 'Contract ID';
                    }
                } else {
                    System.debug('>>NF_CheckWLIDType.checkType: no regex match');
                }
            }
        }

        output.add(out);

        return output;
    }
}