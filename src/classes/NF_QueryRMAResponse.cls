/**
 * Created by mcasella on 2019-11-18.
 */

public with sharing  class NF_QueryRMAResponse extends NF_ApiResponse {
    public QueryRMAResponse queryRMAResponse{ get;set; }

    public class QueryRMAResponse extends ApiResponse{
        public String customerUniqueTransactionID { get; set; }
        public String responseDateTime { get; set; }
        public String serviceRequestNumber { get; set; }
        public String customerSourceID { get; set; }
        public String customerCaseNumber { get; set; }
        public RMAInfo[] rmaInformation { get; set; }
    }

    public override Boolean isError(){
        return queryRMAResponse == null || queryRMAResponse.isError();
    }

    public override Integer getStatusCode(){
        return queryRMAResponse != null ? queryRMAResponse.getStatusCode() : -1;
    }

    public override Integer getErrorCode(){
        return queryRMAResponse != null ? queryRMAResponse.getErrorCode() : -1;
    }

    public override String getDescription(){
        return queryRMAResponse != null ? queryRMAResponse.getDescription() : error;
    }

    public class RMAInfo {
        public String rmaNumber { get; set; }
        public String rmaHeaderStatus { get; set; }
        public String rmaHeaderCreatedDate { get; set; }
        public RMAContact rmaContact { get; set; }
        public DefectiveItem[] defectiveItems { get; set; }
        public CEItem[] ceItems { get; set; }
        public ReplacementItem[] replacementItems {get; set; }
    }

    public class RMAContact{
        public String companyName { get; set; }
        public String contactName { get; set; }
        public String contactEmail { get; set; }
        public String telephoneCountryCode { get; set; }
        public String telephoneNumber { get; set; }
        public Address address { get; set; }
    }
    public class Address{
        public String address1 { get; set; }
        public String address2 { get; set; }
        public String city { get; set; }
        public String stateCode { get; set; }
        public String state { get; set; }
        public String countryCode { get; set; }
        public String country { get; set; }
        public String postalCode { get; set; }
    }

    public class DefectiveItem{
        public String itemNumber { get; set; }
        public String dateTime_c { get; set; }
        public String productID { get; set; }
        public String serialNumber { get; set; }
        public String trackingNumber { get; set; }
        public String trackingNumberUrl { get; set; }
        public String carrierDescription { get; set; }
        public String receivedDate { get; set; }
        public String defectiveItemStatus { get; set; }
        public String defectiveRmaType { get; set; }
        public String defectiveRmaSubType { get; set; }
    }

    public class CEItem{
        public String defectiveItemNumber { get; set; }
        public String productID { get; set; }
        public String engineerStatus { get; set; }
        public String vendorName { get; set; }
        public String vendorPhone { get; set; }
        public String serviceRequestedDate { get; set; }
        public String actualServicedDate { get; set; }
        public String contactedDate { get; set; }
        public String estimatedArrivalDate { get; set; }
    }
    
    //added By Manishi to match the QeryRMA response
    public class ReplacementItem {
        public String itemNumber { get; set; }
        public String defectiveItemNumber { get; set; }
        public String productID { get; set; }
        public String serialNumber { get; set; }
        public String carrierDescription { get; set; }
        public String shipDate { get; set; }
        public String shipmentServiceLevel { get; set; }
        public String trackingNumber { get; set; }
        public String replacementStatus { get; set; }
        public String receivedDate { get; set; }
        public String receivedBy { get; set; }
        public String replacementRmaType { get; set; }
        public String replacementRmaSubType { get; set; }
        public String trackingNumberUrl { get; set; }
    }
}