public with sharing class NF_AssignRole {
    public static String ENDPOINT = 'callout:Juniper_API_Gateway/v01/assignrole';
    
    public class Input{
        @InvocableVariable(required=true)
        public String chatKey;
        
        @InvocableVariable(required=false)
        public String email;
        
        @InvocableVariable(required=false)
        public String roles;
        
    }
    public class Output{
        @InvocableVariable
        public String response;
        
        @invocableVariable
        public Integer returnCode;
        
        @invocableVariable
        public Boolean isAgreementSigned;
    }
    @InvocableMethod(label='Neuraflash - Assign Role')
    public static List<Output> assignRole(List<Input> input) {
        Integer returnCode = nf_ApiController.API_RESULT.FAILURE;
        
        List<Output> output = new List<Output>();
        Output out = new Output();
        out.isAgreementSigned = false;
        try {
            String chatkey = input[0].chatKey;
            String email   = input[0].email;
            String roles = input[0].roles;
            system.debug('roles->'+roles);
            //Start - Added by ashwini for jira ticket https://jira.junipercloud.net/it/browse/CECCB-128
            If(roles.startsWithIgnoreCase('access')){
                JDA_assign_role__c JDAroles = JDA_assign_role__c.getvalues(roles);
                system.debug('JDAroles->->'+JDAroles);
                roles = JDAroles.JDA_Role_assign_API__c;
            }
            //End
            //roles = 'SW-EVAL-VQFX';
            system.debug('initially Chatkey==>>' +chatKey);
            system.debug('initially email==>>' +email);
            system.debug('initially roles==>>' +roles);
            
            NF_ApiController apiController = new NF_ApiController();
            apiController.initialize(chatKey);
            
            NF_AssignRoleRequest apiRequest = new NF_AssignRoleRequest(chatKey, 'c');
            
            apiRequest.setEmail(email);
            system.debug('email==>' +email);
            apiRequest.setRoles(roles);
            system.debug('Roles@@@==>' +roles);
            
            System.debug('>>NF_AssignRoleRequest apiRequest=' + apiRequest);
            
            NF_ApiController.ToReplace unreserve = new NF_ApiController.ToReplace();
            unreserve.replaced = 'dateTime';
            unreserve.replacedWith = 'dateTime_c';
            
            apiController.toReplaces.add(unreserve);
            
            NF_AssignRoleResponse apiResponse = (NF_AssignRoleResponse) apiController.sendApiRequest(apiRequest, NF_AssignRoleResponse.class, true);
            System.debug('>>NF_AssignRoleResponse response=' + apiResponse);
            
            if (apiResponse != null) {
                try {
                    out.response = JSON.serialize(apiResponse.assignRoleResponse);
                    if (apiResponse.isError() == false) {
                        //out.response = JSON.serialize(apiResponse.assignRoleResponse);
                        System.debug('>>NF_AssignRoleResponse out.response=' + out.response);
                        out.isAgreementSigned = (apiResponse.assignRoleResponse.encryption == 'YES') ? true : false;
                        returnCode = nf_ApiController.API_RESULT.SUCCESS;
                    }
                    else{
                        returnCode = nf_ApiController.API_RESULT.FAILURE;
                    }
                    
                    
                }catch(Exception e){
                    System.debug('NF_AssignRoleResponse Exception>>>>'+e.getMessage() + e.getStackTraceString()); 
                    returnCode = nf_ApiController.API_RESULT.FAILURE;
                }
            }
            else{
                System.debug('Entered in else');
                returnCode = nf_ApiController.API_RESULT.FAILURE;
            }
            
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
        }
        
        out.returnCode = returnCode;
        
        output.add(out);
        return output;
    }
    
}