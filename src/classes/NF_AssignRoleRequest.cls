public with sharing class NF_AssignRoleRequest extends NF_ApiRequest {
    public NF_AssignRoleRequest(String chatKey, String tracUniqueParam){
        assignRoleRequest = new AssignRoleRequest();
        assignRoleRequest.caseInformation = new CaseInformation();

        //the third of 3 sequential API calls in Software Downloads, make sure the ID is unique
        assignRoleRequest.caseInformation.customerUniqueTransactionID = NF_Util.updateUniqueId(NF_Util.generateTransactionId(chatKey), tracUniqueParam);
        assignRoleRequest.assignRoleDetails = new AssignRoleDetails();
    }

    public void setRoles(String roles){
        assignRoleRequest.assignRoleDetails.roles = roles;
        system.debug('Roles from Assign role Request@@####+++' +roles);
    }

    public void setEmail(String email){
        system.debug('Email before Request@@####+++' +email);
        assignRoleRequest.assignRoleDetails.emailAddress = email;
        system.debug('Email from Assign role Request@@####+++' +email);
    }

    public override String getApiName(){
        return 'Assign Role';
        
    }

    public override String getEndpoint(){
        return 'callout:Juniper_API_Gateway/v01/assignrole';
    }

    public override String getTransactionId(){
        return assignRoleRequest != null ? assignRoleRequest.caseInformation.customerUniqueTransactionID : '';
    }

    public AssignRoleRequest assignRoleRequest;

    public class AssignRoleRequest extends AbstractRequest{
        public CaseInformation caseInformation { get; set; }
        public AssignRoleDetails assignRoleDetails { get; set; }

    }
    public class CaseInformation{
        public String customerSourceID = 'juni-chatbot';
        public String customerUniqueTransactionID { get; set; }
    }
    public class AssignRoleDetails{
        public String roles = 'JUNOS-EVO, cSRX';
        //public String roles = 'SW-EVAL-VQFX, SW-EVAL-VQFX';
        //public String roles = 'JUNOS';
        
        public String emailAddress { get; set; }
    }

}