/**
 * Created by mcasella on 2019-11-18.
 */

public with sharing  class NF_QuerySRResponse extends NF_ApiResponse {
    public QuerySRResponse querySRResponse { get; set; }

    public class QuerySRResponse extends ApiResponse{
        public String customerUniqueTransactionID { get; set; }
        public String responseDateTime { get; set; }
        public String serviceRequestNumber { get; set; }
        public String customerSourceID { get; set; }
        public String customerCaseNumber { get; set; }
        public CaseType caseType { get; set; }
        public String synopsis { get; set; }
        public String problemDescription { get; set; }
        public String priority { get; set; }
        public String srStatus { get; set; }
        public String adminSRType { get; set; }
        public String adminSRSubType { get; set; }
        public String ccEmail { get; set; }
        public Boolean addSenderToEmail { get; set; }
        public String followUpMethod { get; set; }
        public String srOwnerFullName { get; set; }
        public String srOwnerEmailAddress { get; set; }
        public Contact contact { get; set; }
        public Product product { get; set; }
        public RMA[] rma {get; set; }
        public Escalate[] escalate {get; set; }
        public Attachment[] attachments {get; set; }
        public LinkedReference[] linkedReferences {get; set; }
        
    }

    public class CaseType {
        public String caseTypeCode { get; set; }
        public String caseTypeDescription { get; set; }
    }

    public override Boolean isError(){
        return querySRResponse == null || querySRResponse.isError();
    }

    public override Integer getStatusCode(){
        return querySRResponse != null ? querySRResponse.getStatusCode() : -1;
    }

    public override Integer getErrorCode(){
        return querySRResponse != null ? querySRResponse.getErrorCode() : -1;
    }

    public override String getDescription(){
        return querySRResponse != null ? querySRResponse.getDescription() : error;
    }

    public class Contact {
        public String accountID { get; set; }
        public String accountName { get; set; }
        public String contactName { get; set; }
        public String contactEmail { get; set; }
        public String preferredTelephoneCountryCode { get; set; }
        public String preferredTelephoneNumber { get; set; }
        public String preferredTelephoneExtension { get; set; }
    }

    public class Product {
        public String serialNumber { get; set; }
        public String productID { get; set; }
        public String productSeries { get; set; }
        public String platform { get; set; }
        public String version { get; set; }
        public String release { get; set; }
        public String software { get; set; }
        public String specialRelease { get; set; }
        public String routerName { get; set; }
    }

    public class RMA {
        public String rmaNumber { get; set; }
        public RMAItem[] items { get; set; }
    }

    public class RMAItem {
        public String itemNumber { get; set; }
        public String itemType { get; set; }
        public String itemStatus { get; set; }
    }

    public class Escalate {
        public String description { get; set; }
        //public String dateTime { get; set; }
        public String escalatedBy { get; set; }
        public String status { get; set; }
    }

    public class Attachment {
        public String sequenceNumber { get; set; }
        public String title { get; set; }
        public String sizeInBytes { get; set; }
        public String path { get; set; }
        public String typeDescription { get; set; }
        public String uploadedDateTime { get; set; }
        public String uploadedBy { get; set; }
    }

    public class LinkedReference {
        public String url { get; set; }
        public String referenceId { get; set; }
        public String referenceType { get; set; }
    }
}