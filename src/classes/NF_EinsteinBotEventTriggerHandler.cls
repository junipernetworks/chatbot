public with sharing class NF_EinsteinBotEventTriggerHandler {
    public static String EVENT_UPDATE_BOT_SESSION = 'UPDATE_BOT_SESSION';
//    public static String EVENT_ADD_API_ERROR = 'ADD_API_ERROR';
//    public static String EVENT_UPDATE_API_INDEX = 'UPDATE_API_INDEX';
//    public static String EVENT_ADD_PAGE_URL = 'ADD_PAGE_URL';

    //TODO: should make some generic function that updates the Bot Session and just expose wrapper functions
    //this method will update the BotName and Chat Page on Bot Session.
    public static void updateBotSessionFields(String chatKey, String botName){
        boolean updateBotSession = false;
        ebotdata__Bot_Session__c session = null;

        try {
            System.debug('>>NF_EinsteinBotEventTriggerHandler.updateBotName: chatKey='+ chatKey);

            session = NF_Util.getBotSession(chatKey);
            if(session != null){
                session.ebotdata__BotName__c = botName;
                updateBotSession = true;

                //write transcript fields to session
                LiveChatTranscript transcript = NF_Util.getLiveChatTranscript(chatKey);
                if(String.isNotBlank(transcript.Page_URL__c)){
                    session.ebotdata__Chat_Page__c = transcript.Page_URL__c;
                }

                if(String.isNotBlank(transcript.C2CEmail__c)){
                    session.Visitor_Email__c = transcript.C2CEmail__c;
                }
            }
        }
        catch (Exception ex) {
            NF_EinsteinBotLogger.populatedLog(ex);
        }

        if(session != null && updateBotSession){
            update session;
        }
    }

//    public static void updateApiIndex(String chatKey, Decimal apiIndex){
//        try {
//            System.debug('>>NF_EinsteinBotEventTriggerHandler.updateBotName: chatKey='+ chatKey);
//
//            ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatKey);
//
//            if(session != null){
//                session.API_Request_Index__c = apiIndex;
//
//                update session;
//            }
//        }
//        catch (Exception ex) {
//            NF_EinsteinBotLogger.populatedLog(ex);
//        }
//    }
}