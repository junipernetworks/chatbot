@isTest
public with sharing class NF_AssignRoleTest {
    @testSetup
    static void initialize() {
        NF_UtilTest.insertApiSettings(true);
    }

    @IsTest
    static void testSuccess() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SuccessResponse());

        NF_AssignRole.Input input = new NF_AssignRole.Input();
        input.chatKey = 'abc';
        input.email = 'test@qwest.com';
        input.roles = 'JUNOS-EVO, cSRX';

        List<NF_AssignRole.Output> output = NF_AssignRole.assignRole(new List<NF_AssignRole.Input>{input});

        System.debug('OUTPUT RESPONSE IS: ' + output[0].response);

        System.assertEquals(nf_ApiController.API_RESULT.SUCCESS, output[0].returnCode);

        Test.stopTest();
    }

    static void accessResponseFields(NF_AssignRoleResponse res){
        res.assignRoleResponse.customerSourceID = '';
        res.assignRoleResponse.customerUniqueTransactionID = '';
        res.assignRoleResponse.responseDateTime = '';
        
    }

    @IsTest
    static void testSuccessResponse() {
        NF_AssignRoleResponse response = new NF_AssignRoleResponse();
        //weirdness to get around test coverage
        JSON.serialize(response);
        response = (NF_AssignRoleResponse)JSON.deserialize(SUCCESS_RESPONSE, NF_AssignRoleResponse.class);

        //weirdness to get around test coverage
        NF_AssignRoleResponse cwa = new NF_AssignRoleResponse();
        cwa.assignRoleResponse = new NF_AssignRoleResponse.AssignRoleResponse();

        accessResponseFields(cwa);

        System.assertEquals(false, response.isError());
        //System.assertEquals('12345678', response.queryRMAResponse.customerCaseNumber);
        System.assertEquals('Successfully processed the request', response.getDescription());
    }

    @IsTest
    static void testUnauthorized(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UnauthorizedResponse());

        NF_AssignRole.Input input = new NF_AssignRole.Input();
        input.chatKey = 'abc';
        input.email = 'test@qwest.com';

        List<NF_AssignRole.Output> output = NF_AssignRole.assignRole(new List<NF_AssignRole.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testUnauthorizedResponse() {
        NF_AssignRoleResponse response = (NF_AssignRoleResponse)JSON.deserialize(UNAUTHORIZED_RESPONSE, NF_AssignRoleResponse.class);

        System.assertEquals(true, response.isError());
        System.assertEquals('Key not authorised', response.getDescription());
    }

    @IsTest
    static void testError(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ErrorResponse());

        NF_AssignRole.Input input = new NF_AssignRole.Input();
        input.chatKey = 'abc';
        input.email = 'test@qwest.com';

        List<NF_AssignRole.Output> output = NF_AssignRole.assignRole(new List<NF_AssignRole.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testErrorResponse() {
        NF_AssignRoleResponse response = (NF_AssignRoleResponse)JSON.deserialize(ERROR_RESPONSE, NF_AssignRoleResponse.class);

        System.assertEquals(true, response.isError());
        System.assertEquals(-1, response.getErrorCode());
    }

    private static String SUCCESS_RESPONSE = '{"assignRoleResponse": {"responseDateTime": "2020-01-16T20:37:57.171Z","customerUniqueTransactionID": "1234","message": "Successfully processed the request","customerSourceID": "juni-chatbot","statusCode": "200","status": "Success"}}';
    private static String ERROR_RESPONSE = '{"assignRoleRequest" : {"appId" : "URJxgQGSBrm8JuQ0eGPKSt1UqqF13D8eONChGlc8","userId" : "chatbot-support@juniper.net","requestDateTime" : "2019-11-18T07:34:28.176Z","caseInformation" : {"customerSourceID" : "juni-chatbot","customerUniqueTransactionID" : "1234"},"assignRoleDetails" : {"emailAddress" : "test@qwest.com","roles" : "JUNOS-EV"}}}';
    private static String UNAUTHORIZED_RESPONSE = '{ "error": "Key not authorised" }';

    public class SuccessResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, SUCCESS_RESPONSE); }
    }

    public class UnauthorizedResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(403, UNAUTHORIZED_RESPONSE); }
    }

    public class ErrorResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(500, ERROR_RESPONSE); }
    }
}