/**
 * Created by mcasella on 2019-11-22.
 */

@isTest
public with sharing class NF_GetUserProfileTest {

    @testSetup
    static void initialize() {
        NF_UtilTest.insertApiSettings(true);
    }

    @IsTest
    static void testSuccess() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SuccessResponse());

        NF_GetUserProfile.Input input = new NF_GetUserProfile.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_GetUserProfile.Output> output = NF_GetUserProfile.getProfile(new List<NF_GetUserProfile.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.SUCCESS, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testSuccessResponse() {
        NF_GetUserProfileResponse response = new NF_GetUserProfileResponse();
        //weirdness to get around test coverage
        JSON.serialize(response);
        response = (NF_GetUserProfileResponse)JSON.deserialize(SUCCESS_RESPONSE, NF_GetUserProfileResponse.class);


        System.assertEquals(false, response.isError());
        System.assert(String.isEmpty(response.getDescription()));

        System.assertEquals('0200991419', response.getContactId());
        System.assertNotEquals('', response.id);
        System.assertNotEquals('', response.status);
    }

    @IsTest
    static void testUnauthorized(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UnauthorizedResponse());

        NF_GetUserProfile.Input input = new NF_GetUserProfile.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_GetUserProfile.Output> output = NF_GetUserProfile.getProfile(new List<NF_GetUserProfile.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testUnauthorizedResponse() {
        NF_GetUserProfileResponse response = (NF_GetUserProfileResponse)JSON.deserialize(UNAUTHORIZED_RESPONSE, NF_GetUserProfileResponse.class);

        System.assertEquals(true, response.isError());
        System.assertEquals('Key not authorised', response.getDescription());
    }

    @IsTest
    static void testError(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ErrorResponse());

        NF_GetUserProfile.Input input = new NF_GetUserProfile.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_GetUserProfile.Output> output = NF_GetUserProfile.getProfile(new List<NF_GetUserProfile.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testErrorResponse() {
        NF_GetUserProfileResponse response = (NF_GetUserProfileResponse)JSON.deserialize(ERROR_RESPONSE, NF_GetUserProfileResponse.class);

        System.assertEquals('902', response.errorCode);
        System.assertEquals('Error occurred while fetching user profile. Please retry later.', response.errorMessage);

        System.assertEquals(true, response.isError());
        System.assertEquals('Error occurred while fetching user profile. Please retry later.', response.getDescription());
        System.assertEquals(902, response.getStatusCode());
    }

    private static String SUCCESS_RESPONSE = '{"id":"00unxq025nr9yrmPC0h7","status":"ACTIVE","created":"2019-10-18T03:04:15.000Z","activated":"2019-10-18T06:55:28.000Z","statusChanged":"2019-10-18T06:55:28.000Z","lastLogin":null,"lastUpdated":"2019-10-18T06:55:28.000Z","passwordChanged":"2019-10-18T03:04:16.000Z","profile":{"zipCode":"49130","email":"it@cellebrite.com","sapBPID":"0200991419"}}';
    private static String ERROR_RESPONSE = '{ "errorMessage": "Error occurred while fetching user profile. Please retry later.", "errorCode": "902" }';
    private static String UNAUTHORIZED_RESPONSE = '{ "error": "Key not authorised" }';

    public class SuccessResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, SUCCESS_RESPONSE); }
    }

    public class UnauthorizedResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(403, UNAUTHORIZED_RESPONSE); }
    }

    public class ErrorResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(500, ERROR_RESPONSE); }
    }
}