/**
 * Created by joaquintalon on 12/12/19.
 */

public with sharing class NF_GenerateCarouselPayload {
    public class Input {
        @InvocableVariable(required=true)
        public String rawJSON;
        @InvocableVariable(required=true)
        public String serialNum;
    }

    public class ProductInformation {
        public String serialNo = 'N/A';
        public String productId = 'N/A';
        public String registrationDate = 'N/A';
        public String warrantyExpiry = 'N/A';
    }

    public class InstallationInformation {
        public String company = 'N/A';
        public String address = 'N/A';
        public String city = 'N/A';
        public String country = 'N/A';
        public String state = 'N/A';
    }

    public class ServiceContract {
        public String contractId = 'N/A';
        public String startDate = 'N/A';
        public String endDate = 'N/A';
        public String service = 'N/A';
        public String status = 'N/A';
    }

    public class SoftwareSubscription {
        public String contractId = 'N/A';
        public String startDate = 'N/A';
        public String endDate = 'N/A';
        public String service = 'N/A';
        public String status = 'N/A';
    }

    public class EndOfLifeEndOfSupport {
        public String eolDate = 'N/A';
        public String eosDate = 'N/A';
        public String replacement = 'N/A';
    }

    @InvocableMethod(label='Neuraflash Get Carousel')
    public static List<String> generateCarousel(List<Input> input){
        String rawjsonstring = input[0].rawJSON;

        ProductInformation pi = new ProductInformation();
        InstallationInformation ii = new InstallationInformation();
        ServiceContract sc = new ServiceContract();
        SoftwareSubscription ss = new SoftwareSubscription();
        EndOfLifeEndOfSupport eoleos = new EndOfLifeEndOfSupport();

        pi.serialNo = input[0].serialNum;

        NF_QuerySNEntitlementResponse json = (NF_QuerySNEntitlementResponse) JSON.deserialize(rawjsonstring, NF_QuerySNEntitlementResponse.class);

        if (json != null){
            if (json.getSNEntitlementDetailsResponse != null){

                NF_QuerySNEntitlementResponse.SNEntitlementDetail details = json.getSNEntitlementDetailsResponse.snEntitlementDetails;
                if(details != null && details.ssrnInfo != null && details.ssrnInfo.size() > 0){
                    //details.ssrnInfo[0].
                    NF_QuerySNEntitlementResponse.SSRNInfo info = details.ssrnInfo[0];

                    sc.contractId = info.serviceContractNumber;
                    sc.startDate = info.entitlementStartDate;
                    sc.endDate = info.entitlementEndDate;
                    sc.service = info.serviceProduct;
                    sc.status = info.serviceContractStatus;

                    ss.contractId = info.serviceContractNumber;
                    ss.startDate = info.entitlementStartDate;
                    ss.endDate = info.entitlementEndDate;
                    ss.service = info.serviceProduct;
                    ss.status = info.serviceContractStatus;

                    eoleos.eolDate = info.itemEndDateFilterEol;
                    eoleos.eosDate = info.itemEndDateFilterEos;
                    eoleos.replacement = info.eolProductReplacement;

                    if(info.productList != null && info.productList.size() > 0){
                        pi.productId = info.productList[0].product;
                        pi.registrationDate = info.productList[0].registeredDate;
                        pi.warrantyExpiry = info.productList[0].warrantyEndDate;

                        ii.city = info.productList[0].installedAtCity;
                        ii.country = info.productList[0].installedAtCountry;
                        ii.state = info.productList[0].installedAtState;
                    }

                    if(info.accountList != null){
                        NF_QuerySNEntitlementResponse.Account account = null;
                        for(NF_QuerySNEntitlementResponse.Account acc : info.accountList){
                            if(acc.ibEndCustomerFlag == 'Y'){
                                account = acc;
                                break;
                            }
                        }
                        if(account != null) {
                            ii.company = account.accountName;
                            ii.address = account.street;
                        }
                    }
                }
            }
        }

        return null;
    }
}