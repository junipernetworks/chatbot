/**
 * Created by mcontractlla on 2019-11-17.
 */

public with sharing class NF_QuerySNEntitlementRequest extends NF_ApiRequest {
    public NF_QuerySNEntitlementRequest(String chatKey){
        getSNEntitlementDetailsRequest = new QuerySNEntitlementRequest();
        getSNEntitlementDetailsRequest.contractInformation = new ContractInformation();
        getSNEntitlementDetailsRequest.contractInformation.customerUniqueTransactionID = NF_Util.generateTransactionId(chatKey);
        getSNEntitlementDetailsRequest.contact = new Contact();
        getSNEntitlementDetailsRequest.product = new Product();
    }

    public void setContactId(String id){
        getSNEntitlementDetailsRequest.contact.contactId = id;
    }

    public void setEmail(String email){
        getSNEntitlementDetailsRequest.contact.contactEmail = email;
    }

    public override String getApiName(){
        return 'Query SNEntitlement';
    }

    public override String getEndpoint(){
        return 'callout:Juniper_API_Gateway/v01/querysnentitlement';
    }

    public override String getTransactionId(){
        return getSNEntitlementDetailsRequest != null ? getSNEntitlementDetailsRequest.contractInformation.customerUniqueTransactionID : '';
    }

    public QuerySNEntitlementRequest getSNEntitlementDetailsRequest;

    public class Contact {
        public String contactId { get; set; }
        public String contactEmail { get; set; }
    }

    public class QuerySNEntitlementRequest extends AbstractRequest{
        public ContractInformation contractInformation { get; set; }
        public Product product { get; set; }
        public Contact contact { get; set; }
    }

    public class Product {
        public String serialNumber { get; set; }
        public String ssrnFlag { get; set; }
    }

    public class ContractInformation{
        public String customerSourceID = 'juni-chatbot';
        public String customerUniqueTransactionID { get; set; }
        public String contractId { get; set; }
        public String salesOrderNumber { get; set; }
    }
}