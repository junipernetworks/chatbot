/**
 * Created by mcasella on 2019-11-06.
 */

public with sharing class NF_ReassignBotCase {

    public static boolean reassignCases(Map<Id, LiveChatTranscript> liveChatTranscripts){

        if(liveChatTranscripts == null || liveChatTranscripts.size() == 0){
            return false;
        }

        RecordType ccareCaseRecordType = NF_Util.getCcareRecordType();
        User platformIntegrationUser = NF_Util.getPlatformIntegrationUser();
        if(ccareCaseRecordType == null || platformIntegrationUser == null){
            System.debug('Unable to Reassign Bot Case: ccareCaseRecordType='+ccareCaseRecordType+', platformIntegrationUser=['+platformIntegrationUser+']');
            return false;

            //TODO: Log an exception from a String
        }

        try{
            List<Case> cases = new List<Case>();

            for (Id liveChatId : liveChatTranscripts.keySet()) {
                System.debug('>> NF_LiveChatTranscriptReassignBotCase trigger update: liveChatId=' + liveChatId);
                if (liveChatTranscripts.get(liveChatId).CreatedById != liveChatTranscripts.get(liveChatId).OwnerId) {

                    LiveChatTranscript transcript = liveChatTranscripts.get(liveChatId);
                    Id createdById = transcript.CreatedById;
                    System.debug('>> NF_LiveChatTranscriptReassignBotCase: createdById=' + createdById);
                    if (createdById == platformIntegrationUser.Id) {
                        Case c = new Case();
                        if (liveChatTranscripts.get(liveChatId).CaseId != null) {
                            c.Id = liveChatTranscripts.get(liveChatId).CaseId;
                            c.OwnerId = liveChatTranscripts.get(liveChatId).OwnerId;
                            cases.add(c);
                        }
                    }
                }
            }
            if (cases.size() > 0) {
                update cases;
                return true;
            }
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
            //NF_EinsteinBotLogger.error(e);
        }

        return false;
    }
}