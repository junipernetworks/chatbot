@isTest
public with sharing class NF_GetPlatformToRoleDetailsTest {
    @testSetup
    static void initialize() {
        NF_UtilTest.insertApiSettings(true);
    }

    @IsTest
    static void testSuccess() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SuccessResponse());

        NF_GetPlatformToRoleDetails.Input input = new NF_GetPlatformToRoleDetails.Input();
        input.chatKey = 'abc';
        input.platformName = 'ptx10003';
        input.osId = '246';

        List<NF_GetPlatformToRoleDetails.Output> output = NF_GetPlatformToRoleDetails.getPlatformToRoleDetails(new List<NF_GetPlatformToRoleDetails.Input>{input});

        System.debug('OUTPUT RESPONSE IS: ' + output[0].response);

        System.assertEquals(nf_ApiController.API_RESULT.SUCCESS, output[0].returnCode);

        Test.stopTest();
    }

    static void accessResponseFields(NF_GetPlatformToRoleDetailsResponse res){
        res.queryPlatformToRoleDetailsResponse.customerSourceID = '';
        res.queryPlatformToRoleDetailsResponse.customerUniqueTransactionID = '';
        res.queryPlatformToRoleDetailsResponse.responseDateTime = '';
        res.queryPlatformToRoleDetailsResponse.roles = new List<NF_GetPlatformToRoleDetailsResponse.Roles>{new NF_GetPlatformToRoleDetailsResponse.Roles()};
        res.queryPlatformToRoleDetailsResponse.roles[0].roleName = '';
        res.queryPlatformToRoleDetailsResponse.roleOperator = '';
        
    }

    @IsTest
    static void testSuccessResponse() {
        NF_GetPlatformToRoleDetailsResponse response = new NF_GetPlatformToRoleDetailsResponse();
        //weirdness to get around test coverage
        JSON.serialize(response);
        response = (NF_GetPlatformToRoleDetailsResponse)JSON.deserialize(SUCCESS_RESPONSE, NF_GetPlatformToRoleDetailsResponse.class);

        //weirdness to get around test coverage
        NF_GetPlatformToRoleDetailsResponse cwa = new NF_GetPlatformToRoleDetailsResponse();
        cwa.queryPlatformToRoleDetailsResponse = new NF_GetPlatformToRoleDetailsResponse.QueryPlatformToRoleDetailsResponse();
        cwa.queryPlatformToRoleDetailsResponse.roles = new List<NF_GetPlatformToRoleDetailsResponse.Roles>{new NF_GetPlatformToRoleDetailsResponse.Roles()};

        accessResponseFields(cwa);

        System.assertEquals(false, response.isError());
        //System.assertEquals('12345678', response.queryRMAResponse.customerCaseNumber);
        System.assertEquals('Successfully processed the request', response.getDescription());
    }

    @IsTest
    static void testUnauthorized(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UnauthorizedResponse());

        NF_GetPlatformToRoleDetails.Input input = new NF_GetPlatformToRoleDetails.Input();
        input.chatKey = 'abc';
        input.platformName = 'ptx10003';

        List<NF_GetPlatformToRoleDetails.Output> output = NF_GetPlatformToRoleDetails.getPlatformToRoleDetails(new List<NF_GetPlatformToRoleDetails.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testUnauthorizedResponse() {
        NF_GetPlatformToRoleDetailsResponse response = (NF_GetPlatformToRoleDetailsResponse)JSON.deserialize(UNAUTHORIZED_RESPONSE, NF_GetPlatformToRoleDetailsResponse.class);

        System.assertEquals(true, response.isError());
        System.assertEquals('Key not authorised', response.getDescription());
    }

    @IsTest
    static void testError(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ErrorResponse());

        NF_GetPlatformToRoleDetails.Input input = new NF_GetPlatformToRoleDetails.Input();
        input.chatKey = 'abc';
        input.platformName = 'ptx10003';

        List<NF_GetPlatformToRoleDetails.Output> output = NF_GetPlatformToRoleDetails.getPlatformToRoleDetails(new List<NF_GetPlatformToRoleDetails.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testErrorResponse() {
        NF_GetPlatformToRoleDetailsResponse response = (NF_GetPlatformToRoleDetailsResponse)JSON.deserialize(ERROR_RESPONSE, NF_GetPlatformToRoleDetailsResponse.class);

        System.assertEquals(true, response.isError());
        //System.assertEquals('\r\n[775] rmaNumber - R00002098 - is not associated with serviceRequestNumber - 2015-0811-T-0007', response.getDescription());
        System.assertEquals(400, response.getStatusCode());
    }

    private static String SUCCESS_RESPONSE = '{"queryPlatformToRoleDetailsResponse": {"customerSourceID": "juni-chatbot","customerUniqueTransactionID": "getPlatformToRoleDetails-021820201112","responseDateTime": "2020-02-18T19:19:50.815Z","statusCode": "200","status": "Success","message": "Successfully processed the request","roleOperator": "And","roles": [{"roleName": "JUNOS-EVO"}]}}';
    private static String ERROR_RESPONSE = '{"queryPlatformToRoleDetailsResponse": {"customerSourceID": "juni-chatbot","customerUniqueTransactionID": "getPlatformToRoleDetails-021820201113","responseDateTime": "2020-02-18T19:21:29.450Z","statusCode": "400","status": "Error","message": "Error in processing the request","fault": [{"errorClass": "FAILURE","errorType": "FAILURE","errorCode": "A001","errorMessage": "OS and Platform are not related"}]}}';
    private static String UNAUTHORIZED_RESPONSE = '{ "error": "Key not authorised" }';

    public class SuccessResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, SUCCESS_RESPONSE); }
    }

    public class UnauthorizedResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(403, UNAUTHORIZED_RESPONSE); }
    }

    public class ErrorResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(500, ERROR_RESPONSE); }
    }
}