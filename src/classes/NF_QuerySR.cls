/**
 * Created by mcasella on 2019-11-17.
 */

public with sharing class NF_QuerySR {
    public static String ENDPOINT = 'callout:Juniper_API_Gateway/v01/querysr';

    public class Input{
        @InvocableVariable(required=true)
        public String chatKey;

        @InvocableVariable(required=true)
        public String email;

        @InvocableVariable(required=true)
        public String srNumber;

        @InvocableVariable(required=false)
        public String contactId;
    }

    public class Output{
        @InvocableVariable
        public String response;

        @InvocableVariable
        public Integer returnCode;

        @InvocableVariable
        public String carouselPayload;

        @InvocableVariable
        public String failureReason;
    }

    @InvocableMethod(label='Neuraflash - Query SR')
    public static List<Output> querySR(List<Input> input) {
        List<Output> output = new List<Output>();
        Output out = new Output();
        out.returnCode = nf_ApiController.API_RESULT.FAILURE;

        try {
            String chatkey = input[0].chatKey;
            String email   = input[0].email;
            String srNumber = input[0].srNumber;
            String contactId = input[0].contactId;

            NF_ApiController apiController = new NF_ApiController();
            apiController.initialize(chatKey);

            NF_QuerySRRequest apiRequest = new NF_QuerySRRequest(chatKey);
            apiRequest.querySRRequest.caseInformation.serviceRequestNumber = srNumber;
            apiRequest.querySRRequest.caseInformation.customerCaseNumber = '';

            apiRequest.setEmail(email);
            apiRequest.setContactId(contactId);
            apiRequest.setAccountId(contactId);

            System.debug('>>NF_QuerySR apiRequest=' + apiRequest);

            NF_QuerySRResponse apiResponse = (NF_QuerySRResponse) apiController.sendApiRequest(apiRequest, NF_QuerySRResponse.class, true);
            System.debug('>>NF_QuerySR response=' + apiResponse);

            if (apiResponse != null) {
                out.response = JSON.serialize(apiResponse);

                if (apiResponse.isError() == false) {
                    out.returnCode = nf_ApiController.API_RESULT.SUCCESS;
                    System.debug('>>NF_QuerySR out.response=' + out.response);
                    String carouselPayload = NF_DynamicCarouselContentFactory.createNF_QuerySRCarousel(apiResponse);
                    out.carouselPayload = carouselPayload;
                    System.debug('>>>carouselPayload'+carouselPayload);

                    ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatkey);
                    if(session != null){
                        session.CarouselContent__c = carouselPayload;
                        update session;
                        System.debug('Updated field>>>'+session.CarouselContent__c);
                    }
                }
                else {
                    Integer errorCode = apiResponse.getErrorCode();
                    System.debug('>>NF_QuerySR errorCode=' + errorCode);
                    if(errorCode == 755 || errorCode == 757 || errorCode == 806){
                        out.returnCode = nf_ApiController.API_RESULT.NOT_FOUND;
                        if(errorCode == 755){
                            out.failureReason = 'insufficient';
                        }
                        else{
                            out.failureReason = 'invalid';
                        }

                    }
                }
            }
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
        }

        System.debug('NF_QuerySR: returning out>>>'+out);

        output.add(out);
        return output;
    }
}