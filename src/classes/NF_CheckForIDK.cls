/**
 * Created by joaquintalon on 12/10/19.
 */

public with sharing class NF_CheckForIDK {
    @InvocableMethod(label='Neuraflash - Check for IDK')
    public static List<String> checkForIdk(List<String> input){
        String ui = input[0];
        Pattern idk = Pattern.compile('(?i).*?((don(\'?)t know)|(((can(\'?)t)|(couldn(\'?)t)) find)).*');
        Matcher idkMatcher = idk.matcher(ui);

        if(idkMatcher.matches()){
            return new List<String>{'I don\'t know'};
        }else{
            return new List<String>{ui};
        }
    }
}