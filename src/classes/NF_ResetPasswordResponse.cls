/**
 * Created by mcasella on 2019-11-21.
 */

public with sharing class NF_ResetPasswordResponse extends NF_ApiResponse {
    public ResetPasswordResponse resetPasswordResponse;

    public class ResetPasswordResponse extends ApiResponse{
        public String customerSourceID { get; set; }
        public String customerUniqueTransactionID { get; set; }
        public String responseDateTime { get; set; }
    }
    
    public override Boolean isError(){
        return resetPasswordResponse == null || resetPasswordResponse.isError();
    }

    public override Integer getStatusCode(){
        return resetPasswordResponse != null ? resetPasswordResponse.getStatusCode() : -1;
    }

    public override Integer getErrorCode(){
        return resetPasswordResponse != null ? resetPasswordResponse.getErrorCode() : -1;
    }

    public override String getDescription(){
        return resetPasswordResponse != null ? resetPasswordResponse.getDescription() : error;
    }
}