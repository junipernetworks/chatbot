/**
 * Created by mcasella on 2019-11-18.
 */

public with sharing  class NF_QuerySNEntitlementResponse extends NF_ApiResponse {
    public QuerySNEntitlementResponse getSNEntitlementDetailsResponse { get; set; }

    public class QuerySNEntitlementResponse extends ApiResponse{
        public String customerUniqueTransactionID { get; set; }
        public String responseDateTime { get; set; }
        public String customerSourceID { get; set; }
        public String customerCaseNumber { get; set; }
        public String salesOrderNumber { get; set; }
        public String contractId { get; set; }
        public SNEntitlementDetail snEntitlementDetails { get; set; }
    }

    public class SNEntitlementDetail {
        public Contact contact { get; set; }
        public SSRNInfo[] ssrnInfo { get; set; }
    }

    public class SSRNInfo {
        public String serviceRequestNumber { get; set; }
        public String entitlementStartDate { get; set; }
        public String entitlementEndDate { get; set; }
        public String entitlementType { get; set; }
        public String serviceProductDescription { get; set; }
        public String lineItemSubStatus { get; set; }
        public String serviceProduct { get; set; }
        public String serviceContractStatus { get; set; }
        public String entitlementSerialNumber { get; set; }
        public String serviceContractNumber { get; set; }
        public String recordCount { get; set; }
        public String itemNumber { get; set; }
        public String ssrn { get; set; }
        public String materialId { get; set; }
        public String equipmentId { get; set; }
        public String legacyContractNo { get; set; }
        public String itemStartDateFilterEol { get; set; }
        public String itemStartDateFilterEos { get; set; }
        public String itemEndDateFilterEol { get; set; }
        public String itemEndDateFilterEos { get; set; }
        public String parentSerialNumber { get; set; }
        public String parentMaterialId { get; set; }
        public String equipmentStatus { get; set; }
        public String customerSONumber { get; set; }
        public String customerPONumber { get; set; }
        public String eolProductReplacement { get; set; }
        public Account[] accountList { get; set; }
        public Product[] productList { get; set; }
    }

    public class Product {
        public String registeredDate { get; set; }
        public String product { get; set; }
        public String warrantyStartDate { get; set; }
        public String warrantyEndDate { get; set; }
        public String installedAtCity { get; set; }
        public String installedAtState { get; set; }
        public String installedAtCountry { get; set; }
        public String shipDate { get; set; }
        public String serviceStatus { get; set; }
        public String ibaseStatus { get; set; }
        public String serviceEligible { get; set; }
        public String serviceDeclineReason { get; set; }
        public String productSalesOrderNumber { get; set; }
        public String productPurchaseOrderNumber { get; set; }
    }

    public class Account {
        public String serviceBillToFlag { get; set; }
        public String serviceInstalledAtFlag { get; set; }
        public String serviceManagerResellerFlag { get; set; }
        public String serviceResellerFlag { get; set; }
        public String serviceShipToFlag { get; set; }
        public String accountName { get; set; }
        public String commonAccountId { get; set; }
        public String serviceDistributorFlag { get; set; }
        public String serviceEndUserFlag { get; set; }
        public String ibEndCustomerFlag { get; set; }
        public String street { get; set; }
    }

    public override Boolean isError(){
        return getSNEntitlementDetailsResponse == null || getSNEntitlementDetailsResponse.isError();
    }

    public override Integer getStatusCode(){
        return getSNEntitlementDetailsResponse != null ? getSNEntitlementDetailsResponse.getStatusCode() : -1;
    }

    public override Integer getErrorCode(){
        return getSNEntitlementDetailsResponse != null ? getSNEntitlementDetailsResponse.getErrorCode() : -1;
    }

    public override String getDescription(){
        return getSNEntitlementDetailsResponse != null ? getSNEntitlementDetailsResponse.getDescription() : error;
    }

    public class Contact {
        public String contactId { get; set; }
        public String contactEmail { get; set; }
    }
}