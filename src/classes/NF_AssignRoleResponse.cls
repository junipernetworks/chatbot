public with sharing class NF_AssignRoleResponse extends NF_ApiResponse {

    public AssignRoleResponse assignRoleResponse{ get;set; }

    public class AssignRoleResponse extends ApiResponse{
        public String customerUniqueTransactionID { get; set; }
        public String responseDateTime { get; set; }
        public String customerSourceID { get; set; }
        public String encryption { get; set; }
    }
    public override Boolean isError(){
        return assignRoleResponse == null || assignRoleResponse.isError();
    }

    public override Integer getStatusCode(){
        return assignRoleResponse != null ? assignRoleResponse.getStatusCode() : -1;
    }

    public override Integer getErrorCode(){
        return assignRoleResponse != null ? assignRoleResponse.getErrorCode() : -1;
    }

    public override String getDescription(){
        return assignRoleResponse != null ? assignRoleResponse.getDescription() : error;
    }

}