/**
 * Created by mcasella on 2019-11-06.
 */

@IsTest
private class NF_ReassignBotCaseTest {

    @IsTest
    static void testEmptyTranscriptList() {
        System.assertEquals(false, NF_ReassignBotCase.reassignCases(null));
    }

    @isTest
    static void TestEscalateTranscript(){
        Test.startTest();


//        User u = new User(
//                ProfileId = [SELECT Id, Name, ProfileId FROM User WHERE name = 'Platform Integration User'].ProfileId,
//                LastName = 'last',
//                Email = 'test@test.com',
//                Username = 'test@test.com' + System.currentTimeMillis(),
//                CompanyName = 'TEST',
//                Title = 'title',
//                Alias = 'alias',
//                TimeZoneSidKey = 'America/Los_Angeles',
//                EmailEncodingKey = 'UTF-8',
//                LanguageLocaleKey = 'en_US',
//                LocaleSidKey = 'en_US'
//        );
//        insert u;

        User platformIntegrationUser = NF_Util.getPlatformIntegrationUser();

        User u2 = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
                LastName = 'last2',
                Email = 'test@test.com',
                Username = 'test@test.com' + System.currentTimeMillis(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US'
        );

        insert u2;

        Case c = new Case();
        LiveChatTranscript transcript;

        System.runAs(platformIntegrationUser){
            transcript = insertEmptyLiveChatTranscript();

            c.OwnerId = platformIntegrationUser.Id;
            c.RecordTypeId = NF_Util.getCcareRecordType().Id;
            insert c;
        }

        transcript.CaseId = c.Id;
        transcript.OwnerId = u2.Id;
        update transcript;
        Case theCase = [SELECT Id, OwnerId FROM Case WHERE Id = :c.Id];
        System.assert(theCase.OwnerId == transcript.OwnerId);

        Test.stopTest();
    }


    public static LiveChatTranscript insertEmptyLiveChatTranscript(){
        LiveChatVisitor v = new LiveChatVisitor();
        insert v;
        LiveChatTranscript transcript = new LiveChatTranscript();
        transcript.LiveChatVisitorId= v.Id;

        insert transcript;

        return transcript;
    }

}