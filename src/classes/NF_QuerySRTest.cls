/**
 * Created by mcasella on 2019-11-17.
 */

@isTest
public with sharing class NF_QuerySRTest {
    @testSetup
    static void initialize() {
        NF_UtilTest.insertApiSettings(true);
    }

    @IsTest
    static void testSuccess() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SuccessResponse());

        NF_QuerySR.Input input = new NF_QuerySR.Input();
        input.chatKey = 'abc';
        input.email = 'javier.francoferrer@altran.com';
        input.srNumber = '2019-0131-T-0483';
        input.contactId = '0201301343';

        List<NF_QuerySR.Output> output = NF_QuerySR.querySR(new List<NF_QuerySR.Input>{input});

        System.debug('OUTPUT RESPONSE IS: ' + output[0].response);

        System.assertEquals(nf_ApiController.API_RESULT.SUCCESS, output[0].returnCode);

        Test.stopTest();
    }

    static void accessResponseFields(NF_QuerySRResponse res){
        res.querySRResponse.customerCaseNumber = '';
        res.querySRResponse.customerSourceID = '';
        res.querySRResponse.customerUniqueTransactionID = '';
        res.querySRResponse.responseDateTime = '';
        res.querySRResponse.serviceRequestNumber = '';
        res.querySRResponse.addSenderToEmail = false;
        res.querySRResponse.adminSRSubType = '';
        res.querySRResponse.adminSRType = '';
        res.querySRResponse.ccEmail = '';
        res.querySRResponse.priority = '';
        res.querySRResponse.srStatus = '';
        res.querySRResponse.problemDescription = '';
        res.querySRResponse.synopsis = '';
        res.querySRResponse.srOwnerEmailAddress = '';
        res.querySRResponse.srOwnerFullName = '';
        res.querySRResponse.followUpMethod = '';
        res.querySRResponse.caseType = new NF_QuerySRResponse.CaseType();
        res.querySRResponse.caseType.caseTypeCode = '';
        res.querySRResponse.caseType.caseTypeDescription = '';
        res.querySRResponse.contact = new NF_QuerySRResponse.Contact();
        res.querySRResponse.contact.accountName = '';
        res.querySRResponse.contact.contactName = '';
        res.querySRResponse.contact.contactEmail = '';
        res.querySRResponse.contact.accountID = '';
        res.querySRResponse.contact.preferredTelephoneCountryCode = '';
        res.querySRResponse.contact.preferredTelephoneExtension = '';
        res.querySRResponse.contact.preferredTelephoneNumber = '';
        res.querySRResponse.product = new NF_QuerySRResponse.Product();
        res.querySRResponse.product.serialNumber = '';
        res.querySRResponse.product.productID = '';
        res.querySRResponse.product.platform = '';
        res.querySRResponse.product.productSeries = '';
        res.querySRResponse.product.release = '';
        res.querySRResponse.product.routerName = '';
        res.querySRResponse.product.software ='';
        res.querySRResponse.product.specialRelease = '';
        res.querySRResponse.product.version = '';
        res.querySRResponse.rma = new List<NF_QuerySRResponse.RMA>{new NF_QuerySRResponse.RMA()};
        res.querySRResponse.rma[0].rmaNumber = '';
        res.querySRResponse.rma[0].items = new List<NF_QuerySRResponse.RMAItem>{new NF_QuerySRResponse.RMAItem()};
        res.querySRResponse.rma[0].items[0].itemNumber = '';
        res.querySRResponse.rma[0].items[0].itemType = '';
        res.querySRResponse.rma[0].items[0].itemStatus = '';
        res.querySRResponse.escalate = new List<NF_QuerySRResponse.Escalate>{new NF_QuerySRResponse.Escalate()};
        res.querySRResponse.escalate[0].description = '';
        res.querySRResponse.escalate[0].escalatedBy = '';
        res.querySRResponse.escalate[0].status = '';
        res.querySRResponse.attachments = new List<NF_QuerySRResponse.Attachment>{new NF_QuerySRResponse.Attachment()};
        res.querySRResponse.attachments[0].sequenceNumber = '';
        res.querySRResponse.attachments[0].title = '';
        res.querySRResponse.attachments[0].sizeInBytes = '';
        res.querySRResponse.attachments[0].path = '';
        res.querySRResponse.attachments[0].typeDescription = '';
        res.querySRResponse.attachments[0].uploadedDateTime = '';
        res.querySRResponse.attachments[0].uploadedBy = '';
    }

    @IsTest
    static void testSuccessResponse() {
        NF_QuerySRResponse response = new NF_QuerySRResponse();
        //weirdness to get around test coverage
        JSON.serialize(response);
        response = (NF_QuerySRResponse)JSON.deserialize(SUCCESS_RESPONSE, NF_QuerySRResponse.class);

        //weirdness to get around test coverage
        NF_QuerySRResponse cwa = new NF_QuerySRResponse();
        cwa.querySRResponse = new NF_QuerySRResponse.QuerySRResponse();

        system.debug(cwa);
        accessResponseFields(cwa);

        System.assertEquals(false, response.isError());
        //System.assertEquals('12345678', response.querySRResponse.customerCaseNumber);
        System.assertEquals('Successfully processed the request', response.getDescription());
    }

    @IsTest
    static void testUnauthorized(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UnauthorizedResponse());

        NF_QuerySR.Input input = new NF_QuerySR.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_QuerySR.Output> output = NF_QuerySR.querySR(new List<NF_QuerySR.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testUnauthorizedResponse() {
        NF_QuerySRResponse response = (NF_QuerySRResponse)JSON.deserialize(UNAUTHORIZED_RESPONSE, NF_QuerySRResponse.class);

        System.assertEquals(true, response.isError());
        System.assertEquals('Key not authorised', response.getDescription());
    }

    @IsTest
    static void testError(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ErrorResponse());

        NF_QuerySR.Input input = new NF_QuerySR.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_QuerySR.Output> output = NF_QuerySR.querySR(new List<NF_QuerySR.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testErrorResponse() {
        NF_QuerySRResponse response = (NF_QuerySRResponse)JSON.deserialize(ERROR_RESPONSE, NF_QuerySRResponse.class);

        System.assertEquals(true, response.isError());
        //System.assertEquals('\r\n[775] rmaNumber - R00002098 - is not associated with serviceRequestNumber - 2015-0811-T-0007', response.getDescription());
        System.assertEquals(400, response.getStatusCode());
    }

    @IsTest
    static void errorCode(){
        String jsonString = '{"querySRResponse":{"customerCaseNumber":null,"customerSourceID":"juni-chatbot","customerUniqueTransactionID":"b56a6c16-da5b-426e-8c3c-2192e845c972-002","serviceRequestNumber":null,"responseDateTime":"2020-02-05T18:59:21.858Z","statusCode":"400","status":"Error","message":"Error in processing the request","fault":[{"errorClass":"Processing","errorType":"Error","errorCode":"757","errorMessage":"Invalid serviceRequestNumber - 2019-1002-T-0305."}]}}';
        NF_QuerySRResponse response = (NF_QuerySRResponse)JSON.deserialize(jsonString, NF_QuerySRResponse.class);

        Integer errorCode = response.getErrorCode();
        System.assertEquals(757, errorCode);
    }

    private static String SUCCESS_RESPONSE = '{ "querySRResponse": { "customerCaseNumber": "1234-SWAPNA-12-Y", "customerSourceID": "juni-chatbot", "customerUniqueTransactionID": "123456", "serviceRequestNumber": "2019-0910-T-0018", "responseDateTime": "2019-11-04T04:10:31.909Z", "statusCode": "200", "status": "Success", "message": "Successfully processed the request", "caseType": { "caseTypeCode": "TEC", "caseTypeDescription": "Technical Service Request" }, "synopsis": "TEST ODATA LENGTHS", "problemDescription": null, "srStatus": "Open", "priority": "P4 - Low", "adminSRType": null, "adminSRSubType": null, "ccEmail": null, "addSenderToEmail": false, "followUpMethod": null, "srOwnerFullName": "Stacey Saxon", "srOwnerEmailAddress": "PMAURICIO##@XXJUNIPER.NETX", "contact": { "accountID": "0100004775", "accountName": "ABSI CORP", "contactName": "prateek Sethi", "contactEmail": "SETHI@GMAIL.COM", "preferredTelephoneCountryCode": "US", "preferredTelephoneNumber": "78676543457", "preferredTelephoneExtension": "12" }, "product": { "serialNumber": "JN1234512346", "productID": "SRX5400E-B1-AC", "productSeries": "ACX", "platform": "ACX1000", "version": null, "release": null, "software": null, "specialRelease": null, "routerName": null } ,"rma": [{"rmaNumber": "R200245011","items": [{"itemNumber": "100","itemType": "RMA Defective","itemStatus": "Awaiting Defective Return"}]}],"escalate": [{"description": "SR Reassignment","dateTime": "2019-12-27T19:42:50.000Z","escalatedBy": "Swapna Vadapali","status": "Dispatch"}],"attachments": [{"sequenceNumber": "0007338102","title": "log.zip","sizeInBytes": "1602","path": "S3Upload:archive/attachments/PCLR/2019/01/03/0150/20190103045839727","typeDescription": null,"uploadedDateTime": "2019-01-03T12:58:53.000Z","uploadedBy": "Fernando Garcia Soltero"}],"linkedReferences": [{"url": "http://prsearch.juniper.net/PR1412256","referenceId": "PR1412256","referenceType": "PR"}]} } ';
    private static String ERROR_RESPONSE = '{ "querySRResponse": { "customerCaseNumber": null, "customerSourceID": "juni-chatbot", "customerUniqueTransactionID": "1234", "serviceRequestNumber": "2015-0811-T-0007", "responseDateTime": "2019-11-20T03:07:13.283Z", "statusCode": "400", "status": "Error", "message": "Error in processing the request", "fault": [ { "errorClass": "Processing", "errorType": "Error", "errorCode": "775", "errorMessage": "rmaNumber - R00002098 - is not associated with serviceRequestNumber - 2015-0811-T-0007." } ] } }';
    private static String UNAUTHORIZED_RESPONSE = '{ "error": "Key not authorised" }';

    public class SuccessResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, SUCCESS_RESPONSE); }
    }

    public class UnauthorizedResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(403, UNAUTHORIZED_RESPONSE); }
    }

    public class ErrorResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(500, ERROR_RESPONSE); }
    }
}