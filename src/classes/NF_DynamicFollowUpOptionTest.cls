/**
 * @File Name          : NF_DynamicFollowUpOptionTest.cls
 * @Description        : This class is test class for NF_DynamicFollowUpOption.cls
 * @Author             : Manishi Kalra
 * @Created By         : NeuraFlash, LLC
**/
@isTest
public class NF_DynamicFollowUpOptionTest {
    @isTest
    static void getFollowUpOptionProduct() {
        List<String> inputDialog = new List<String>{'Product Details'};
        Test.startTest();
        List<List<String>> outputList = NF_DynamicFollowUpOption.getdynamicFollowUpOptions(inputDialog);
        Test.stopTest();
        System.assert(outputList.size() >0);
    }
    @isTest
    static void getFollowUpOptionRMA() {
        List<String> inputDialog = new List<String>{'RMA Details'};
        Test.startTest();
        List<List<String>> outputList = NF_DynamicFollowUpOption.getdynamicFollowUpOptions(inputDialog);
        Test.stopTest();
        System.assert(outputList.size() >0);
    }
    @isTest
    static void getFollowUpOptionCase() {
        List<String> inputDialog = new List<String>{'Case Details'};
        Test.startTest();
        List<List<String>> outputList = NF_DynamicFollowUpOption.getdynamicFollowUpOptions(inputDialog);
        Test.stopTest();
        System.assert(outputList.size() >0);
    }

}