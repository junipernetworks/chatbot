/**
 * Created by NeuraFlash LLC on 10/22/19.
 */

@IsTest
private class NF_SalesforceCaseCreationTest {
    @IsTest
    static void test1() {
        Test.startTest();
        String transid = createLiveChatTranscript();

        callCreateCase(transid);
        Test.stopTest();
    }
    @IsTest
    static void test2() {
        Test.startTest();
        String transid = createLiveChatTranscript();

        callCreateCaseNoDetails(transid);
        Test.stopTest();
    }

//    @IsTest
//    static void testFuture(){
//        NF_SalesforceCaseCreation.serviceRequestCreation('abc');
//    }

    static void callCreateCase(String transid){
        NF_SalesforceCaseCreation.Input testInput = new NF_SalesforceCaseCreation.Input();
        testInput.chatKey = transid;
        ebotdata__Bot_Session__c session = NF_Utiltest.insertBotSession(testInput.chatKey);
        session.Comments__c = 'Testing Comment';
        update session;
        List<NF_SalesforceCaseCreation.Input> inputs = new List<NF_SalesforceCaseCreation.Input>();
        inputs.add(testInput);

        List<NF_SalesforceCaseCreation.Output> results = NF_SalesforceCaseCreation.createCase(inputs);

        System.assert(results != null);
    }

    static void callCreateCaseNoDetails(String transid){
        NF_SalesforceCaseCreation.Input testInput = new NF_SalesforceCaseCreation.Input();
        testInput.chatKey = '';

        List<NF_SalesforceCaseCreation.Output> results = NF_SalesforceCaseCreation.createCase(new List<NF_SalesforceCaseCreation.Input>{testInput});

        System.assert(results != null);
    }
    static String createLiveChatTranscript(){
        LiveChatTranscript t1 = new LiveChatTranscript();
        LiveChatVisitor lcv = new LiveChatVisitor();
        try{
            insert lcv;
        }catch(Exception e){

        }
        t1.LiveChatVisitorId = lcv.Id;
        t1.ChatKey = 'somechatkey';
        insert t1;

        return t1.ChatKey;
    }
}