/**
 * Created by Jyotika on 11/21/19.
 */

@IsTest
private class NF_ProductInquiryTest {
    @IsTest
    static void test1() {
        NF_ProductInquiry.Input input = new NF_ProductInquiry.Input();
        input.productName = 'EX';

        List<NF_ProductInquiry.Output> output = NF_ProductInquiry.getProductDetailLink(new List<NF_ProductInquiry.Input>{input});

        System.assertNotEquals(null, output);
    }
    @IsTest
    static void test2() {
        NF_ProductInquiry.Input input = new NF_ProductInquiry.Input();
        input.productName = 'Routing';

        List<NF_ProductInquiry.Output> output = NF_ProductInquiry.getProductDetailLink(new List<NF_ProductInquiry.Input>{input});

        System.assertNotEquals(null, output);
    }
}