/**
 * Created by NeuraFlash LLC on 10/24/19.
 */

public with sharing class NF_UpdateUserInfo {

    public class Input{
        @InvocableVariable(required=true)
        public String chatKey = '';

        @InvocableVariable(required=false)
        public String botName = '';
    }

    public class Output{
        @InvocableVariable(required=true)
        public String userEmail = '';

        @InvocableVariable(required=true)
        public String firstName = '';

        @InvocableVariable(required=true)
        public String lastName = '';
    }

    @InvocableMethod(label='Neuraflash - Update User Info')
    public static List<Output> updateUserInfo(List<Input> inputs){
        List<Output> result = new List<Output>();
        Output out = new Output();

        String chatKey = inputs[0].chatKey;
        String botName = inputs[0].botName;

        LiveChatTranscript transcript = NF_Util.getLiveChatTranscript(chatKey);

        if(transcript != null){
            out.userEmail = transcript.C2CEmail__c;
            out.firstName = transcript.C2CFirstName__c;
            out.lastName = transcript.C2CLastName__c;
        }else{
            System.debug('>>NF_UpdateUserInfo updateUserInfo failed to retrieve LiveChatTranscript');
        }

        //Just fire a single event, the remaining fields will be copied over from the Live Chat Transcript
        if(String.isNotBlank(botName)){
            System.debug('>>>Entered to update bot name');
            ebotdata__Bot_Event__e evt = new ebotdata__Bot_Event__e(
                    ebotdata__Type__c = NF_EinsteinBotEventTriggerHandler.EVENT_UPDATE_BOT_SESSION,
                    Bot_Name__c = botName,
                    ebotdata__Live_Agent_Session_Id__c = chatKey);
            EventBus.publish(evt);
        }
//        if(String.isNotBlank(transcript.Id)){
//            System.debug('>>>Entered to update page url');
//            ebotdata__Bot_Event__e evt = new ebotdata__Bot_Event__e(
//                    ebotdata__Type__c = NF_EinsteinBotEventTriggerHandler.EVENT_ADD_PAGE_URL,
//                    ebotdata__Live_Agent_Session_Id__c = chatKey);
//            EventBus.publish(evt);
//        }

        result.add(out);
        return result;
    }
}