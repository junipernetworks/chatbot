/**
 * Created by NeuraFlash LLC on 11/4/19.
 */

@IsTest
private class NF_CheckWLIDTypeTest {
    @IsTest
    static void testValidSalesNumber() {
        String id = '10000000';
        List<NF_CheckWLIDType.Output> output = NF_CheckWLIDType.checkType(new List<String>{id});
        System.assertEquals(true, output[0].isValid);
        System.assertEquals(id, output[0].id);
    }

    @IsTest
    static void testNlpSalesNumber() {
        String id = '10000000';
        List<NF_CheckWLIDType.Output> output = NF_CheckWLIDType.checkType(new List<String>{'my id is '+id});
        System.assertEquals(true, output[0].isValid);
        System.assertEquals(id, output[0].id);
    }

    @IsTest
    static void testValidContractId() {
        String id = '60000000';
        List<NF_CheckWLIDType.Output> output = NF_CheckWLIDType.checkType(new List<String>{'my id is '+id+'.'});
        System.assertEquals(true, output[0].isValid);
        System.assertEquals(id, output[0].id);
    }

    @IsTest
    static void testInvalidId() {
        String id = '432313';
        List<NF_CheckWLIDType.Output> output = NF_CheckWLIDType.checkType(new List<String>{'my id is '+id});
        System.assertEquals(false, output[0].isValid);
        System.assertEquals(null, output[0].id);
    }


    @IsTest
    static void testIdontKnow() {


        //List<NF_CheckWLIDType.Output> output = NF_CheckWLIDType.checkType(new List<String>{'i dOn\'t KNOW it'});
        List<NF_CheckWLIDType.Output> output = NF_CheckWLIDType.checkType(new List<String>{'DONT KNOW'});
        System.assertEquals(true, output[0].isValid);
        System.assertEquals('I don\'t know', output[0].id);
    }
}