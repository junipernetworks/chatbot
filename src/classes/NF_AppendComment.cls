/**
 * Created by joaquintalon on 12/10/19.
 */

public with sharing class NF_AppendComment {
    public class Input {
        @InvocableVariable(required=true)
        public String comment;
        @InvocableVariable(required=true)
        public String chatKey;
        @InvocableVariable
        public String botVariable; //append Bot Variable if you want to.
    }

    @InvocableMethod(label='Neuraflash - Append Comment')
    public static void appendComment(List<Input> input){
        try{
            String chatKey = input[0].chatKey;
            String comment = input[0].comment;
            String botVariable = input[0].botVariable;
            ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatKey);

            if(session != null) {
                if(String.isNotBlank(botVariable)) {
                    comment = comment + ''+ botVariable;
                    session.Comments__c = comment + '\r\n';
                }
                else{
                    session.Comments__c += comment + '\r\n';
                }
                
                update session;
            }

        } catch(Exception e) {
            System.debug('Exception>>>>'+e.getMessage() + 'Stack Trace>>'+e.getStacktraceString());
        }
    }
}