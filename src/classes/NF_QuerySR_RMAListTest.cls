@isTest
public with sharing class NF_QuerySR_RMAListTest {
    @testSetup
    static void initialize() {
        NF_UtilTest.insertApiSettings(true);
        NF_UtilTest.insertBotSession('abc');
    }

    @IsTest
    static void testSuccessSR() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SuccessResponse());
		
        NF_QuerySR_RMAList.Input input = new NF_QuerySR_RMAList.Input();
        input.chatKey = 'abc';
        input.email = 'javier.francoferrer@altran.com';
        input.isSRRequest = 'true';
        input.isRMARequest = 'false';
        input.contactId = '0201301343';
        List<NF_QuerySR_RMAList.Output> output = NF_QuerySR_RMAList.querySRRMAList(new List<NF_QuerySR_RMAList.Input>{input});
        
        System.debug('OUTPUT RESPONSE IS: ' + output[0].response);

        System.assert(String.isNotBlank(output[0].response));

        Test.stopTest();
    }
    
    static void accessResponseFields(NF_QuerySR_RMAListResponse res){
        res.querySRRMAListDetailsResponse.customerCaseNumber = '';
        res.querySRRMAListDetailsResponse.customerSourceID = '';
        res.querySRRMAListDetailsResponse.customerUniqueTransactionID = '';
        res.querySRRMAListDetailsResponse.responseDateTime = '';
        res.querySRRMAListDetailsResponse.serviceRequestNumber = '';
        res.querySRRMAListDetailsResponse.rmaDetails = new List<NF_QuerySR_RMAListResponse.RMADetails>{new NF_QuerySR_RMAListResponse.RMADetails()};
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaNumber = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaHeaderStatus = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaHeaderCreatedDate = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact = new NF_QuerySR_RMAListResponse.RMAContact();
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.companyName = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.contactName = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.telephoneCountryCode = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.telephoneNumber = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address = new NF_QuerySR_RMAListResponse.Address();
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.address1 = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.address2 = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.city = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.stateCode = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.state = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.countryCode = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.country = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.postalCode = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems = new NF_QuerySR_RMAListResponse.DefectiveItem[1];
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0] = new NF_QuerySR_RMAListResponse.DefectiveItem();
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].itemNumber = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].dateTime_c = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].productID = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].serialNumber = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].trackingNumber = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].trackingNumberUrl = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].carrierDescription = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].receivedDate = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].defectiveItemStatus = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].defectiveRmaType = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].defectiveRmaSubType = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].ceItems = new NF_QuerySR_RMAListResponse.CEItem[1];
        res.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0] = new NF_QuerySR_RMAListResponse.CEItem();
        res.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].defectiveItemNumber = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].productID = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].engineerStatus = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].vendorName = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].vendorPhone = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].serviceRequestedDate = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].actualServicedDate = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].contactedDate = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].estimatedArrivalDate = '';

        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems = new NF_QuerySR_RMAListResponse.ReplacementItem[1];
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0] = new NF_QuerySR_RMAListResponse.ReplacementItem();
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].itemNumber = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].defectiveItemNumber = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].productID = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].serialNumber = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].carrierDescription = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].shipDate = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].shipmentServiceLevel = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].trackingNumber = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].replacementStatus = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].receivedDate = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].receivedBy = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].replacementRmaType = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].replacementRmaSubType = '';
        res.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].trackingNumberUrl = '';

        res.querySRRMAListDetailsResponse.srDetails = new List<NF_QuerySR_RMAListResponse.SRDetails>{new NF_QuerySR_RMAListResponse.SRDetails()};
        res.querySRRMAListDetailsResponse.srDetails[0].serviceRequestNumber = '';
        res.querySRRMAListDetailsResponse.srDetails[0].customerCaseNumber = '';
        res.querySRRMAListDetailsResponse.srDetails[0].synopsis = '';
        res.querySRRMAListDetailsResponse.srDetails[0].problemDescription = '';
        res.querySRRMAListDetailsResponse.srDetails[0].srStatus = '';
        res.querySRRMAListDetailsResponse.srDetails[0].priority = '';
        res.querySRRMAListDetailsResponse.srDetails[0].adminSRType = '';
        res.querySRRMAListDetailsResponse.srDetails[0].adminSRSubType = '';
        res.querySRRMAListDetailsResponse.srDetails[0].ccEmail = '';
        res.querySRRMAListDetailsResponse.srDetails[0].addSenderToEmail = '';
        res.querySRRMAListDetailsResponse.srDetails[0].followUpMethod = '';
        res.querySRRMAListDetailsResponse.srDetails[0].srOwnerFullName = '';
        res.querySRRMAListDetailsResponse.srDetails[0].srOwnerEmailAddress = '';
        res.querySRRMAListDetailsResponse.srDetails[0].caseType = new NF_QuerySR_RMAListResponse.CaseType();
        res.querySRRMAListDetailsResponse.srDetails[0].escalate = new List<NF_QuerySR_RMAListResponse.Escalate>{new NF_QuerySR_RMAListResponse.Escalate()};
        res.querySRRMAListDetailsResponse.srDetails[0].escalate[0].description = '';
        res.querySRRMAListDetailsResponse.srDetails[0].escalate[0].escalatedBy = '';
        res.querySRRMAListDetailsResponse.srDetails[0].escalate[0].status = '';
        res.querySRRMAListDetailsResponse.srDetails[0].attachments = new List<NF_QuerySR_RMAListResponse.Attachment>{new NF_QuerySR_RMAListResponse.Attachment()};
        res.querySRRMAListDetailsResponse.srDetails[0].attachments[0].sequenceNumber = '';
        res.querySRRMAListDetailsResponse.srDetails[0].attachments[0].title = '';
        res.querySRRMAListDetailsResponse.srDetails[0].attachments[0].sizeInBytes = '';
        res.querySRRMAListDetailsResponse.srDetails[0].attachments[0].path = '';
        res.querySRRMAListDetailsResponse.srDetails[0].attachments[0].typeDescription = '';
        res.querySRRMAListDetailsResponse.srDetails[0].attachments[0].uploadedDateTime = '';
        res.querySRRMAListDetailsResponse.srDetails[0].attachments[0].uploadedBy = '';
        res.querySRRMAListDetailsResponse.srDetails[0].product = new NF_QuerySR_RMAListResponse.Product();
        res.querySRRMAListDetailsResponse.srDetails[0].product.serialNumber = '';
        res.querySRRMAListDetailsResponse.srDetails[0].product.productID = '';
        res.querySRRMAListDetailsResponse.srDetails[0].product.productSeries = '';
        res.querySRRMAListDetailsResponse.srDetails[0].product.platform = '';
        res.querySRRMAListDetailsResponse.srDetails[0].product.version = '';
        res.querySRRMAListDetailsResponse.srDetails[0].product.release = '';
        res.querySRRMAListDetailsResponse.srDetails[0].product.software = '';
        res.querySRRMAListDetailsResponse.srDetails[0].product.specialRelease = '';
        res.querySRRMAListDetailsResponse.srDetails[0].product.routerName = '';


        res.querySRRMAListDetailsResponse.srDetails[0].contact = new NF_QuerySR_RMAListResponse.Contact();
        res.querySRRMAListDetailsResponse.srDetails[0].contact.accountID = '';
        res.querySRRMAListDetailsResponse.srDetails[0].contact.accountName = '';
        res.querySRRMAListDetailsResponse.srDetails[0].contact.contactName = '';
        res.querySRRMAListDetailsResponse.srDetails[0].contact.contactEmail = '';
        res.querySRRMAListDetailsResponse.srDetails[0].contact.preferredTelephoneCountryCode = '';
        res.querySRRMAListDetailsResponse.srDetails[0].contact.preferredTelephoneNumber = '';
        res.querySRRMAListDetailsResponse.srDetails[0].contact.preferredTelephoneExtension = '';

        res.querySRRMAListDetailsResponse.srDetails[0].rma = new NF_QuerySR_RMAListResponse.RMA[1];
        res.querySRRMAListDetailsResponse.srDetails[0].rma[0] = new NF_QuerySR_RMAListResponse.RMA();
        res.querySRRMAListDetailsResponse.srDetails[0].rma[0].rmaNumber = '';
        res.querySRRMAListDetailsResponse.srDetails[0].rma[0].itemNumber = '';
        res.querySRRMAListDetailsResponse.srDetails[0].rma[0].itemType = '';
        res.querySRRMAListDetailsResponse.srDetails[0].rma[0].itemStatus = '';

    }

    @IsTest
    static void testSuccessResponse() {
        NF_QuerySR_RMAListResponse response = new NF_QuerySR_RMAListResponse();
        //weirdness to get around test coverage
        JSON.serialize(response);
        response = (NF_QuerySR_RMAListResponse)JSON.deserialize(SUCCESS_RESPONSE, NF_QuerySR_RMAListResponse.class);

        //weirdness to get around test coverage
        NF_QuerySR_RMAListResponse cwa = new NF_QuerySR_RMAListResponse();
        cwa.querySRRMAListDetailsResponse = new NF_QuerySR_RMAListResponse.QuerySRRMAListDetailsResponse();

        system.debug(cwa);
        accessResponseFields(cwa);

        System.assertEquals(false, response.isError());
        //System.assertEquals('12345678', response.querySRResponse.customerCaseNumber);
        System.assertEquals('Successfully processed the request', response.getDescription());
    }
    @IsTest
    static void testSuccessResponseRMA() {
        NF_QuerySR_RMAListResponse response = new NF_QuerySR_RMAListResponse();
        //weirdness to get around test coverage
        JSON.serialize(response);
        response = (NF_QuerySR_RMAListResponse)JSON.deserialize(SUCCESS_RESPONSE_RMA, NF_QuerySR_RMAListResponse.class);

        //weirdness to get around test coverage
        NF_QuerySR_RMAListResponse cwa = new NF_QuerySR_RMAListResponse();
        cwa.querySRRMAListDetailsResponse = new NF_QuerySR_RMAListResponse.QuerySRRMAListDetailsResponse();

        system.debug(cwa);
        accessResponseFields(cwa);

        System.assertEquals(false, response.isError());
        //System.assertEquals('12345678', response.querySRResponse.customerCaseNumber);
        System.assertEquals('Successfully processed the request', response.getDescription());
    }

    @IsTest
    static void testUnauthorized(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UnauthorizedResponse());

        NF_QuerySR_RMAList.Input input = new NF_QuerySR_RMAList.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_QuerySR_RMAList.Output> output = NF_QuerySR_RMAList.querySRRMAList(new List<NF_QuerySR_RMAList.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testUnauthorizedResponse() {
        NF_QuerySR_RMAListResponse response = (NF_QuerySR_RMAListResponse)JSON.deserialize(UNAUTHORIZED_RESPONSE, NF_QuerySR_RMAListResponse.class);

        System.assertEquals(true, response.isError());
        System.assertEquals('Key not authorised', response.getDescription());
    }

    @IsTest
    static void testError(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ErrorResponse());

        NF_QuerySR_RMAList.Input input = new NF_QuerySR_RMAList.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_QuerySR_RMAList.Output> output = NF_QuerySR_RMAList.querySRRMAList(new List<NF_QuerySR_RMAList.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testErrorResponse() {
        NF_QuerySR_RMAListResponse response = (NF_QuerySR_RMAListResponse)JSON.deserialize(ERROR_RESPONSE, NF_QuerySR_RMAListResponse.class);

        System.assertEquals(true, response.isError());
        //System.assertEquals('\r\n[775] rmaNumber - R00002098 - is not associated with serviceRequestNumber - 2015-0811-T-0007', response.getDescription());
        System.assertEquals(400, response.getStatusCode());
    }

    private static String SUCCESS_RESPONSE = '{"querySRRMAListDetailsResponse": {"statusCode": "200","status": "Success","message": "Successfully processed the request","customerSourceID": "juni-chatbot","customerUniqueTransactionID": "1234","responseDateTime": "2020-01-07T22:48:04.557Z","srDetails": [{"serviceRequestNumber": "2019-1226-T-0002","customerCaseNumber": "67588","caseType": {"caseTypeCode": "TEC","caseTypeDescription": "Technical Service Request"},"synopsis": "synopsis 1","problemDescription": null,"srStatus": "Open","priority": "P1 - Critical","adminSRType": null,"adminSRSubType": null,"ccEmail": "JAIKANTH@JUNIPER.NET,PENG@JUNIPER.NET", "addSenderToEmail": true,"followUpMethod": null,"srOwnerFullName": "PFAL TES01","srOwnerEmailAddress": "PFALTEST01@JUNIPER.NET","contact": {"accountID": "100004775","accountName": "ABSI CORP","contactName": "KTEST CONT KTSTCONT PERSON","contactEmail": "SETHI1@GMAIL.COM","preferredTelephoneCountryCode": null,"preferredTelephoneNumber": null,"preferredTelephoneExtension": null},"rma": [{"rmaNumber": "R200002449","itemNumber": "0000000100","itemType": "RMA Defective","itemStatus": "Awaiting Defective Return"}],"product": {"serialNumber": "CABM5577","productID": "EX9208-CHAS-S","productSeries": "EX-Series","platform": "EX4300","version": "R1-S7","release": "17.2","software": null,"specialRelease": null,"routerName": "TEST_SV"},"escalate": [{"description": "Faster RMA Progress","dateTime": "2019-12-27T20:50:46.000Z","escalatedBy": null,"status": "Dispatch"}],"attachments": [{"sequenceNumber": "0000003157","title": "test","sizeInBytes": "100","path": "/n/d/test attachment update","typeDescription": "Word, PPT, Excel, PDF  documents","uploadedDateTime": "2019-12-26T21:51:19.000Z","uploadedBy": "prateek Sethi"}],"linkedReferences": [{"url": "KBSAP@com","referenceId": "KB","referenceType": null}]}]}}';
    private static String ERROR_RESPONSE = '{ "querySRRMAListDetailsResponse": { "customerCaseNumber": null, "customerSourceID": "juni-chatbot", "customerUniqueTransactionID": "1234", "serviceRequestNumber": "2015-0811-T-0007", "responseDateTime": "2019-11-20T03:07:13.283Z", "statusCode": "400", "status": "Error", "message": "Error in processing the request", "fault": [ { "errorClass": "Processing", "errorType": "Error", "errorCode": "775", "errorMessage": "rmaNumber - R00002098 - is not associated with serviceRequestNumber - 2015-0811-T-0007." } ] } }';
    private static String UNAUTHORIZED_RESPONSE = '{ "error": "Key not authorised" }';
    private static String SUCCESS_RESPONSE_RMA = '{"querySRRMAListDetailsResponse": {"statusCode": "200","status": "Success","message": "Successfully processed the request","customerSourceID": "juni-chatbot","customerUniqueTransactionID": "1234","responseDateTime": "2020-01-07T20:03:06.145Z","rmaDetails": [{"rmaNumber": "R200002396","rmaHeaderStatus": "Open","rmaHeaderCreatedDate": "2019-12-05T09:38:48.000Z","rmaContact": {"companyName": "SOSU NYKOBING F","contactName": "geetha","contactEmail": "geetha@XXgmail.comX","telephoneCountryCode": "DK","telephoneNumber": "123","address": {"address1": "VESTENSBORG ALLE 78","address2": null,"city": "NYKOBING","stateCode": null,"state": null, "countryCode": "DK","country": "Denmark","postalCode": "4800"}},"defectiveItems": [{"itemNumber": "100","dateTime": "2019-12-05T09:41:36.000Z","productID": "EX4300-48T","serialNumber": "PE3713330142","trackingNumber": null,"trackingNumberUrl": null,"carrierDescription": null, "receivedDate": null,"defectiveItemStatus": "Awaiting Defective Return","defectiveRmaType": null,"defectiveRmaSubType": null}],"replacementItems": [{"itemNumber": "110","defectiveItemNumber": "100","productID": "2WFXO-RTM","serialNumber": "PE3713330142","carrierDescription": null,"shipDate": null,"shipmentServiceLevel": "Standard Warranty Hardware","trackingNumber": "T12","trackingNumberUrl": null,"replacementStatus": "RMA Delivered - Closed","receivedDate": "2019-12-05T05:00:00.000Z","receivedBy": "GEETHA","replacementRmaSubType": null}],"ceItems": [{"defectiveItemNumber": "100","productID": "CEONSITE","engineerStatus": null,"vendorName": "SWAPNA-1","vendorPhone": "7323452345","serviceRequestedDate": null,"actualServicedDate": null,"contactedDate": null,"estimatedArrivalDate": null}]}]}}';
    public class SuccessResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, SUCCESS_RESPONSE); }
    }
    public class SuccessResponseRMA implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, SUCCESS_RESPONSE_RMA); }
    }

    public class UnauthorizedResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(403, UNAUTHORIZED_RESPONSE); }
    }

    public class ErrorResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(500, ERROR_RESPONSE); }
    }
}