/**
 * Created by NeuraFlash LLC on 11/5/19.
 */

@IsTest
private class NF_GetRandomResponseTest {
    @IsTest
    static void test1() {
        NF_GetRandomResponse.Input input = new NF_GetRandomResponse.Input();
        input.intentName = 'nonexistent';
        System.assert(NF_GetRandomResponse.getResponse(new List<NF_GetRandomResponse.Input>{input})[0].response == null);
    }
}

/*
valid
{
    "access_token": "568235ee1cc2370001000001548f17cd15df446b8d7081cf07f29a2c",
    "expires_in": 3600,
    "token_type": "bearer"
}

 */

/*
invalid
{
    "error": "unsupported_grant_type",
    "error_description": "The authorization grant type is not supported by the authorization server."
}
 */