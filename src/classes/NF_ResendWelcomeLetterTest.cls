/**
 * Created by NeuraFlash LLC on 10/18/19.
 */

@IsTest
private class NF_ResendWelcomeLetterTest {

    @IsTest
    static void testRequestCIDSONDiscrimination(){
        NF_ResendWelcomeLetterRequest req = new NF_ResendWelcomeLetterRequest('xxx');
        req.setContractId('10000000');
        System.assert(req.resendWelcomeLetterRequest.contractInformation.salesOrderNumber == '10000000');
        req.setContractId('60000000');
        System.assert(req.resendWelcomeLetterRequest.contractInformation.contractId == '60000000');
    }

    @IsTest
    static void testSuccess() {
        NF_UtilTest.insertApiSettings(true);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SuccessResponse());

        NF_ResendWelcomeLetter.Input input = new NF_ResendWelcomeLetter.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';
        input.contactId = '123';
        input.contractId = '123';

        List<NF_ResendWelcomeLetter.Output> output = NF_ResendWelcomeLetter.resendWelcomeLetter(new List<NF_ResendWelcomeLetter.Input>{
                input
        });

        System.assertEquals(0, output[0].returnCode);

        Test.stopTest();
    }

    private static String SUCCESS_RESPONSE   = '{"resendWelcomeLetterResponse":{"customerCaseNumber":null,"customerSourceID":"juni-chatbot","customerUniqueTransactionID":"123456","salesOrderNumber":null,"contractId":"60581017","responseDateTime":"2019-12-05T11:41:58.436Z","statusCode":"200","status":"Success","message":"Successfully processed the request","fault":[{"errorClass":"Processing","errorCode":"808","errorMessage":"Welcome letter sent to E-mail ID - juniperwayjason@gmail.com -for contarct - 0060581017."}]}}';
    private static String NOT_FOUND_RESPONSE = '{"resendWelcomeLetterResponse":{"customerCaseNumber":null,"customerSourceID":"juni-chatbot","customerUniqueTransactionID":"12345116","salesOrderNumber":null,"contractId":"61581018","responseDateTime":"2019-12-05T16:25:55.495Z","statusCode":"400","status":"Error","message":"Error in processing the request","fault":[{"errorClass":"Processing","errorType":"Error","errorCode":"802","errorMessage":"Invalid ContractId - 0061581018."}]}}';


    @IsTest
    static void testSuccessResponse() {
        NF_ResendWelcomeLetterResponse response = new NF_ResendWelcomeLetterResponse();
        //weirdness to get around test coverage
        JSON.serialize(response);
        response = (NF_ResendWelcomeLetterResponse)JSON.deserialize(SUCCESS_RESPONSE, NF_ResendWelcomeLetterResponse.class);

        System.assertEquals(false, response.isError());
        System.assertEquals('\r\n[808] Welcome letter sent to E-mail ID - juniperwayjason@gmail.com -for contarct - 0060581017.', response.getDescription());

        System.assertNotEquals('0060581017', response.resendWelcomeLetterResponse.contractId);
    }

    @IsTest
    static void testNotFound() {
        NF_UtilTest.insertApiSettings(true);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new NotFoundResponse());

        NF_ResendWelcomeLetter.Input input = new NF_ResendWelcomeLetter.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';
        input.contactId = '123';
        input.contractId = '123';

        List<NF_ResendWelcomeLetter.Output> output = NF_ResendWelcomeLetter.resendWelcomeLetter(new List<NF_ResendWelcomeLetter.Input>{
                input
        });

        System.assertEquals(1, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testNotFoundResponse() {
        NF_ResendWelcomeLetterResponse apiResponse = new NF_ResendWelcomeLetterResponse();
        //weirdness to get around test coverage
        JSON.serialize(apiResponse);
        apiResponse = (NF_ResendWelcomeLetterResponse)JSON.deserialize(NOT_FOUND_RESPONSE, NF_ResendWelcomeLetterResponse.class);


        System.assertEquals(false, apiResponse.isError());
        System.assertEquals(true, apiResponse.isNotFound());
    }


    @IsTest
    static void testFailure() {
        String responseJson = '{"resendWelcomeLetterResponse":{"customerCaseNumber":null,"customerSourceID":"juni-chatbot","customerUniqueTransactionID":"123456","salesOrderNumber":null,"contractId":"60625375","responseDateTime":"2019-11-25T20:10:18.791Z","statusCode":"400","status":"Error","message":"Error in processing the request","fault":[{"errorClass":"Processing","errorType":"Error","errorCode":"812","errorMessage":"SalesorderNumber or Contractid -0060625375 - is not relevant for Welcome Letter per visibility check. Please reach out Juniper Customer care Team."}]}}';

        NF_ResendWelcomeLetterResponse apiResponse = (NF_ResendWelcomeLetterResponse)JSON.deserialize(responseJson, NF_ResendWelcomeLetterResponse.class);

        System.assertEquals('\r\n[812] SalesorderNumber or Contractid -0060625375 - is not relevant for Welcome Letter per visibility check. Please reach out Juniper Customer care Team.', apiResponse.getDescription());
    }

    @IsTest
    static void testUnauthorized() {
        String responseJson = '{ "error": "Authorization field missing" }';

        NF_ResendWelcomeLetterResponse apiResponse = (NF_ResendWelcomeLetterResponse)JSON.deserialize(responseJson, NF_ResendWelcomeLetterResponse.class);

        System.assertEquals('Authorization field missing', apiResponse.getDescription());
    }

    @IsTest
    static void testResponse() {
        NF_ResendWelcomeLetterResponse response = new NF_ResendWelcomeLetterResponse();
        response.resendWelcomeLetterResponse = new NF_ResendWelcomeLetterResponse.ResendWelcomeLetterResponse();
        response.resendWelcomeLetterResponse.responseDateTime = '';
        response.resendWelcomeLetterResponse.customerCaseNumber = '';
        response.resendWelcomeLetterResponse.customerUniqueTransactionID = '';
        response.resendWelcomeLetterResponse.contractId = '';
        response.resendWelcomeLetterResponse.salesOrderNumber = '';
        response.resendWelcomeLetterResponse.customerSourceID = '';
        response.resendWelcomeLetterResponse.message = '';
        response.resendWelcomeLetterResponse.statusCode = '';
        response.resendWelcomeLetterResponse.status = '';

        response.isError();
    }

    public class SuccessResponse implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, SUCCESS_RESPONSE); }
    }

    public class NotFoundResponse implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, NOT_FOUND_RESPONSE); }
    }
}