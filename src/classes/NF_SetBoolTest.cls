/**
 * Created by NeuraFlash LLC on 11/4/19.
 */

@IsTest
private class NF_SetBoolTest {
    @IsTest
    static void test1() {
        System.assert(NF_SetBool.setBool(new List<String>{'true'})[0]);
    }
    @IsTest
    static void test2() {
        System.assert(!NF_SetBool.setBool(new List<String>{'false'})[0]);
    }
}