/**
 * Created by mcasella on 2019-11-19.
 */

public with sharing class NF_GetAccessTokenRequest extends NF_ApiRequest {

    public NF_GetAccessTokenRequest(){
        String clientId = NF_ApiController.clientId;
        String clientSecret = NF_ApiController.clientSecret;

        client_id = EncodingUtil.urlEncode(clientId, 'UTF-8');
        client_secret = EncodingUtil.urlEncode(clientSecret, 'UTF-8');
        grant_type = EncodingUtil.urlEncode('client_credentials', 'UTF-8');
    }

    public override String getApiName(){
        return 'Get Access Token';
    }

    public override String getEndpoint(){
        return 'callout:Juniper_API_Gateway/v01/oauth/token/';
    }

    /* No Transaction Id for this request */
    public override String getTransactionId(){
        return '';
    }

    public override Map<String, String> getHeaders(){
        Map<String, String> headers = super.getHeaders();

        headers.put('Content-Type', 'application/x-www-form-urlencoded');
        headers.put('Authorization', getAuthToken())
                ;
        return headers;
    }

    public override String getBody(){
        return 'client_id=' + client_id + '&client_secret='+client_secret+'&grant_type='+grant_type;
    }

    public String getAuthToken(){
        System.debug('NF_ApiRequestWrapper_GetAccessToken.getAuthToken()');
        Blob idSecret = Blob.valueOf(client_id + ':' + client_secret);
        System.debug('NF_ApiRequestWrapper_GetAccessToken.getAuthToken(): idSecret='+idSecret);

        String token = 'Basic ' + EncodingUtil.base64Encode(idSecret);
        System.debug('NF_ApiRequestWrapper_GetAccessToken.getAuthToken(): token='+token);

        return token;
    }

    public String client_id { get; set; }
    public String client_secret { get; set; }
    public String grant_type = 'client_credentials';
}