/**
 * Created by mcasella on 2019-11-17.
 */

public with sharing class NF_QuerySRRequest extends NF_ApiRequest {
    public NF_QuerySRRequest(String chatKey){
        querySRRequest = new QuerySRRequest();
        querySRRequest.caseInformation = new CaseInformation();
        querySRRequest.caseInformation.customerUniqueTransactionID = NF_Util.generateTransactionId(chatKey);
        querySRRequest.contact = new Contact();
    }

    public void setContactId(String id){
        querySRRequest.contact.contactId = id;
    }

    public void setEmail(String email){
        querySRRequest.contact.contactEmail = email;
    }

    public void setAccountId(String id){
        querySRRequest.contact.accountID = id;
    }

    public override String getApiName(){
        return 'Query SR';
    }

    public override String getEndpoint(){
        return 'callout:Juniper_API_Gateway/v01/querysr';
    }

    public override String getTransactionId(){
        return querySRRequest != null ? querySRRequest.caseInformation.customerUniqueTransactionID : '';
    }

    public QuerySRRequest querySRRequest;

    public class QuerySRRequest extends AbstractRequest{
        public CaseInformation caseInformation { get; set; }
        public String srNumber { get; set; }
        public Contact contact { get; set; }
    }

    public class CaseInformation{
        public String customerSourceID = 'juni-chatbot';
        public String customerUniqueTransactionID { get; set; }
        public String customerCaseNumber { get; set; }
        public String serviceRequestNumber { get; set; }
    }
}