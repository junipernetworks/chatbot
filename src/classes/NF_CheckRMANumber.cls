public with sharing class NF_CheckRMANumber {
 
    public class Input{
        @InvocableVariable(required=true)
        public String rmaNumber;
    }
    
    public class Output{
        @InvocableVariable
        public Boolean isValid;

        @InvocableVariable(required=false)
        public String rmaNumber;
    }

    /*@InvocableMethod(label='Neuraflash - RMA Number Validation')
    public static List<Output> validateRMANumber(List<Input> input) {
        List<Output> output = new List<Output>();

        Output out = new Output();
        out.isValid = false;
        String userInput = input[0].rmaNumber;

        try{
            Pattern pattern = Pattern.compile('.*?([R]{1}\\d{9}|\\d{9}).*');
            Matcher matcher = pattern.matcher(userInput);

            out.isValid = matcher.matches();
            out.rmaNumber = out.isValid ? matcher.group(1) : '';
        }
        catch(Exception e){
            NF_EinsteinBotLogger.logException(e);
        }*/
        @InvocableMethod(label='Neuraflash - Validate RMA Number')
        public static List<Output> validateRMANumber(List<Input> input) {
            List<Output> output = new List<Output>();

            Output out = new Output();
            out.isValid = false;
            String userInput = input[0].rmaNumber.toUpperCase();

            try{
                Pattern pattern = Pattern.compile('.*?([R]{1}\\d{9}|\\d{9}).*');
                Matcher matcher = pattern.matcher(userInput);

                out.isValid = matcher.matches();
                out.rmaNumber = out.isValid ? matcher.group(1) : '';
            }
            catch(Exception e){
                NF_EinsteinBotLogger.logException(e);
            }

        output.add(out);
        return output;
    }

}