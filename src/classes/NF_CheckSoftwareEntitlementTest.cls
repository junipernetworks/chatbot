@isTest
public with sharing class NF_CheckSoftwareEntitlementTest {
    @testSetup
    static void initialize() {
        NF_UtilTest.insertApiSettings(true);
    }

    @IsTest
    static void testSuccess() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SuccessResponse());

        NF_CheckSoftwareEntitlement.Input input = new NF_CheckSoftwareEntitlement.Input();
        input.chatKey = 'abc';
        input.email = 'juniperwayjason@gmail.com';
        input.ssrnOrserialnumber = '1539851713000';
        input.contactId = '0201123987';

        List<NF_CheckSoftwareEntitlement.Output> output = NF_CheckSoftwareEntitlement.checkSoftwareEntitlement(new List<NF_CheckSoftwareEntitlement.Input>{input});

        System.debug('OUTPUT RESPONSE IS: ' + output[0].response);

        System.assertEquals(nf_ApiController.API_RESULT.SUCCESS, output[0].returnCode);

        Test.stopTest();
    }

    static void accessResponseFields(NF_CheckSoftwareEntitlementResponse res){
        res.swEntitlementCheckResponse.customerCaseNumber = '';
        res.swEntitlementCheckResponse.customerSourceID = '';
        res.swEntitlementCheckResponse.customerUniqueTransactionID = '';
        res.swEntitlementCheckResponse.responseDateTime = '';
        res.swEntitlementCheckResponse.osDetails = new List<NF_CheckSoftwareEntitlementResponse.OSDetails>{new NF_CheckSoftwareEntitlementResponse.OSDetails()};
        res.swEntitlementCheckResponse.osDetails[0].modelShortName = '';
        res.swEntitlementCheckResponse.osDetails[0].osId = '';
        res.swEntitlementCheckResponse.osDetails[0].osDisplayName = '';
        res.swEntitlementCheckResponse.osDetails[0].srId = '';
        
    }

    @IsTest
    static void testSuccessResponse() {
        NF_CheckSoftwareEntitlementResponse response = new NF_CheckSoftwareEntitlementResponse();
        //weirdness to get around test coverage
        JSON.serialize(response);
        response = (NF_CheckSoftwareEntitlementResponse)JSON.deserialize(SUCCESS_RESPONSE, NF_CheckSoftwareEntitlementResponse.class);

        //weirdness to get around test coverage
        NF_CheckSoftwareEntitlementResponse cwa = new NF_CheckSoftwareEntitlementResponse();
        cwa.swEntitlementCheckResponse = new NF_CheckSoftwareEntitlementResponse.SWEntitlementCheckResponse();
        cwa.swEntitlementCheckResponse.osDetails = new List<NF_CheckSoftwareEntitlementResponse.osDetails>{new NF_CheckSoftwareEntitlementResponse.osDetails()};

        accessResponseFields(cwa);

        System.assertEquals(false, response.isError());
        //System.assertEquals('12345678', response.queryRMAResponse.customerCaseNumber);
        System.assertEquals('Successfully processed the request', response.getDescription());
    }

    @IsTest
    static void testUnauthorized(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UnauthorizedResponse());

        NF_CheckSoftwareEntitlement.Input input = new NF_CheckSoftwareEntitlement.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_CheckSoftwareEntitlement.Output> output = NF_CheckSoftwareEntitlement.checkSoftwareEntitlement(new List<NF_CheckSoftwareEntitlement.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testUnauthorizedResponse() {
        NF_CheckSoftwareEntitlementResponse response = (NF_CheckSoftwareEntitlementResponse)JSON.deserialize(UNAUTHORIZED_RESPONSE, NF_CheckSoftwareEntitlementResponse.class);

        System.assertEquals(true, response.isError());
        System.assertEquals('Key not authorised', response.getDescription());
    }

    @IsTest
    static void testError(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ErrorResponse());

        NF_CheckSoftwareEntitlement.Input input = new NF_CheckSoftwareEntitlement.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_CheckSoftwareEntitlement.Output> output = NF_CheckSoftwareEntitlement.checkSoftwareEntitlement(new List<NF_CheckSoftwareEntitlement.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testErrorResponse() {
        NF_CheckSoftwareEntitlementResponse response = (NF_CheckSoftwareEntitlementResponse)JSON.deserialize(ERROR_RESPONSE, NF_CheckSoftwareEntitlementResponse.class);

        System.assertEquals(true, response.isError());
        //System.assertEquals('\r\n[775] rmaNumber - R00002098 - is not associated with serviceRequestNumber - 2015-0811-T-0007', response.getDescription());
        System.assertEquals(400, response.getStatusCode());
    }

    /* start JCSS-279 */
    /* Start of Test methods for first queue job (Patform to Role API) */
    /* Success scenario */
    @IsTest
    static void testPlatformSuccess() {
        Test.startTest();
        String chatKey = 'abc';
        ebotdata__Bot_Session__c botSession = NF_Utiltest.insertBotSession(chatKey);
		botSession.IsPlatformToRoleSuccess__c = true;
        update botSession;
        Test.setMock(HttpCalloutMock.class, new PlatformSuccessResponse());
        NF_QueueablePlatformRoleAPI queuePlatform = new NF_QueueablePlatformRoleAPI('abc', '', 'srx4600', '183', 'juniperwayjason@gmail.com');
        System.enqueueJob(queuePlatform);
        Test.stopTest();
    }
    /* End of Test methods for first queue job (Patform to Role API) */


    /* Start of Test methods for second queue job (Assign Role API) */
    /* Success scenario */
    @IsTest
    static void testRoleApiSuccess() {
        Test.startTest();
        String chatKey = 'abc';
        ebotdata__Bot_Session__c botSession = NF_Utiltest.insertBotSession(chatKey);
		botSession.IsAssignRoleSuccess__c = true;
        update botSession;
        Test.setMock(HttpCalloutMock.class, new RoleApiSuccessResponse());
        NF_QueueableRoleAssign queuePlatform = new NF_QueueableRoleAssign('abc', '', 'JUNOS, Encryption', 'juniperwayjason@gmail.com', 'srx4600', '183');
        System.enqueueJob(queuePlatform);
        Test.stopTest();
    }
    /* End of Test methods for second queue job (Assign Role API) */
    /* end JCSS-279 */

    private static String SUCCESS_RESPONSE = '{"swEntitlementCheckResponse" : {"customerCaseNumber" : null,"customerSourceID" : "juni-chatbot","customerUniqueTransactionID" : "028c18e0-dfab-44d2-b2c9-a1f81fbfe073-001","ssrnOrserialnumber" : "1539851713000","responseDateTime" : "2020-03-04T09:12:08.957Z","statusCode" : "200","status" : "Success","message" : "Successfully processed the request","osDetails" : [ {"srId" : null,"osId" : "183","modelShortName" : "srx4600","osDisplayName" : "Junos"} ]}}';
    private static String ERROR_RESPONSE = '{ "swEntitlementCheckResponse" : { "customerCaseNumber" : null, "customerSourceID" : "juni-chatbot", "customerUniqueTransactionID" : "swEntitlementCheck0gvh12720202108", "ssrnOrserialnumber" : "SER11-180049", "responseDateTime" : "2020-02-18T09:26:29.608Z", "statusCode" : "400", "status" : "Error", "message" : "Error in processing the request", "fault" : [ { "errorClass" : "Processing", "errorType" : "Error", "errorCode" : "827", "errorMessage" : "SN/SSRN - SER11-180049, Associated Equipment ID is not installed status .Please reach out customer care." }, { "errorClass" : "Processing", "errorType" : "Error", "errorCode" : "862", "errorMessage" : "SWDL Entitlement Failed for SN/SSRN- SER11-180049." } ] } }';
    private static String UNAUTHORIZED_RESPONSE = '{ "error": "Key not authorised" }';
    private static String SUCCESS_PLATFORM = '{"queryPlatformToRoleDetailsResponse" : {"customerSourceID" : "juni-chatbot","customerUniqueTransactionID" : "028c18e0-dfab-44d2-b2c9-a1f81fbfe073-002","responseDateTime" : "2020-03-04T09:12:10.003Z","statusCode" : "200","status" : "Success","message" : "Successfully processed the request","roleOperator" : "And","roles" : [ {"roleName" : "JUNOS"}, {"roleName" : "Encryption"} ]}}';
	private static String PLATFORM_ERROR_RESPONSE = '{"queryPlatformToRoleDetailsResponse": {"customerSourceID": "juni-chatbot","customerUniqueTransactionID": "getPlatformToRoleDetails-021820201113","responseDateTime": "2020-02-18T19:21:29.450Z","statusCode": "400","status": "Error","message": "Error in processing the request","fault": [{"errorClass": "FAILURE","errorType": "FAILURE","errorCode": "A001","errorMessage": "OS and Platform are not related"}]}}';
    private static String Role_API_SUCCESS = '{"assignRoleResponse" : {"customerSourceID" : "juni-chatbot","customerUniqueTransactionID" : "028c18e0-dfab-44d2-b2c9-a1f81fbfe073-003","responseDateTime" : "2020-03-04T09:12:10.912Z","statusCode" : "200","status" : "Success","message" : "Successfully processed the request","encryption" : "YES"}}';
    private static String Role_API_ERROR = '{"assignRoleRequest" : {"appId" : "URJxgQGSBrm8JuQ0eGPKSt1UqqF13D8eONChGlc8","userId" : "chatbot-support@juniper.net","requestDateTime" : "2019-11-18T07:34:28.176Z","caseInformation" : {"customerSourceID" : "juni-chatbot","customerUniqueTransactionID" : "1234"},"assignRoleDetails" : {"emailAddress" : "test@qwest.com","roles" : "JUNOS-EV"}}}';

    public class SuccessResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, SUCCESS_RESPONSE); }
    }

    public class UnauthorizedResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(403, UNAUTHORIZED_RESPONSE); }
    }

    public class ErrorResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(500, ERROR_RESPONSE); }
    }

    /* JCSS-279 */
    /* start of mock callout for Platfprm to role api */
    public class PlatformSuccessResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, SUCCESS_PLATFORM); }
    }
    public class PlatformErrorResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(500, PLATFORM_ERROR_RESPONSE); }
    }
    /* end of mock callout for Platfprm to role api */

    /* start of mock callout for assign role api */
    public class RoleApiSuccessResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, Role_API_SUCCESS); }
    }
    public class RoleApiErrorResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(500, Role_API_ERROR); }
    }
    /* end of mock callout for assign role api */
}