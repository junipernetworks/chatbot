/**
 * Created by NeuraFlash LLC on 11/6/19.
 */

@IsTest
private class NF_EinsteinBotLoggerTest {
    public class TestException extends Exception{

    }
    public class GenericObject{

    }
    @IsTest
    static void test1() {
        //Test.startTest();
        System.assert(NF_EinsteinBotLogger.populatedLog(new TestException('test')));
        //Test.stopTest();
    }
    @IsTest
    static void test2() {
        //Test.startTest();
        System.assert(NF_EinsteinBotLogger.populatedLog(new GenericObject()));
        //Test.stopTest();
    }
    @IsTest
    static void test3() {
        System.assert(NF_EinsteinBotLogger.getExceptionData(new TestException('test')) != null);
    }

    @IsTest
    static void logString(){
        NF_EinsteinBotLogger.logAPIException('an error');
        System.assert(true);
    }
}