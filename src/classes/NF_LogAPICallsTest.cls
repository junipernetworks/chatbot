@IsTest
public class NF_LogAPICallsTest {

    @isTest
    static void logApiCall(){
        String chatKey = 'abc';
        NF_ApiLog log = getGetUserProfileRequestLog(chatKey);
        NF_LogAPICalls.logApiCall(log);
    }

    @isTest
    static void logApiCallFail(){
        NF_LogAPICalls.logApiCall(null);
    }

    @IsTest
    static void logApiCallASyncGetUserProfile(){
        String chatKey = 'abc';
        NF_ApiLog log = getGetUserProfileRequestLog(chatKey);
        ebotdata__Bot_Session__c session = NF_UtilTest.insertBotSession(chatKey);

        Test.startTest();
        NF_LogAPICalls.logApiCallAsync(chatKey, true, JSON.serialize(log));
        Test.stopTest();

        ebotdata__Bot_Session__c updatedSession = getSession(chatKey);
        System.assertEquals(JSON.serialize(log), updatedSession.API_Log_Get_User_Profile__c);
    }

    @IsTest
    static void logApiCallsASync(){
        String chatKey = 'abc';
        NF_ApiLog log = getGetUserProfileRequestLog(chatKey);
        NF_ApiLog log2 = getSomeOtherRequestLog(chatKey);
        ebotdata__Bot_Session__c session = NF_UtilTest.insertBotSession(chatKey);

        Test.startTest();
        NF_LogAPICalls.logApiCallAsync(chatKey, true, JSON.serialize(log));
        NF_LogAPICalls.logApiCallAsync(chatKey, false, JSON.serialize(log2));
        Test.stopTest();

        ebotdata__Bot_Session__c updatedSession = getSession(chatKey);
        List<NF_ApiLog> logs = new List<NF_ApiLog>{log, log2};
        List<NF_ApiLog> sessionLogs = (List<NF_ApiLog>)JSON.deserialize(updatedSession.Api_Logs__c, List<NF_ApiLog>.class);
        System.assertEquals(logs.size(), sessionLogs.size());
        System.assertEquals(logs.get(0).isGetUserProfileRequest, sessionLogs.get(0).isGetUserProfileRequest);
    }

    private static NF_ApiLog getGetUserProfileRequestLog(String chatKey){
        NF_GetUserProfileRequest request = new NF_GetUserProfileRequest('test@nf.com');
        NF_ApiLog apiLog = new NF_ApiLog(request);
        apiLog.chatKey = chatKey;

        return apiLog;
    }

    private static NF_ApiLog getSomeOtherRequestLog(String chatKey){
        NF_QueryRMARequest request = new NF_QueryRMARequest(chatKey);
        NF_ApiLog apiLog = new NF_ApiLog(request);
        apiLog.chatKey = chatKey;

        return apiLog;
    }

    private static ebotdata__Bot_Session__c getSession(String chatKey){
        return [SELECT Id, API_Log_Get_User_Profile__c, Api_Logs__c
                FROM ebotdata__Bot_Session__c
                WHERE ebotdata__Live_Agent_Session_Id__c = :chatKey];
    }
}