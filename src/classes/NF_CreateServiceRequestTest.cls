/**
 * Created by mcasella on 2019-11-17.
 */

@isTest
public with sharing class NF_CreateServiceRequestTest {
    @testSetup
    static void initialize() {
        NF_UtilTest.insertApiSettings(true);
        NF_UtilTest.insertLiveChatTranscript('abc', 'test.com', '');
    }

    @IsTest
    static void testSuccess() {
        String chatKey = 'abc';

        NF_UtilTest.insertBotSession(chatKey);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SuccessResponse());

        NF_CreateServiceRequest.Input input = new NF_CreateServiceRequest.Input();
        input.chatKey = chatKey;
        input.email = 'test@neuraflash.com';
        input.accountId = '123';

        List<NF_CreateServiceRequest.Output> output = NF_CreateServiceRequest.createServiceRequest(new List<NF_CreateServiceRequest.Input>{input});

        Test.stopTest();
        Boolean createdSR = [SELECT ID, Created_SAP_Service_Request__c FROM ebotdata__Bot_Session__c WHERE ebotdata__Live_Agent_Session_Id__c =: 'abc'].Created_SAP_Service_Request__c;
        System.assertEquals(true, createdSR);
    }

    @IsTest
    static void testSuccessResponse() {
        NF_CreateServiceRequestResponse response = new NF_CreateServiceRequestResponse();
        //weirdness to get around test coverage
        JSON.serialize(response);
        response = (NF_CreateServiceRequestResponse)JSON.deserialize(SUCCESS_RESPONSE, NF_CreateServiceRequestResponse.class);


        System.assertEquals(false, response.isError());
        System.assertEquals('12345678', response.createSRResponse.customerCaseNumber);
        System.assertEquals('Successfully processed the request', response.getDescription());
    }

    @IsTest
    static void testUnauthorized(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UnauthorizedResponse());

        NF_CreateServiceRequest.Input input = new NF_CreateServiceRequest.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_CreateServiceRequest.Output> output = NF_CreateServiceRequest.createServiceRequest(new List<NF_CreateServiceRequest.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testUnauthorizedResponse() {
        NF_CreateServiceRequestResponse response = (NF_CreateServiceRequestResponse)JSON.deserialize(UNAUTHORIZED_RESPONSE, NF_CreateServiceRequestResponse.class);

        System.assertEquals(true, response.isError());
        System.assertEquals('Key not authorised', response.getDescription());
    }

    @IsTest
    static void testError(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ErrorResponse());

        NF_CreateServiceRequest.Input input = new NF_CreateServiceRequest.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_CreateServiceRequest.Output> output = NF_CreateServiceRequest.createServiceRequest(new List<NF_CreateServiceRequest.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testErrorResponse() {
        NF_CreateServiceRequestResponse response = (NF_CreateServiceRequestResponse)JSON.deserialize(ERROR_RESPONSE, NF_CreateServiceRequestResponse.class);

        System.assertEquals(true, response.isError());
        System.assertEquals('\r\n[999] customerUniqueTransactionID provided exceeds the maximum allowed 40 characters', response.getDescription());
        System.assertEquals(400, response.getStatusCode());
    }

    private static String SUCCESS_RESPONSE = '{"createSRResponse":{"status":"Success","statusCode":"200","message":"Successfully processed the request","responseDateTime":"2019-10-30T18:19:24.680Z","serviceRequestNumber":null,"customerCaseNumber":"12345678","customerUniqueTransactionID":"3e7liajj3o0v495cib6vnuz7tukwqwozsmuph6x4"}}';
    private static String ERROR_RESPONSE = '{"createSRResponse":{"status":"Error","statusCode":"400","message":"Error in processing the request","responseDateTime":"2019-11-27T16:51:11.658Z","serviceRequestNumber":null,"customerCaseNumber":"CRD-OpenClose-103020191119","customerUniqueTransactionID":"3e7liajj3oasdasdadsduz7tukwqwaomuph12abc2","fault":[{"errorClass":"Validation","errorType":"Error","errorCode":"999","errorMessage":"customerUniqueTransactionID provided exceeds the maximum allowed 40 characters"}]}}';
    private static String UNAUTHORIZED_RESPONSE = '{ "error": "Key not authorised" }';

    public class SuccessResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, SUCCESS_RESPONSE); }
    }

    public class UnauthorizedResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(403, UNAUTHORIZED_RESPONSE); }
    }

    public class ErrorResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(500, ERROR_RESPONSE); }
    }
}