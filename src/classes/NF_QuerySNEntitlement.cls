/**
 * Created by mcontractlla on 2019-11-17.
 */

public with sharing class NF_QuerySNEntitlement {
    public static String ENDPOINT = 'callout:Juniper_API_Gateway/v01/querysnentitlement';

    public class Input{
        @InvocableVariable(required=true)
        public String chatKey;

        @InvocableVariable(required=false)
        public String email;

        @InvocableVariable(required=true)
        public String serialNumber;

        @InvocableVariable(required=false)
        public String contactId;
    }
    public class Output{
        @InvocableVariable
        public String response;

        @InvocableVariable
        public Boolean activeServiceContract;

        @InvocableVariable
        public String serialType;

        @InvocableVariable
        public String failReason;

        @invocableVariable
        public Integer returnCode;
        
        @InvocableVariable
        public String carouselPayload;

    }

    @InvocableMethod(label='Neuraflash - Query SNEntitlement')
    public static List<Output> querySNEntitlement(List<Input> input) {
        Integer returnCode = nf_ApiController.API_RESULT.FAILURE;

        List<Output> output = new List<Output>();
        Output out = new Output();
        out.activeServiceContract = false;
        out.failReason = 'generic';
        out.serialType = 'number';

        Boolean isServiceContractActive = false;
        Boolean isSoftwareSubscription = false;
        Integer serviceIndex = 0; 
        Integer softwareIndex = 0;

        try {
            String chatkey = input[0].chatKey;
            String email   = input[0].email;
            String serialNumber = input[0].serialNumber.toUpperCase();
            String contactId = input[0].contactId;

            NF_ApiController apiController = new NF_ApiController();
            apiController.initialize(chatKey);

            System.debug('>>NF_QuerySNEntitlement.querySNEntitlement making request>>');
            NF_QuerySNEntitlementRequest apiRequest = new NF_QuerySNEntitlementRequest(chatKey);
            System.debug('>>NF_QuerySNEntitlement.querySNENtitlement request initialized');
            apiRequest.getSNEntitlementDetailsRequest.product.serialNumber = serialNumber;
            apiRequest.getSNEntitlementDetailsRequest.product.ssrnFlag = '';
            apiRequest.getSNEntitlementDetailsRequest.contractInformation.salesOrderNumber = '';
            apiRequest.getSNEntitlementDetailsRequest.contractInformation.contractId = '';

            apiRequest.setEmail(email);
            apiRequest.setContactId(contactId);


            System.debug('>>NF_QuerySNEntitlement apiRequest=' + apiRequest);

            NF_QuerySNEntitlementResponse apiResponse = (NF_QuerySNEntitlementResponse) apiController.sendApiRequest(apiRequest, NF_QuerySNEntitlementResponse.class, true);
            System.debug('>>NF_QuerySNEntitlement response=' + apiResponse);

            if (apiResponse != null) {
                System.debug('Respinse>>>>'+apiResponse.isError());
                if (apiResponse.isError() == false) {
                    if (apiResponse.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo.size() > 0){
                        System.debug('Entered in first if ');
                        for(integer i = 0; i<apiResponse.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo.size(); i++) {
                            System.debug('Entered in second for index>>>'+i);
                            if(apiResponse.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[i].entitlementType == 'Service') {
                                serviceIndex = i;
                                if(apiResponse.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[i].serviceContractStatus == 'Active') {
                                    out.activeServiceContract = true;
                                    isServiceContractActive = true;
                                }
                            }
                            if(apiResponse.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[i].entitlementType == 'Subscription') {
                                softwareIndex = i;
                                isSoftwareSubscription = true;
                                system.debug('In if softwareIndex>>'+softwareIndex+'isSoftwareSubscription>>>'+isSoftwareSubscription);
                            }
                        }
                        /*if(apiResponse.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].serviceContractStatus == 'Active'){
                            out.activeServiceContract = true;
                            
                            //added by Manishi
                            isServiceContractActive = true;
                        }*/
                        out.serialType = (apiResponse.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].ssrn == 'N/A') ? 'SN' : 'SSRN';
                        System.debug('serviceIndex>>'+serviceIndex+'softwareIndex>>'+softwareIndex+'isSoftwareSubscription>>>'+isSoftwareSubscription);
                        //added by Manishi for dynamic carousel
                        String finalPayload = NF_DynamicCarouselContentFactory.createNF_ProductDetailCarousel(apiResponse,serialNumber,isServiceContractActive,isSoftwareSubscription, serviceIndex, softwareIndex);
                        //List<String> payloadList = new List<String>{finalPayload};
                        out.carouselPayload = NF_DynamicCarouselContentFactory.createNF_ProductDetailCarousel(apiResponse,serialNumber,isServiceContractActive,isSoftwareSubscription, serviceIndex, softwareIndex);
                        //out.carouselPayloadList = payloadList;
                        ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatkey);
                        if(session != null){
                            session.CarouselContent__c = finalPayload;
                            update session;
                            System.debug('Updated field>>>'+session.CarouselContent__c);
                        }
                    }
                    out.failReason = 'success';
                    System.debug('Before serialize>>'+apiResponse);
                    if(apiResponse.getSNEntitlementDetailsResponse != null) {
                        out.response = JSON.serialize(apiResponse.getSNEntitlementDetailsResponse);
                    }
                    
                    System.debug('After serialize');
                    System.debug('>>NF_QuerySNEntitlement out.response=' + out.response);
                    returnCode = nf_ApiController.API_RESULT.SUCCESS;
                } else{
                    System.debug(apiResponse.getErrorCode());
                    if(apiResponse.getErrorCode() == 815){
                        out.failReason = 'DNE';
                    }else if(apiResponse.getErrorCode() == 814){
                        out.failReason = 'IP';
                    }
                    System.debug('NF_QuerySNEntitlement>>> Entered in error code else');
                    returnCode = nf_ApiController.API_RESULT.FAILURE;
                    System.debug('Return code>>>'+returnCode);
                }    
            }
            else{
                System.debug('NF_QuerySNEntitlement>>> Entered in else');
                returnCode = nf_ApiController.API_RESULT.FAILURE;
            }
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
            out.failReason = 'generic';
            returnCode = nf_ApiController.API_RESULT.FAILURE;
        }
        System.debug('Final Return code>>>'+returnCode);
        out.returnCode = returnCode;

        output.add(out);
        return output;
    }
}