/**
 * Created by NeuraFlash LLC on 11/11/19.
 */

@IsTest
private class NF_UpdateSessionFollowupTest {
    @IsTest
    static void test1() {
        String chatKey = '123';

        Test.startTest();

        NF_UtilTest.insertBotSession(chatKey);
        
        NF_UpdateSessionFollowup.Input input = new NF_UpdateSessionFollowup.Input();
        input.chatKey = chatKey;
        input.isPositiveFeedback = 'true';
        NF_UpdateSessionFollowup.updateSession(new List<NF_UpdateSessionFollowup.Input>{input});

        Test.stopTest();

        ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatKey);

        System.assertEquals('Yes', session.Visitor_Feedback_Result__c);
    }
}