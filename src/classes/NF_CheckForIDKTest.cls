/**
 * Created by joaquintalon on 12/10/19.
 */

@IsTest
private class NF_CheckForIDKTest {
    @IsTest
    static void test1() {
        String output = NF_CheckForIDK.checkForIdk(new List<String>{'Not recognized'})[0];
        System.assert(output == 'Not recognized');
    }

    @IsTest
    static void test2() {
        String output = NF_CheckForIDK.checkForIdk(new List<String>{'I can\'t find it'})[0];
        System.assert(output == 'I don\'t know');
    }
}