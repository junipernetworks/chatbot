/**
 * @File Name          : NF_ValidateRMANumberTest.cls
 * @Description        : This class is the test class for NF_ValidateRMANumber.cls
 * @Author             : Manishi Kalra
 * @Created By         : NeuraFlash, LLC
**/

@isTest
public with sharing class NF_ValidateRMANumberTest {

    static testMethod void testValid() {
        NF_ValidateRMANumber.Input input = new NF_ValidateRMANumber.Input();
        input.rmaNumber = 'R123123123';
        List<NF_ValidateRMANumber.Input> inputs = new List<NF_ValidateRMANumber.Input>{ input };

        NF_ValidateRMANumber.Output output = NF_ValidateRMANumber.validateRMANumber(inputs)[0];
        System.assertEquals(true, output.isValid);
    }

    static testMethod void testNlp() {
        NF_ValidateRMANumber.Input input = new NF_ValidateRMANumber.Input();
        String rmaNumber = 'R123123123';
        input.rmaNumber = 'my RMA number is '+rmaNumber;
        List<NF_ValidateRMANumber.Input> inputs = new List<NF_ValidateRMANumber.Input>{ input };

        NF_ValidateRMANumber.Output output = NF_ValidateRMANumber.validateRMANumber(inputs)[0];
        System.assertEquals(true, output.isValid);
        System.assertEquals(rmaNumber, output.rmaNumber);
    }

    static testMethod void testInvalid() {
        NF_ValidateRMANumber.Input input = new NF_ValidateRMANumber.Input();
        input.rmaNumber = 'abc123123';
        List<NF_ValidateRMANumber.Input> inputs = new List<NF_ValidateRMANumber.Input>{ input };

        NF_ValidateRMANumber.Output output = NF_ValidateRMANumber.validateRMANumber(inputs)[0];
        System.assertEquals(false, output.isValid);
    }
}