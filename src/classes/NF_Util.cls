/**
 * Created by NeuraFlash LLC on 10/22/19.
 */

public with sharing class NF_Util {

    @TestVisible
    private static String padDTElement(String ele, Integer len){
        if(ele == null)
            ele = '';
        while(ele.length() < len){
            ele = '0' + ele;
        }
        return ele;
    }

    public static String getDateTime(){
        Datetime dt = Datetime.now();
        String monthString = padDTElement(String.valueOf(dt.month()), 2);
        String dayString = padDTElement(String.valueOf(dt.day()), 2);
        String hourString = padDTElement(String.valueOf(dt.hour()), 2);
        String minuteString = padDTElement(String.valueOf(dt.minute()), 2);
        String secondString = padDTElement(String.valueOf(dt.second()), 2);
        String msString = padDTElement(String.valueOf(dt.millisecond()), 3);
        String retString = dt.year() + '-' + monthString + '-' + dayString + 'T' + hourString + ':' + minuteString + ':' + secondString + '.' + msString + 'Z';
        System.debug('Using DateTime String: ' + retString);
        return retString;
    }

    public static String generateTransactionId(String chatKey){
        ebotdata__Bot_Session__c session = getBotSession(chatKey);
        if(session == null){
            System.debug('>>NF_Util.generateTransactionId cannot find bot session, generating own guid');
            Blob key = Crypto.generateAesKey(256);
            String h = EncodingUtil.convertToHex(key);
            String guid = h.substring(0, 40);
            return guid;
        }
        Integer apiRequestIndex = Integer.valueOf(session.API_Request_Index__c);
        String indexTag = padDTElement(String.valueOf(apiRequestIndex), 2);
        String guid = chatKey + '-' + indexTag;
        System.debug('NF_Util.generateTransactionId: using transactionId=[' + guid + ']');

        if(guid.length() > 40){
            guid = guid.substring(0, 40);
        }

        return guid;
    }

    @Future
    public static void callFuture(){

    }

    public static Decimal incrementApiRequestIndex(String chatKey) {
        ebotdata__Bot_Session__c session = getBotSession(chatKey);
        if (session != null) {
            session.API_Request_Index__c++;

            update session;

            return session.API_Request_Index__c;
        }
        
        return 1;
    }

    public static String getRecordTypeId(String label){
        RecordType[] rt = [SELECT Id
                           FROM RecordType
                           WHERE Name = :label];

        return rt.size() > 0 ? rt[0].Id : null;
    }

    public static LiveChatTranscript getLiveChatTranscript(String chatKey){
        System.debug('>>NF_Util.getLiveChatTranscript chatKey is ' + chatKey);

        if(String.isEmpty(chatKey)){
            System.debug('>>NF_Util.getLiveChatTranscript Cannot find LiveChatTranscript, chatKey is empty');
            return null;
        }

        List<LiveChatTranscript> listTranscripts = new List<LiveChatTranscript>();

        try {
            listTranscripts = [
                    SELECT Id, C2CEmail__c, C2CFirstName__c, C2CLastName__c, CIDorSON__c, Page_URL__c, Salted_ID__c
                    FROM LiveChatTranscript
                    WHERE ChatKey = :chatKey
            ];
        }
        catch(Exception e){
            System.debug('Exception encountered: ' + e);
        }

        return listTranscripts.size() > 0 ? listTranscripts[0] : null;
    }

    public static ebotdata__Bot_Session__c getBotSession(String chatKey){
        ebotdata__Bot_Session__c[] botSessions = [SELECT Id, API_Request_Index__c, Visitor_Feedback_Intent__c, Visitor_Feedback_Result__c, API_Errors__c, ebotdata__BotName__c, Comments__c,
                                                    IsPlatformToRoleSuccess__c, IsAssignRoleSuccess__c, IsEncryptionAgreementSigned__c, 
                                                    Platform_To_Role_Api_ReturnCode__c, Assign_Role_Api_ReturnCode__c
                                                  FROM ebotdata__Bot_Session__c
                                                  WHERE ebotdata__Live_Agent_Session_Id__c = :chatKey
                                                  LIMIT 1];

        return botSessions.size() > 0 ? botSessions[0] : null;
    }
    public static String getLastBusinessIntent(String chatKey){
        String lastBusinessIntent = '';
        try {
            ebotdata__Bot_Session__c botSession = NF_Util.getBotSession(chatKey);
            System.debug('>>NF_Util.getFirstUserInput botSession=' + botSession);

            if (botSession != null) {
                for (ebotdata__Bot_Chat_Log__c chatLog : [
                        SELECT Id, ebotdata__Intent_Name__c
                        FROM ebotdata__Bot_Chat_Log__c
                        WHERE ebotdata__Bot_Session__c = :botSession.Id
                        ORDER BY Name ASC
                        LIMIT 10
                ]) {

                    System.debug('>>NF_Util.getLastBusinessIntent: ebotdata__Intent_Name__c=' + chatLog.ebotdata__Intent_Name__c);
                    if (String.isNotEmpty(chatLog.ebotdata__Intent_Name__c) && chatLog.ebotdata__Intent_Name__c.substring(0, 3) == 'bus') {
                        lastBusinessIntent = chatLog.ebotdata__Intent_Name__c;
                        break;
                    }
                }
            }
        }catch(Exception e){
            System.debug('Exception encountered: ' + e);
        }
        return lastBusinessIntent;
    }
    public static String getFirstUserInput(String chatKey){
        String firstUserInput = '';
        try {
            ebotdata__Bot_Session__c botSession = NF_Util.getBotSession(chatKey);
            System.debug('>>NF_Util.getFirstUserInput botSession=' + botSession);

            if (botSession != null) {
                for (ebotdata__Bot_Chat_Log__c chatLog : [
                        SELECT Id, ebotdata__Current_Utterance__c
                        FROM ebotdata__Bot_Chat_Log__c
                        WHERE ebotdata__Bot_Session__c = :botSession.Id
                        ORDER BY Name ASC
                        LIMIT 10
                ]) {

                    System.debug('>>NF_Util chatLog.getFirstUserInput: ebotdata__Current_Utterance__c=' + chatLog.ebotdata__Current_Utterance__c);
                    if (String.isNotEmpty(chatLog.ebotdata__Current_Utterance__c)) {
                        firstUserInput = chatLog.ebotdata__Current_Utterance__c;
                        break;
                    }
                }
            }
        }catch(Exception e){
            System.debug('Exception encountered: ' + e);
        }
        firstUserInput = firstUserInput.length() > 255 ? firstUserInput.substring(0, 255) : firstUserInput;

        return firstUserInput;
    }

    public static Group getGroup(String label){
        Group[] groups = [SELECT Id
                          FROM Group
                          WHERE DeveloperName = :label
                          LIMIT 1];

        return groups.size() > 0 ? groups[0] : null;
    }

    public static RecordType getCcareRecordType(){
        RecordType[] recordTypes = [SELECT Id, Name
                                    FROM RecordType
                                    WHERE sObjectType = 'Case' AND name = 'CCARE Chat Case'
                                    LIMIT 1];

        return recordTypes.size() > 0 ? recordTypes[0] : null;
    }

    public static User getPlatformIntegrationUser(){
        User[] users = [SELECT Id, Name
                        FROM User
                        WHERE Name = 'Platform Integration User'
                        LIMIT 1];

        return users.size() > 0 ? users[0] : null;
    }

    public static ebotdata__Bot_Chat_Log__c[] getBusinessChatLogs(String chatKey){
        return [SELECT ebotdata__Current_Dialog_Name__c, ebotdata__Intent_Name__c
                FROM ebotdata__Bot_Chat_Log__c
                WHERE ebotdata__Live_Agent_Session_Id__c = :chatKey AND ebotdata__Intent_Name__c LIKE 'bus_%'
                ORDER BY CreatedDate ASC];
    }

    /*
     * Helper function to make our 'unique' ID more unique.
     * Pass in a single character string and this will get appended to the unique ID
     * This is helpful when we have multiple API calls within a transaction (Software Downloads)
     */
    public static String updateUniqueId(String guid, String uniqueValue){
        Integer MAX_LENGTH = 40;

        if(String.isBlank(guid)){
            return '';
        }

        if(String.isBlank(uniqueValue) || guid.length() == MAX_LENGTH){
            return guid;
        }

        guid = guid + uniqueValue.substring(0, 1);

        if(guid.length() > MAX_LENGTH){
            guid = guid.substring(0, MAX_LENGTH);
        }

        return guid;
    }
}