/**
 * Created by mcasella on 2019-11-06.
 */

@isTest
public with sharing class NF_EinsteinBotEventTriggerHandlerTest {

    static testMethod void updateLiveChatTranscript() {
        String chatKey = 'asdf';
        Test.startTest();
        LiveChatTranscript transcript = NF_UtilTest.insertLiveChatTranscript(chatKey, 'https://www.neuraflash.com', null);

        //TODO: update event
        ebotdata__Bot_Session__c session = NF_UtilTest.insertBotSession(chatKey);
        //NF_EinsteinBotEventTriggerHandler.addApiError(chatKey, 'error');
        //NF_EinsteinBotEventTriggerHandler.updateSession(chatKey);
        Test.stopTest();
    }

    static testMethod void updateBotName() {
        String chatKey = 'asdf';
        String botName = 'botty';
        Test.startTest();
        LiveChatTranscript transcript = NF_UtilTest.insertLiveChatTranscript(chatKey, 'https://www.neuraflash.com', null);

        //TODO: update event
        ebotdata__Bot_Session__c session = NF_UtilTest.insertBotSession(chatKey);
        NF_EinsteinBotEventTriggerHandler.updateBotSessionFields(chatKey, botName);
        //NF_EinsteinBotEventTriggerHandler.updatePageURL(chatKey);
        //NF_EinsteinBotEventTriggerHandler.updateSession(chatKey);
        Test.stopTest();
    }

    static testMethod void insertBotEvent(){
        Test.startTest();
        ebotdata__Bot_Event__e evt = new ebotdata__Bot_Event__e(ebotdata__Type__c = NF_EinsteinBotEventTriggerHandler.EVENT_UPDATE_BOT_SESSION, ebotdata__Live_Agent_Session_Id__c  = 'abc');
        EventBus.publish(evt);
        Test.stopTest();
    }
}