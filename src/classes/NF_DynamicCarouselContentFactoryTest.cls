/**
 * Created by mcasella on 1/17/20.
 */

@isTest
public with sharing class NF_DynamicCarouselContentFactoryTest {

    @IsTest
    static void testQueryApiResponse() {
        NF_QueryRMAResponse apiResponse = new NF_QueryRMAResponse();
        //apiResponse.QueryRMAResponse.rmaInformation
        apiResponse.QueryRMAResponse = new NF_QueryRMAResponse.QueryRMAResponse();
        apiResponse.QueryRMAResponse.rmaInformation = new NF_QueryRMAResponse.RMAInfo[1];
        apiResponse.QueryRMAResponse.rmaInformation[0] = new NF_QueryRMAResponse.RMAInfo();
        apiResponse.QueryRMAResponse.rmaInformation[0].defectiveItems = new NF_QueryRMAResponse.DefectiveItem[2];
        apiResponse.QueryRMAResponse.rmaInformation[0].defectiveItems[0] = new NF_QueryRMAResponse.DefectiveItem();
        apiResponse.QueryRMAResponse.rmaInformation[0].defectiveItems[0].defectiveItemStatus = 'RMA Cancelled';
        apiResponse.QueryRMAResponse.rmaInformation[0].defectiveItems[0].itemNumber = '123';
        apiResponse.QueryRMAResponse.rmaInformation[0].defectiveItems[1] = new NF_QueryRMAResponse.DefectiveItem();
        apiResponse.QueryRMAResponse.rmaInformation[0].defectiveItems[1].receivedDate = '1234567890123';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact = new NF_QueryRMAResponse.RMAContact();
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.address = new NF_QueryRMAResponse.Address();
        apiResponse.QueryRMAResponse.rmaInformation[0].replacementItems = new NF_QueryRMAResponse.ReplacementItem[2];
        apiResponse.QueryRMAResponse.rmaInformation[0].replacementItems[0] = new NF_QueryRMAResponse.ReplacementItem();
        apiResponse.QueryRMAResponse.rmaInformation[0].replacementItems[1] = new NF_QueryRMAResponse.ReplacementItem();

        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.contactEmail = '';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.address = new NF_QueryRMAResponse.Address();
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.address.address1 = '';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.address.address2 = '';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.address.city = '';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.address.country = '';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.address.countryCode = '';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.address.postalCode = '';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.address.state = '';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.address.stateCode = '';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.companyName = '';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.contactName = '';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.telephoneCountryCode = '';
        apiResponse.QueryRMAResponse.rmaInformation[0].rmaContact.telephoneNumber = '';

        NF_DynamicCarouselContentFactory.createNF_QueryRMACarousel(apiResponse, '');
    }

    @IsTest
    static void testQueryRMAListApiResponse() {
        NF_QuerySR_RMAListResponse apiResponse = new NF_QuerySR_RMAListResponse();
        apiResponse.querySRRMAListDetailsResponse = new NF_QuerySR_RMAListResponse.QuerySRRMAListDetailsResponse();
        apiResponse.querySRRMAListDetailsResponse.rmaDetails = new NF_QuerySR_RMAListResponse.RMADetails[1];
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0] = new NF_QuerySR_RMAListResponse.RMADetails();

        apiResponse.querySRRMAListDetailsResponse.customerCaseNumber = '';
        apiResponse.querySRRMAListDetailsResponse.customerSourceID = '';
        apiResponse.querySRRMAListDetailsResponse.customerUniqueTransactionID = '';
        apiResponse.querySRRMAListDetailsResponse.responseDateTime = '';
        apiResponse.querySRRMAListDetailsResponse.serviceRequestNumber = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails = new List<NF_QuerySR_RMAListResponse.RMADetails>{new NF_QuerySR_RMAListResponse.RMADetails()};
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaNumber = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaHeaderStatus = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaHeaderCreatedDate = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact = new NF_QuerySR_RMAListResponse.RMAContact();
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.companyName = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.contactName = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.telephoneCountryCode = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.telephoneNumber = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address = new NF_QuerySR_RMAListResponse.Address();
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.address1 = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.address2 = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.city = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.stateCode = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.state = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.countryCode = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.country = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].rmaContact.address.postalCode = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems = new NF_QuerySR_RMAListResponse.DefectiveItem[1];
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0] = new NF_QuerySR_RMAListResponse.DefectiveItem();
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].itemNumber = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].dateTime_c = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].productID = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].serialNumber = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].trackingNumber = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].trackingNumberUrl = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].carrierDescription = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].receivedDate = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].defectiveItemStatus = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].defectiveRmaType = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].defectiveItems[0].defectiveRmaSubType = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].ceItems = new NF_QuerySR_RMAListResponse.CEItem[1];
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0] = new NF_QuerySR_RMAListResponse.CEItem();
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].defectiveItemNumber = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].productID = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].engineerStatus = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].vendorName = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].vendorPhone = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].serviceRequestedDate = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].actualServicedDate = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].contactedDate = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].ceItems[0].estimatedArrivalDate = '';

        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems = new NF_QuerySR_RMAListResponse.ReplacementItem[1];
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0] = new NF_QuerySR_RMAListResponse.ReplacementItem();
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].itemNumber = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].defectiveItemNumber = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].productID = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].serialNumber = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].carrierDescription = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].shipDate = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].shipmentServiceLevel = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].trackingNumber = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].replacementStatus = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].receivedDate = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].receivedBy = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].replacementRmaType = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].replacementRmaSubType = '';
        apiResponse.querySRRMAListDetailsResponse.rmaDetails[0].replacementItems[0].trackingNumberUrl = '';

        apiResponse.querySRRMAListDetailsResponse.srDetails = new List<NF_QuerySR_RMAListResponse.SRDetails>{new NF_QuerySR_RMAListResponse.SRDetails()};
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].serviceRequestNumber = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].customerCaseNumber = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].synopsis = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].problemDescription = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].srStatus = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].priority = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].adminSRType = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].adminSRSubType = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].ccEmail = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].addSenderToEmail = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].followUpMethod = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].srOwnerFullName = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].srOwnerEmailAddress = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].caseType = new NF_QuerySR_RMAListResponse.CaseType();
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].escalate = new List<NF_QuerySR_RMAListResponse.Escalate>{new NF_QuerySR_RMAListResponse.Escalate()};
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].escalate[0].description = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].escalate[0].escalatedBy = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].escalate[0].status = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].attachments = new List<NF_QuerySR_RMAListResponse.Attachment>{new NF_QuerySR_RMAListResponse.Attachment()};
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].attachments[0].sequenceNumber = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].attachments[0].title = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].attachments[0].sizeInBytes = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].attachments[0].path = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].attachments[0].typeDescription = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].attachments[0].uploadedDateTime = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].attachments[0].uploadedBy = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].product = new NF_QuerySR_RMAListResponse.Product();
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].product.serialNumber = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].product.productID = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].product.productSeries = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].product.platform = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].product.version = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].product.release = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].product.software = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].product.specialRelease = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].product.routerName = '';


        apiResponse.querySRRMAListDetailsResponse.srDetails[0].contact = new NF_QuerySR_RMAListResponse.Contact();
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].contact.accountID = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].contact.accountName = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].contact.contactName = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].contact.contactEmail = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].contact.preferredTelephoneCountryCode = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].contact.preferredTelephoneNumber = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].contact.preferredTelephoneExtension = '';

        apiResponse.querySRRMAListDetailsResponse.srDetails[0].rma = new NF_QuerySR_RMAListResponse.RMA[1];
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].rma[0] = new NF_QuerySR_RMAListResponse.RMA();
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].rma[0].rmaNumber = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].rma[0].itemNumber = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].rma[0].itemType = '';
        apiResponse.querySRRMAListDetailsResponse.srDetails[0].rma[0].itemStatus = '';

        NF_DynamicCarouselContentFactory.createNF_QueryRMAListCarousel(apiResponse);
    }

    @IsTest
    static void blankCardTest(){
        String jsonString = '{"querySRResponse":{"customerCaseNumber":null,"customerSourceID":"juni-chatbot","customerUniqueTransactionID":"4c0a141c-29bc-4828-a34c-7683c4af3390-002","serviceRequestNumber":"2019-0206-T-0910","responseDateTime":"2020-02-05T15:22:25.242Z","statusCode":"200","status":"Success","message":"Successfully processed the request","caseType":{"caseTypeCode":"TEC","caseTypeDescription":"Technical Service Request"},"synopsis":"CNCQOH331CW RE1 not functioning properly","srStatus":"Closed","priority":"P2 - High","adminSRType":null,"adminSRSubType":null,"ccEmail":null,"addSenderToEmail":true,"followUpMethod":null,"srOwnerFullName":"Rahul Unnikrishnan","srOwnerEmailAddress":"RAHULUK@JUNIPER.NET","caseLastChangedTime":"2019-04-12T17:56:41.000Z","contact":{"accountID":"0100036471","accountName":"CHARTER COMMERCIAL - MIDW","contactId":"0201145424","contactName":"MICHAEL BOND","contactEmail":"MICHAEL.BOND@CHARTER.COM","preferredTelephoneCountryCode":null,"preferredTelephoneNumber":null,"preferredTelephoneExtension":null},"rma":[{"rmaNumber":"R200226844","items":[{"itemNumber":"500","itemType":"RMA Defective","itemStatus":"Request Satisfied - Closed"},{"itemNumber":"410","itemType":"RMA Replacement","itemStatus":"RMA Delivered - Closed"},{"itemNumber":"510","itemType":"RMA Replacement","itemStatus":"RMA Delivered - Closed"},{"itemNumber":"300","itemType":"RMA Defective","itemStatus":"Request Satisfied - Closed"},{"itemNumber":"310","itemType":"RMA Replacement","itemStatus":"RMA Delivered - Closed"},{"itemNumber":"200","itemType":"RMA Defective","itemStatus":"Request Satisfied - Closed"},{"itemNumber":"400","itemType":"RMA Defective","itemStatus":"Request Satisfied - Closed"},{"itemNumber":"110","itemType":"RMA Replacement","itemStatus":"RMA Delivered - Closed"},{"itemNumber":"210","itemType":"RMA Replacement","itemStatus":"RMA Delivered - Closed"},{"itemNumber":"100","itemType":"RMA Defective","itemStatus":"Request Satisfied - Closed"}]}],"product":{"serialNumber":"JN1156AF4AFA","productID":"MX960BASE-DC","productSeries":"MX-Series","platform":"MX960","version":"R6-S3.2","release":"16.1","software":null,"specialRelease":null,"routerName":"CNCQOH331CW"},"attachments":[{"sequenceNumber":"0007413815","title":"logs-And-rsi.tgz","sizeInBytes":"20073","path":"S3Upload:archive/attachments/PCLR/2019/02/06/0910/20190206225129074","typeDescription":null,"uploadedDateTime":"2019-02-07T06:52:14.000Z","uploadedBy":"MICHAEL BOND"}]}}';
        NF_QuerySRResponse apiResponse = (NF_QuerySRResponse) JSON.deserialize(jsonString, NF_QuerySRResponse.class);

        String expectedPayload = '{"home":1,"cards":["<div class = \'nf-carousel\'><h1>Case 2019-0206-T-0910</h1><h2>Details</h2><div class=\'slds-text-align_left slds-m-left_small\'><p><span class =\'nf-capping\'><b>Synopsis: </b>CNCQOH331CW RE1 not funct...<br><b>Serial #: </b>JN1156AF4AFA<br><b>Priority: </b>P2 - High<br><b>Status: </b>Closed<br></div></div><div><a href=\'https://casemanager.juniper.net/casemanager/#/cmdetails/2019-0206-T-0910\' target=\'_blank\' class =\'slds-button slds-button_neutral nf-carousel-detail-button\' >View full case details</a></div><div class = \'nf-rectangle\'></div>","<div class = \'nf-carousel\'><h1>Case 2019-0206-T-0910</h1><h2>Attached Files</h2><div class=\'slds-text-align_left slds-m-left_small\'><p><span class =\'nf-capping\'>logs-And-rsi.tgz<br>(20073MB)<br><br></div></div><div><a href=\'https://casemanager.juniper.net/casemanager/#/cmdetails/2019-0206-T-0910\' target=\'_blank\' class =\'slds-button slds-button_neutral nf-carousel-detail-button\' >View full case details</a></div><div class = \'nf-rectangle\'></div>","<div class = \'nf-carousel\'><h1>Case 2019-0206-T-0910</h1><h2>Related Items</h2><div class=\'slds-text-align_left slds-m-left_small\'><span class =\'nf-capping\'><b>RMA: </b> <a href=\'https://casemanager.juniper.net/casemanager/#/rmadetails/R200226844\' target=\'_blank\'>R200226844</a></span><br></div></div><div><a href=\'https://casemanager.juniper.net/casemanager/#/cmdetails/2019-0206-T-0910\' target=\'_blank\' class =\'slds-button slds-button_neutral nf-carousel-detail-button\' >View full case details</a></div><div class = \'nf-rectangle\'></div>"]}';

        String payload = NF_DynamicCarouselContentFactory.createNF_QuerySRCarousel(apiResponse);

        System.assertEquals(expectedPayload, payload);

        System.debug('payload='+payload);
    }
    
    @IsTest
    static void viewMoreAttachmentTest(){
        String jsonString = '{ "querySRResponse" : { "customerCaseNumber" : null, "customerSourceID" : "juni-chatbot", "customerUniqueTransactionID" : "12356", "serviceRequestNumber" : "2019-0130-T-0194", "responseDateTime" : "2020-02-07T08:51:24.126Z", "statusCode" : "200", "status" : "Success", "message" : "Successfully processed the request", "caseType" : { "caseTypeCode" : "TEC", "caseTypeDescription" : "Technical Service Request" }, "synopsis" : "MPC3D restart on eqx-015", "srStatus" : "Closed", "priority" : "P3 - Medium", "adminSRType" : null, "adminSRSubType" : null, "ccEmail" : "ENGINEERING##@XXIP-PLUS.NETX,MORONI##@XXIP-PLUS.NETX", "addSenderToEmail" : true, "followUpMethod" : null, "srOwnerFullName" : "Costel Soare", "srOwnerEmailAddress" : "CSOARE@JUNIPER.NET", "caseLastChangedTime" : "2019-02-27T17:05:37.000Z", "contact" : { "accountID" : "0100523664", "accountName" : "SWISSCOM-IPPLUS", "contactId" : "0201080237", "contactName" : "PAOLO MORONI", "contactEmail" : "PAOLO.MORONI@SWISSCOM.COM", "preferredTelephoneCountryCode" : null, "preferredTelephoneNumber" : null, "preferredTelephoneExtension" : null }, "rma" : [ { "rmaNumber" : "R200222512", "items" : [ { "itemNumber" : "110", "itemType" : "RMA Replacement", "itemStatus" : "RMA Delivered - Closed" }, { "itemNumber" : "100", "itemType" : "RMA Defective", "itemStatus" : "Request Satisfied - Closed" } ] } ], "product" : { "serialNumber" : "JN125FD6FAFB", "productID" : "MX480BASE3-DC", "productSeries" : "MX-Series", "platform" : "MX480", "version" : "17.3", "release" : "17.3", "software" : null, "specialRelease" : null, "routerName" : "I00EQX-015" }, "attachments" : [ { "sequenceNumber" : "0007398194", "title" : "rsi-20190130.txt", "sizeInBytes" : "1130", "path" : "S3Upload:archive/attachments/PCLR/2019/01/30/0194/20190130044446167", "typeDescription" : null, "uploadedDateTime" : "2019-01-30T12:45:00.000Z", "uploadedBy" : "PAOLO MORONI" }, { "sequenceNumber" : "0007398200", "title" : "i00eqx-015_fpc2_syslog.log", "sizeInBytes" : "90", "path" : "S3Upload:archive/attachments/PCLR/2019/01/30/0194/20190130044612049", "typeDescription" : null, "uploadedDateTime" : "2019-01-30T12:46:24.000Z", "uploadedBy" : "PAOLO MORONI" }, { "sequenceNumber" : "0007398203", "title" : "i00eqx-015_fpc2_nvram.log", "sizeInBytes" : "32", "path" : "S3Upload:archive/attachments/PCLR/2019/01/30/0194/20190130044719358", "typeDescription" : null, "uploadedDateTime" : "2019-01-30T12:47:33.000Z", "uploadedBy" : "PAOLO MORONI" }, { "sequenceNumber" : "0007398204", "title" : "JCATS_Ext_rsi-20190130.html", "sizeInBytes" : "172", "path" : "S3Upload:archive/attachments/PCLR/2019/01/30/0194/005056A305EF1ED98991AA86AFF840EB", "typeDescription" : null, "uploadedDateTime" : "2019-01-30T12:48:23.000Z", "uploadedBy" : "JCATS User" }, { "sequenceNumber" : "0007398205", "title" : "sh-log-nvram-20190130.txt", "sizeInBytes" : "2", "path" : "S3Upload:archive/attachments/PCLR/2019/01/30/0194/20190130044809206", "typeDescription" : null, "uploadedDateTime" : "2019-01-30T12:48:23.000Z", "uploadedBy" : "PAOLO MORONI" }, { "sequenceNumber" : "0007398222", "title" : "core-NPC2.gz.core.0", "sizeInBytes" : "233865", "path" : "S3Upload:archive/attachments/PCLR/2019/01/30/0194/20190130045910309", "typeDescription" : null, "uploadedDateTime" : "2019-01-30T13:00:43.000Z", "uploadedBy" : "PAOLO MORONI" }, { "sequenceNumber" : "0007398251", "title" : "JCATS_Ext_core-NPC2.0.html", "sizeInBytes" : "16", "path" : "S3Upload:archive/attachments/PCLR/2019/01/30/0194/005056A352F01EE9899205A25DF7CA83", "typeDescription" : null, "uploadedDateTime" : "2019-01-30T13:08:46.000Z", "uploadedBy" : "JCATS User" }, { "sequenceNumber" : "0007398302", "title" : "JCATS_Ext_var-log-20190130.html", "sizeInBytes" : "9", "path" : "S3Upload:archive/attachments/PCLR/2019/01/30/0194/005056A35A711EE989923AABBFA1B49E", "typeDescription" : null, "uploadedDateTime" : "2019-01-30T13:20:37.000Z", "uploadedBy" : "JCATS User" } ] } }';
        NF_QuerySRResponse apiResponse = (NF_QuerySRResponse) JSON.deserialize(jsonString, NF_QuerySRResponse.class);
        String expectedPayload = '{"home":1,"cards":["<div class = \'nf-carousel\'><h1>Case 2019-0130-T-0194</h1><h2>Details</h2><div class=\'slds-text-align_left slds-m-left_small\'><p><span class =\'nf-capping\'><b>Synopsis: </b>MPC3D restart on eqx-015<br><b>Serial #: </b>JN125FD6FAFB<br><b>Priority: </b>P3 - Medium<br><b>Status: </b>Closed<br></div></div><div><a href=\'https://casemanager.juniper.net/casemanager/#/cmdetails/2019-0130-T-0194\' target=\'_blank\' class =\'slds-button slds-button_neutral nf-carousel-detail-button\' >View full case details</a></div><div class = \'nf-rectangle\'></div>","<div class = \'nf-carousel\'><h1>Case 2019-0130-T-0194</h1><h2>Attached Files</h2><div class=\'slds-text-align_left slds-m-left_small\'><p><span class =\'nf-capping\'>rsi-20190130.txt<br>(1130MB)<br>i00eqx-015_fpc2_syslog.lo...<br>(90MB)<br><br></div><a href=\'https://casemanager.juniper.net/casemanager/#/cmdetails/2019-0130-T-0194\' target=\'_blank\' class =\'slds-button slds-button_neutral nf-carousel-product-button nf-carousel-payload-button-viewmore\' >View more</a></div><div><a href=\'https://casemanager.juniper.net/casemanager/#/cmdetails/2019-0130-T-0194\' target=\'_blank\' class =\'slds-button slds-button_neutral nf-carousel-detail-button\' >View full case details</a></div><div class = \'nf-rectangle\'></div>","<div class = \'nf-carousel\'><h1>Case 2019-0130-T-0194</h1><h2>Related Items</h2><div class=\'slds-text-align_left slds-m-left_small\'><span class =\'nf-capping\'><b>RMA: </b> <a href=\'https://casemanager.juniper.net/casemanager/#/rmadetails/R200222512\' target=\'_blank\'>R200222512</a></span><br></div></div><div><a href=\'https://casemanager.juniper.net/casemanager/#/cmdetails/2019-0130-T-0194\' target=\'_blank\' class =\'slds-button slds-button_neutral nf-carousel-detail-button\' >View full case details</a></div><div class = \'nf-rectangle\'></div>"]}';
    	String payload = NF_DynamicCarouselContentFactory.createNF_QuerySRCarousel(apiResponse);

        System.assertEquals(expectedPayload, payload);

        System.debug('payload='+payload);
	}
}