/**
 * Created by mcasella on 2019-11-21.
 */

public with sharing class NF_GetUserProfileResponse extends NF_ApiResponse{
    public String id = '';
    public String status = '';
    public String errorMessage = '';
    public String errorCode = '';
    public String error = '';

    public Profile profile;

    public NF_GetUserProfileResponse(){
        profile = new Profile();
    }

    public String getContactId(){
        return profile.sapBPID;
    }

    public String getAccountId(){
        return profile.sapAcctID;
    }

    public class Profile{
        public String zipCode = '';

        //Contact ID to be used in Resend Welcome Letter
        public String sapBPID = '';

        //Account ID to be used in Create Service Request
        public String sapAcctID = '';
    }

    public override Boolean isError(){
        return String.isNotEmpty(error) || String.isNotEmpty(errorCode);
    }

    public override Integer getStatusCode(){
        return getErrorCode();
    }

    public override Integer getErrorCode(){
        return String.isNotEmpty(errorCode) ? Integer.valueOf(errorCode) : -1;
    }

    public override String getDescription(){
        return String.isNotEmpty(errorMessage) ? errorMessage : error;
    }
}

/*
Error
{
    "errorMessage": "Error occurred while fetching user profile. Please retry later.",
    "errorCode": "902"
}



Success
{
    "id": "00unysszlx4kcBExv0h7",
    "status": "ACTIVE",
    "created": "2019-10-19T10:49:45.000Z",
    "activated": "2019-10-20T20:05:11.000Z",
    "statusChanged": "2019-10-20T20:05:11.000Z",
    "lastLogin": null,
    "lastUpdated": "2019-10-20T20:05:11.000Z",
    "passwordChanged": "2019-10-19T10:49:46.000Z",
    "profile": {
        "zipCode": "77024",
        "siteURL": "",
        "awebRowID": "",
        "companyName": "IHS INC",
        "partnerLevel": "",
        "sieblContactID": "1-1NAK79Y",
        "employeeNumber": "",
        "division": "",
        "productSpecialization": "",
        "govComment": "LicenseFree-NoConcerns",
        "honorificSuffix": "",
        "theater": "NAM",
        "selfServiceAccess": "",
        "state": "TX",
        "buyingModel": "",
        "orsPilotUser": "",
        "distributors": "",
        "nickName": "dinh  pham ",
        "costCenter": "",
        "sapAcctID": "0100503848",
        "GUID": "8484-8153-8146-2161",
        "pwdExpFlag": "N",
        "supportLevel": "",
        "orsSiteUserID": "",
        "varID": "",
        "organizationID": "",
        "firstName": "dinh",
        "primaryPhone": "",
        "postalAddress": "8584 katy freeway ",
        "mobilePhone": "",
        "userType": "Customer",
        "partnerType": "Customer",
        "region": "",
        "profileUrl": "",
        "lastName": " pham ",
        "preferredLanguage": "",
        "userStatus": "APPROVED",
        "juniperRep": "",
        "city": "katy",
        "displayName": "dinh  pham ",
        "partnerCountryISO": "",
        "companyURL": "",
        "title": "",
        "locale": "",
        "wappTZ": "(GMT-06:00) Central Time (US & Canada)",
        "login": "dinh.pham@ihs.com",
        "primaryRole": "IT - Other",
        "prmFlag": "",
        "countryCode": "",
        "contactRole": "",
        "secondaryLevel": "",
        "department": "",
        "camEmail": "",
        "email": "dinh.pham@ihs.com",
        "authCode": "",
        "manager": "",
        "partnerName": "",
        "secondEmail": "",
        "honorificPrefix": "",
        "managerId": "",
        "isOPTEligibleCountry": "",
        "userJobFunction": "",
        "rolesEntitled": "Customer Service Agreement;Customer Admin;SW-EVAL;Elearning;Screen OS;Encryption;Case Manager Read/Write;",
        "streetAddress": "8584 katy freeway , ",
        "userSystemID": "",
        "organization": "",
        "primaryContact": "",
        "portalID": "",
        "sapBPID": "0200512827",
        "wappID": "",
        "middleName": "",
        "govReasonCode": "System",
        "pwdLastChangedWapp": "02/19/2014 12:17:04",
        "tcFlag": ""
    },
    "credentials": {
        "password": {},
        "provider": {
            "type": "OKTA",
            "name": "OKTA"
        }
    },
    "_links": {
        "suspend": {
            "href": "https://iam-signin-stage.juniper.net/api/v1/users/00unysszlx4kcBExv0h7/lifecycle/suspend",
            "method": "POST"
        },
        "resetPassword": {
            "href": "https://iam-signin-stage.juniper.net/api/v1/users/00unysszlx4kcBExv0h7/lifecycle/reset_password",
            "method": "POST"
        },
        "forgotPassword": {
            "href": "https://iam-signin-stage.juniper.net/api/v1/users/00unysszlx4kcBExv0h7/credentials/forgot_password",
            "method": "POST"
        },
        "expirePassword": {
            "href": "https://iam-signin-stage.juniper.net/api/v1/users/00unysszlx4kcBExv0h7/lifecycle/expire_password",
            "method": "POST"
        },
        "changeRecoveryQuestion": {
            "href": "https://iam-signin-stage.juniper.net/api/v1/users/00unysszlx4kcBExv0h7/credentials/change_recovery_question",
            "method": "POST"
        },
        "self": {
            "href": "https://iam-signin-stage.juniper.net/api/v1/users/00unysszlx4kcBExv0h7"
        },
        "changePassword": {
            "href": "https://iam-signin-stage.juniper.net/api/v1/users/00unysszlx4kcBExv0h7/credentials/change_password",
            "method": "POST"
        },
        "deactivate": {
            "href": "https://iam-signin-stage.juniper.net/api/v1/users/00unysszlx4kcBExv0h7/lifecycle/deactivate",
            "method": "POST"
        }
    }
}*/