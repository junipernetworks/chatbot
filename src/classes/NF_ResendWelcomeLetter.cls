/**
 * Created by NeuraFlash LLC on 10/18/19.
 */
public with sharing class NF_ResendWelcomeLetter {
    public static String ENDPOINT = 'callout:Juniper_API_Gateway/v01/resendwelcomeletter';

    public class Input {
        @InvocableVariable(required=false)
        public String chatKey;

        @InvocableVariable(required=false)
        public String email;

        @InvocableVariable(required=false)
        public String contactId;

        @InvocableVariable(required=false)
        public String contractId;
    }
    public class Output {
        @InvocableVariable
        public String response;

        @invocableVariable
        public Integer returnCode;
    }

    @InvocableMethod(label='Neuraflash - Call Resend Welcome Letter')
    public static List<Output> resendWelcomeLetter(List<Input> input) {
        Integer returnCode = nf_ApiController.API_RESULT.FAILURE;

        List<Output> output = new List<Output>();
        Output out = new Output();

        try {
            String chatkey = input[0].chatKey;
            String email = input[0].email;
            String contractId = input[0].contractId;
            String contactId = input[0].contactId;

            NF_ApiController apiController = new NF_ApiController();
            apiController.initialize(chatKey);

            NF_ResendWelcomeLetterRequest apiRequest = new NF_ResendWelcomeLetterRequest(chatKey);
            apiRequest.setEmail(email);
            apiRequest.setContactId(contactId);
            apiRequest.setContractId(contractId);
            System.debug('>>NF_ResendWelcomeLetter apiRequest=' + apiRequest);

            NF_ResendWelcomeLetterResponse apiResponse = (NF_ResendWelcomeLetterResponse) apiController.sendApiRequest(apiRequest, NF_ResendWelcomeLetterResponse.class, true);
            System.debug('>>NF_ResendWelcomeLetter response=' + apiResponse);

            if (apiResponse != null) {
                out.response = JSON.serialize(apiResponse.resendWelcomeLetterResponse);
                System.debug('>>NF_CreateServiceRequest: apiResponse.getErrorCode()=' + apiResponse.getErrorCode());
                System.debug('>>NF_CreateServiceRequest: out.response=' + out.response);

                if(apiResponse.isNotFound()){
                    returnCode = NF_ApiController.API_RESULT.NOT_FOUND;
                }
                else if (apiResponse.isError() == false) {
                    returnCode = NF_ApiController.API_RESULT.SUCCESS;
                }
            }
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
        }

        out.returnCode = returnCode;

        output.add(out);
        return output;
    }

}