/**
 * Created by mcurry on 2019-11-20.
 */
@IsTest
public with sharing class NF_ValidatePreLeadInfoTest {
    @IsTest
    static void test1(){
        Test.startTest();

        NF_ValidatePreLeadInfo.Input input = new NF_ValidatePreLeadInfo.Input();
        input.FullNameString = 'Michael Curry';
        input.EmailString = 'michael.curry@neuraflash.com';

        List<NF_ValidatePreLeadInfo.Output> output = NF_ValidatePreLeadInfo.validateInputs(new List<NF_ValidatePreLeadInfo.Input>{input});

        System.assert(output.size() == 1);
        System.assert(output[0].isValid == true);

        NF_ValidatePreLeadInfo.Input input2 = new NF_ValidatePreLeadInfo.Input();
        input2.FullNameString = 'Blah';
        input2.EmailString = 'this is not great';

        List<NF_ValidatePreLeadInfo.Output> output2 = NF_ValidatePreLeadInfo.validateInputs(new List<NF_ValidatePreLeadInfo.Input>{input2});

        System.assert(output2.size() == 1);
        System.assert(output2[0].isValid == false);

        NF_ValidatePreLeadInfo.Input input3 = new NF_ValidatePreLeadInfo.Input();
        input3.FullNameString = 'Blah Blah';
        input3.EmailString = '';

        List<NF_ValidatePreLeadInfo.Output> output3 = NF_ValidatePreLeadInfo.validateInputs(new List<NF_ValidatePreLeadInfo.Input>{input3});

        System.assert(output3.size() == 1);
        System.assert(output3[0].isValid == false);

        Test.stopTest();
    }
}