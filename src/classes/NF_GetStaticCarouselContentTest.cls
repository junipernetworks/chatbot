/**
 * @File Name          : NF_GetStaticCarouselContentTest.cls
 * @Description        : This class is test class for NF_GetStaticCarouselContent.cls
 * @Author             : Manishi Kalra
 * @Created By         : NeuraFlash, LLC
**/
@isTest
private class NF_GetStaticCarouselContentTest {
    
    /* 
    * method name: testCarouselContent
    * description: test method that will test getStaticContent() method.
    */
	@IsTest
    static void testCarouselContent() {
        /* Create test input LiveChatTranscript */
        String chatKey = 'asdfasdf';
        LiveChatVisitor lcv = new LiveChatVisitor();
        insert lcv;
        LiveChatTranscript lct = new LiveChatTranscript();
        lct.LiveChatVisitorid = lcv.id;
        lct.ChatKey = chatKey;
        insert lct;
        
        /* Create test input wrapper */
        NF_GetStaticCarouselContent.InputWrapper input = new NF_GetStaticCarouselContent.InputWrapper();
        input.payload = 'This is test payload';
        input.routableID = lct.Id;
        List<NF_GetStaticCarouselContent.InputWrapper> inputList = new List<NF_GetStaticCarouselContent.InputWrapper>{input};
        
        Test.startTest();
        NF_GetStaticCarouselContent.getStaticContent(inputList);
        /*List<LiveChatTranscript> transcriptList = [SELECT ID, CarouselContent__c
                                                   FROM LiveChatTranscript 
                                                   ORDER BY CreatedDate DESC LIMIT 1];*/
        Test.stopTest();
        
        //System.assertEquals(transcriptList[0].CarouselContent__c, 'This is test payload');
        
    }
    
}