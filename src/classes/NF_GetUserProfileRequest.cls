/**
 * Created by mcasella on 2019-11-21.
 */

public with sharing class NF_GetUserProfileRequest extends NF_ApiRequest {
    public String email = '';

    public override String getApiName(){
        return 'Query User Profile';
    }

    public override String getMethod(){
        return 'GET';
    }

    public override String getEndpoint(){
        return 'callout:Juniper_API_Gateway/v01/queryuserprofile';
    }

    /* No Transaction Id for this request */
    public override String getTransactionId(){
        return '';
    }

    public override Map<String, String> getHeaders(){
        Map<String, String> headers = super.getHeaders();

        headers.put('Email', email);

        return headers;
    }

    public NF_GetUserProfileRequest(String email){
        this.email = email;
    }
}