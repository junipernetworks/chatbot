/**
 * Created by NeuraFlash LLC on 10/22/19.
 */

public with sharing class NF_SalesforceCaseCreation {
    static String OWNER = 'CCARE_PRI_CASE';
    static String STATUS = 'Closed';
    static String ORIGIN = 'Chatbot';
    static String TYPE = 'CCARE Chat Case';

    public class Input{
        @InvocableVariable(required=true)
        public String chatKey = ''; //transcript id
    }

    public class Output{
        @InvocableVariable(required=true)
        public String caseNumber;
    }

    @InvocableMethod(label='Neuraflash - Create Case')
    public static List<Output> createCase(List<Input> input){
        List<Output> result = new List<Output>();
        String chatKey = input[0].chatKey;

        Case theCase = new Case();
        theCase.LCT_ChatKey__c = chatKey;
        theCase.Origin = ORIGIN;
        theCase.Status = STATUS;

        Group g = NF_Util.getGroup(OWNER);
        if(g != null){
            theCase.OwnerId = g.Id;
        }

        theCase.RecordTypeId = NF_Util.getRecordTypeId(TYPE);

        String firstUserInput = NF_Util.getFirstUserInput(chatKey);
        theCase.Subject = String.isNotEmpty(firstUserInput) ? firstUserInput : 'Chatbot Case';
        System.debug('>>NF_SalesforceCaseCreation subject='+theCase.Subject);


        ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatKey);
        System.debug('>>NF_SalesforceCaseCreation session='+session);
        if(session != null){
            System.debug('>>NF_SalesforceCaseCreation session.API_Errors__c='+session.API_Errors__c);
            if(String.isNotEmpty(session.API_Errors__c)){
                theCase.Chatbot_Comments__c = 'API Errors:\r\n'+session.API_Errors__c;
            }
            else if(String.isNotEmpty(session.Comments__c)) {
                theCase.Chatbot_Comments__c = session.Comments__c;
            }
        }

        try {
            insert theCase;
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
            System.debug('>>NF_SalesforceCaseCreation Exception: ' + e);
        }
        if(theCase.id != null){
            system.debug('>>>theCase.id '+theCase.id );
            //serviceRequestCreation(chatKey);
        }

        LiveChatTranscript transcript = NF_Util.getLiveChatTranscript(chatKey);
        if(transcript != null){
            associateTranscriptWithCase(transcript, theCase.Id);
        }

        try{
            Case gotCase = [SELECT CaseNumber
            FROM Case
            WHERE Id = :theCase.Id];

            Output out = new Output();
            result.add(out);
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
            System.debug('Exception: ' + e);
            Output out = new Output();
            out.caseNumber = 'an error has occurred';
            result.add(out);
        }

        return result;
    }

    public static boolean associateTranscriptWithCase(LiveChatTranscript transcript, String caseId) {
        System.debug('>>NF_SalesforceCaseCreation associateTranscriptWithCase');

        boolean result = false;

        if (transcript != null && String.isNotEmpty(caseId)) {
            transcript.caseId = caseId;

            try {
                update transcript;
                result = true;
            }
            catch (Exception e) {
                NF_EinsteinBotLogger.populatedLog(e);
                System.debug('Exception encountered while updating transcript: ' + e);
                return false;
            }
        }

        return result;
    }
}