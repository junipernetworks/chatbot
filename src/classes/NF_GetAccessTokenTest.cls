/**
 * Created by mcasella on 2019-11-27.
 */

@isTest
public class NF_GetAccessTokenTest {
    @IsTest
    static void testSuccess() {
        Test.startTest();

        String response = '{"access_token":"568235ee1cc23700010000011c03ebe2b49943349f1fcf349b5f913f","expires_in":3600,"token_type":"bearer"}';

        NF_GetAccessTokenResponse apiResponse = (NF_GetAccessTokenResponse)JSON.deserialize(response, NF_GetAccessTokenResponse.class);

        System.assertEquals(false, apiResponse.isError());
        System.assertEquals('', apiResponse.getDescription());

        Test.stopTest();
    }

    @IsTest
    static void testFailure() {
        Test.startTest();

        String errorDescription = 'The authorization grant type is not supported by the authorization server.';
        String response = '{"error":"unsupported_grant_type","error_description":"'+errorDescription+'"}';

        NF_GetAccessTokenResponse apiResponse = (NF_GetAccessTokenResponse)JSON.deserialize(response, NF_GetAccessTokenResponse.class);

        System.assertEquals(true, apiResponse.isError());
        System.assertEquals(errorDescription, apiResponse.getDescription());

        Test.stopTest();
    }


    public class FailureResponse implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('');
            res.setStatusCode(200);
            return res;
        }
    }
}