/**
 * @File Name          : NF_CreateServiceRequestQueueable.cls
 * @Description        : This class is a queeable class to call the Create Service Request async.(due to einstein bot timeout error)
 * @Author             : Manishi Kalra
 * @Created By         : NeuraFlash, LLC
**/
public class NF_CreateServiceRequestQueueable implements Queueable,Database.AllowsCallouts {
    public String accountId, email, firstUserInput, caseNumber, chatkey, saltedId;
    
    public NF_CreateServiceRequestQueueable (String accountId, String emailId, String firstUserInput, String caseNumber, String chatkey, String saltedId) {
        this.accountId = accountId;
        this.email = emailId;
        this.firstUserInput = firstUserInput;
        this.caseNumber = caseNumber;
        this.chatkey = chatkey;
        this.saltedId = saltedId;
    }
    public void execute(QueueableContext qc){
        NF_ApiController apiController = new NF_ApiController();
        apiController.initialize(chatKey);
        apiController.email = email;
        apiController.saltedId = saltedId;
        
        NF_CreateServiceRequestRequest apiRequest = new NF_CreateServiceRequestRequest(chatKey);
        apiRequest.createSRRequest.contact.contactEmail = email;
        apiRequest.createSRRequest.contact.accountID = accountId;
        apiRequest.createSRRequest.caseInformation.synopsis = firstUserInput;
        apiRequest.createSRRequest.caseInformation.problemDescription = firstUserInput;
        apiRequest.createSRRequest.caseInformation.customerCaseNumber = caseNumber;
        
        System.debug('>>NF_CreateServiceRequest apiRequest=' + apiRequest);
        
        NF_CreateServiceRequestResponse apiResponse = (NF_CreateServiceRequestResponse) apiController.sendApiRequest(apiRequest, NF_CreateServiceRequestResponse.class, true);
        System.debug('>>NF_CreateServiceRequest response=' + apiResponse);
        
        if (apiResponse != null) {
            //out.response = JSON.serialize(apiResponse.createSRResponse);
            System.debug('>>NF_CreateServiceRequest out.response=' + JSON.serialize(apiResponse.createSRResponse));
            
            if (apiResponse.isError() == false) {
                ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatKey);
                if(session != null){
                    session.Created_SAP_Service_Request__c = true;
                    update session;
                }
            }
            else {
                //call API Log
                String error = 'Create Service Request: Errorneous Response with case# '+caseNumber;
                NF_ApiController.addApiError(chatkey, error);
                //throw new  NF_ApiController.ConfigurationException(error);
            }
        }
        else {
            //call API Log
            String error = 'Create Service Request: Response Not Found with case# '+caseNumber;
            NF_ApiController.addApiError(chatkey, error);
            //throw new  NF_ApiController.ConfigurationException(error);
        }
    }
}