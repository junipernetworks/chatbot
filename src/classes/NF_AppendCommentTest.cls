/**
 * Created by joaquintalon on 12/10/19.
 */

@IsTest
private class NF_AppendCommentTest {
    @IsTest
    static void test1() {
        NF_UTilTest.insertBotSession('abc');

        NF_AppendComment.Input input = new NF_AppendComment.Input();
        input.chatKey = 'abc';
        input.comment = 'test';

        NF_AppendComment.appendComment(new List<NF_AppendComment.Input>{input});
    }
}