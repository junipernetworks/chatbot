/**
 * Created by NeuraFlash LLC on 10/18/19.
 */

public with sharing class NF_ResetPassword{
    public static String ENDPOINT = 'callout:Juniper_API_Gateway/v01/resetpassword';

    public class Input{
        @InvocableVariable(required=true)
        public String chatKey;

        @InvocableVariable(required=false)
        public String email;
    }
    public class Output{
        @InvocableVariable
        public String response;

        @invocableVariable
        public Integer returnCode;
    }

    @InvocableMethod(label='Neuraflash - Call Reset Password')
    public static List<Output> resetPassword(List<Input> input) {
        Integer returnCode = nf_ApiController.API_RESULT.FAILURE;

        String chatkey = input[0].chatKey;
        String email   = input[0].email;

        List<Output> output = new List<Output>();
        Output out = new Output();

        try {
            LiveChatTranscript transcript = NF_Util.getLiveChatTranscript(chatKey);

            if (String.isEmpty(email)) {
                email = transcript.C2CEmail__c;
            }

            NF_ApiController apiController = new NF_ApiController();
            apiController.initialize(chatKey);

            NF_ResetPasswordRequest apiRequest = new NF_ResetPasswordRequest(chatKey, email);
            System.debug('>>NF_CreateServiceRequest apiRequest=' + apiRequest);

            NF_ResetPasswordResponse apiResponse = (NF_ResetPasswordResponse) apiController.sendApiRequest(apiRequest, NF_ResetPasswordResponse.class, true);
            System.debug('>>NF_CreateServiceRequest response=' + apiResponse);

            if (apiResponse != null) {
                out.response = JSON.serialize(apiResponse.resetPasswordResponse);
                System.debug('>>NF_CreateServiceRequest out.response=' + out.response);

                if (apiResponse.isError() == false) {
                    returnCode = nf_ApiController.API_RESULT.SUCCESS;
                }
            }
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
        }

        out.returnCode = returnCode;

        output.add(out);
        return output;
    }
}