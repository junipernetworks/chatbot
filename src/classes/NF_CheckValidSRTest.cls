/**
 * @File Name          : NF_CheckValidSRTest.cls
 * @Description        : This class is the test class for NF_CheckValidSR.cls
 * @Author             : Manishi Kalra
 * @Created By         : NeuraFlash, LLC
**/

@isTest
private class NF_CheckValidSRTest {

    /* 
    * method name: testValidSR
    * description: test method that will check the valid SR based on regex.
    */
    @IsTest
    static void testValidSR() {
        String id = '2019-0103-T-0150';
        List<NF_CheckValidSR.OutputSR> output = NF_CheckValidSR.NF_CheckValidSR(new List<String>{id});
        System.assertEquals(true, output[0].isValidCaseId);
    }
    /* 
    * method name: testValidSRwithoutT
    * description: test method that will check the valid SR based on regex.
    */
    @IsTest
    static void testValidSRwithoutT() {
        String id = '2019-0103-0150';
        List<NF_CheckValidSR.OutputSR> output = NF_CheckValidSR.NF_CheckValidSR(new List<String>{id});
        System.assertEquals(true, output[0].isValidCaseId);
    }
}