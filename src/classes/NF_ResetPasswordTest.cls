/**
 * Created by NeuraFlash LLC on 10/18/19.
 */

@IsTest
private class NF_ResetPasswordTest {

    @IsTest
    static void test1() {
        NF_UtilTest.insertApiSettings(true);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SuccessfulResponse());

        NF_ResetPassword.Input input = new NF_ResetPassword.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_ResetPassword.Output> output = NF_ResetPassword.resetPassword(new List<NF_ResetPassword.Input>{
                input
        });

        Test.stopTest();
    }

    @IsTest
    static void testResponse() {

        NF_ResetPasswordResponse response = new NF_ResetPasswordResponse();
        response.resetPasswordResponse = new NF_ResetPasswordResponse.ResetPasswordResponse();
        response.resetPasswordResponse.status = '';
        response.resetPasswordResponse.statusCode = '';
        response.resetPasswordResponse.message = '';
        response.resetPasswordResponse.customerSourceID = '';
        response.resetPasswordResponse.customerUniqueTransactionID = '';
        response.resetPasswordResponse.responseDateTime = '';

        response.isError();
}

    public class SuccessfulResponse implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"resetPasswordResponse":{"customerSourceID":"juni-chatbot","customerUniqueTransactionID":"1234abcd","responseDateTime":"2019-11-21T19:16:54.089Z","statusCode":"200","status":"Success","message":"Successfully processed the request"}}');
            res.setStatusCode(200);
            return res;
        }
    }
}