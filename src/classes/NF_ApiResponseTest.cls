/**
 * Created by mcasella on 2019-11-20.
 */

@isTest
public with sharing class NF_ApiResponseTest {

    public static void accessFaultFields(){
        NF_ApiResponse.Fault fault = new NF_ApiResponse.Fault();
        fault.errorMessage = '';
        fault.errorType = '';
        fault.errorClass = '';
        fault.errorCode = '';
    }

    @IsTest
    static void createResponses() {
        NF_GetAccessTokenResponse response = new NF_GetAccessTokenResponse();
        response.token_type   = 'type';
        response.expires_in   = '3600';
        response.access_token = 'abc';
        response.error = 'error';
        response.error_description = 'error description';
        response.isError();
        response.getDescription();
        response.getStatusCode();
        response.getErrorCode();

        accessFaultFields();

        NF_CreateServiceRequestResponse createSRResponse = new NF_CreateServiceRequestResponse();
        createSRResponse.createSRResponse = new NF_CreateServiceRequestResponse.CreateSRResponse();
        createSRResponse.createSRResponse.customerUniqueTransactionID = '';
        createSRResponse.createSRResponse.serviceRequestNumber = '';
        createSRResponse.createSRResponse.message = '';
        createSRResponse.createSRResponse.customerCaseNumber = '';
        createSRResponse.createSRResponse.statusCode = '';
        createSRResponse.createSRResponse.status = '';
        createSRResponse.createSRResponse.fault = null;

        createSRResponse.getDescription();
        createSRResponse.getStatusCode();

        createSRResponse.isError();
        createSRResponse.getErrorCode();

        NF_GetUserProfileResponse profileResponse = new NF_GetUserProfileResponse();
        profileResponse.getStatusCode();
        profileResponse.getDescription();

        NF_ResetPasswordResponse passwordResponse = new NF_ResetPasswordResponse();
        passwordResponse.getStatusCode();
        passwordResponse.getDescription();

    }
}