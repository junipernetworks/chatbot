/**
 * Created by mcasella on 2019-11-21.
 */

public with sharing class NF_ResendWelcomeLetterRequest extends NF_ApiRequest {
    public NF_ResendWelcomeLetterRequest(String chatKey){
        resendWelcomeLetterRequest = new ResendWelcomeLetterRequest(chatKey);
    }

    public override String getApiName(){
        return 'Resend Welcome Letter';
    }

    public override String getEndpoint(){
        return 'callout:Juniper_API_Gateway/v01/resendwelcomeletter';
    }

    public override String getTransactionId(){
        return resendWelcomeLetterRequest != null ? resendWelcomeLetterRequest.contractInformation.customerUniqueTransactionID : '';
    }

    public void setEmail(String email){
        this.resendWelcomeLetterRequest.contact.contactEmail = email;
    }

    public void setContactId( String contactId){
        this.resendWelcomeLetterRequest.contact.contactId = contactId;
    }

    public void setContractId(String contractId){
        if(String.isEmpty(contractId)){
            this.resendWelcomeLetterRequest.contractInformation.salesOrderNumber = '';
        }
        else{
            if(contractID.substring(0, 1) == '1'){
                this.resendWelcomeLetterRequest.contractInformation.salesOrderNumber = contractId;
            }else{
                this.resendWelcomeLetterRequest.contractInformation.contractId = contractId;
            }
        }
    }

    public ResendWelcomeLetterRequest resendWelcomeLetterRequest {get; set;}

    public class ResendWelcomeLetterRequest extends AbstractRequest{
        public ResendWelcomeLetterRequest(String chatKey){
            contact = new Contact();
            contractInformation = new ContractInformation();
            contractInformation.customerUniqueTransactionID = NF_Util.generateTransactionId(chatKey);
        }

        public Contact contact { get; set; }
        public ContractInformation contractInformation { get; set; }
    }

    public class ContractInformation {
        public String customerSourceID = 'juni-chatbot';
        public String customerUniqueTransactionID = '';
        public String contractId = '';
        public String salesOrderNumber = '';
    }
}