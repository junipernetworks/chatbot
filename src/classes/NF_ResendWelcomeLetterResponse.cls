/**
 * Created by mcasella on 2019-11-21.
 */

public with sharing class NF_ResendWelcomeLetterResponse extends NF_ApiResponse{
    public ResendWelcomeLetterResponse resendWelcomeLetterResponse;

    public class ResendWelcomeLetterResponse extends NF_ApiResponse.ApiResponse{
        public String responseDateTime { get; set; }
        public String customerCaseNumber { get; set; }
        public String customerUniqueTransactionID { get; set; }
        public String contractId { get; set; }
        public String salesOrderNumber { get; set; }
        public String customerSourceID { get; set; }
    }

    private static List<Integer> SUCCESS_CODES = new List<Integer>{ 808 };
    private static List<Integer> NOT_FOUND_CODES = new List<Integer>{ 809, 802 };

    //TODO: refactor
    public override Boolean isError(){
        //if the default response is empty, we have a failure,
        //it turns out we cannot trust the HTTP Status code for an error, for specific API handling, we should interrogate the errorCode
        Boolean isError = true;//|| resendWelcomeLetterResponse.isError();

        //otherwise, interrogate the error code
        if(resendWelcomeLetterResponse != null){
            if(SUCCESS_CODES.contains(resendWelcomeLetterResponse.getErrorCode()) || isNotFound()){
                isError = false;
            }
        }

        return isError;
    }

    //TODO: move this to API response super class
    public Boolean isNotFound(){
        return resendWelcomeLetterResponse != null && NOT_FOUND_CODES.contains(resendWelcomeLetterResponse.getErrorCode());
    }

    //TODO: we should implement a method that returns the response body object, so that in the super class we can just do all this logic
    public override Integer getStatusCode(){
        return resendWelcomeLetterResponse != null ? resendWelcomeLetterResponse.getStatusCode() : -1;
    }

    public override Integer getErrorCode(){
        return resendWelcomeLetterResponse != null ? resendWelcomeLetterResponse.getErrorCode() : -1;
    }

    public override String getDescription(){
        return resendWelcomeLetterResponse != null ? resendWelcomeLetterResponse.getDescription() : error;
    }
}