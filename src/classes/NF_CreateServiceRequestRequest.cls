/**
 * Created by mcasella on 2019-11-17.
 */

public with sharing class NF_CreateServiceRequestRequest extends NF_ApiRequest {
    public NF_CreateServiceRequestRequest(String chatKey){
        createSRRequest = new CreateSRRequest();
        createSRRequest.caseInformation = new CaseInformation();
        createSRRequest.caseInformation.customerUniqueTransactionID = NF_Util.generateTransactionId(chatKey);
        createSRRequest.contact = new Contact();
    }

    public override String getApiName(){
        return 'Create Admin Service Request';
    }

    public override String getEndpoint(){
        return 'callout:Juniper_API_Gateway/v01/createsr';
    }

    public override String getTransactionId(){
        return createSRRequest != null ? createSRRequest.caseInformation.customerUniqueTransactionID : '';
    }

    public CreateSRRequest createSRRequest;

    public class CreateSRRequest extends AbstractRequest{
        public CaseInformation caseInformation { get; set; }
        public Contact contact { get; set; }
    }

    public class CaseInformation{
        public String customerSourceID = 'juni-chatbot';
        public String customerUniqueTransactionID { get; set; }
        public String caseTypeCode = 'ADM';
        public String customerCaseNumber { get; set; }
        public String followUpMethod = 'Full';
        public String priority = 'P3 - Medium';
        public String adminSRType = 'Live Chat';
        public String adminSRSubType = 'Live Chat';
        public String synopsis { get; set; }
        public String problemDescription { get; set; }
    }
}