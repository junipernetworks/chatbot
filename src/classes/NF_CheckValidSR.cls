/**
 * @File Name          : NF_CheckValidSR.cls
 * @Description        : This class will check if Case ID is valid or not. (Valid format: XXXX-XXXX-XXXX/ XXXX-XXXX-T-XXXX)
 * @Author             : Manishi Kalra
 * @Created By         : NeuraFlash, LLC
**/
public with sharing class NF_CheckValidSR {

    @InvocableMethod(label='Neuraflash - Check for Valid SR')
    public static List<OutputSR> NF_CheckValidSR(List<String> inputSR) {

        List<OutputSR> outputSRList = new List<OutputSR>();
        OutputSR output = new OutputSR();
        output.isValidCaseId = false;

        if(!inputSR.isEmpty()) {
            //if T exists,remove T
            if(inputSR[0].contains('T')) {
                //remove all - & T
                String inputOnlyNumber = inputSR[0].remove('T');
                inputOnlyNumber = inputOnlyNumber.remove('-');
                //check length 12
                //if 12, manipulate with hyphen and T.
                //else error.
                System.debug('inputOnlyNumber IF>>>'+inputOnlyNumber);
                if(inputOnlyNumber.length() == 12) {  //format: xxxxxxxxxxxx 201901030150
                    //split 4 characters and add -.
                    String formattedSR = formatInputSR(inputOnlyNumber, true);
                    System.debug('formattedSR>>>>'+formattedSR);
                    output.isValidCaseId = true;
                    output.formattedCaseId = formattedSR;
                }
                else {
                    //error.
                    output.isValidCaseId = false;
                }
            }
            else {
                    //remove all -
                    String inputOnlyNumber = inputSR[0].remove('-');
                    //check length 12
                    //if 12, manipulate with hyphen and T.
                    //else error.
                    System.debug('inputOnlyNumber ELSE>>>'+inputOnlyNumber);
                    if(inputOnlyNumber.length() == 12) {  //format: xxxxxxxxxxxx
                        //split 4 characters and add -.
                        String formattedSR = formatInputSR(inputOnlyNumber, false);
                        System.debug('formattedSR ELSE>>>>'+formattedSR);
                        output.isValidCaseId = true;
                        output.formattedCaseId = formattedSR;
                    }
                    else {
                        //error.
                        output.isValidCaseId = false;
                    }
                }
            }
            System.debug('out>>>'+output.isValidCaseId + 'out Case>>'+output.formattedCaseId);
            outputSRList.add(output);
        return outputSRList;

    }

    private static String formatInputSR(String inputOnlyNumber, Boolean isTIncluded) {
        String formattedSR;
        if(isTIncluded) {
            formattedSR = inputOnlyNumber.substring(0,4) + '-' + inputOnlyNumber.substring(4,8) + '-T-' +inputOnlyNumber.substring(8,12);
        }
        else{
           formattedSR = inputOnlyNumber.substring(0,4) + '-' + inputOnlyNumber.substring(4,8) + '-' +inputOnlyNumber.substring(8,12); 
        }
        System.debug('formattedSR METHOD>>>>'+formattedSR);
        return formattedSR; 
    }

    public class OutputSR {
        @InvocableVariable
        public String formattedCaseId;

        @InvocableVariable
        public Boolean isValidCaseId;
    }
    /*private static Boolean checkForValidSR(String inputSR) {
        String userInput = inputSR;
        //Pattern validRegex = Pattern.compile('^[0-9]{4}-[0-9]{4}-[0-9]{4}$');
        //below regex is for testing.
        Pattern validRegex = Pattern.compile('^[0-9]{4}-[0-9]{4}-T-[0-9]{4}$|^[0-9]{4}-[0-9]{4}-[0-9]{4}$');
        Matcher validMatcher = validRegex.matcher(userInput);
        System.debug('checkForValidSR>>>'+ validMatcher.matches());
        return validMatcher.matches();
    }*/
}