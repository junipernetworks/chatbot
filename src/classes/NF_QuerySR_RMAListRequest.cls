public with sharing class NF_QuerySR_RMAListRequest extends NF_ApiRequest {
    public NF_QuerySR_RMAListRequest(String chatKey){
        querySRRMAListDetailsRequest = new QuerySR_RMAListRequest();
        querySRRMAListDetailsRequest.caseInformation = new CaseInformation();
        querySRRMAListDetailsRequest.caseInformation.customerUniqueTransactionID = NF_Util.generateTransactionId(chatKey);
        querySRRMAListDetailsRequest.contact = new Contact();
    }

    public void setContactId(String id){
        querySRRMAListDetailsRequest.contact.contactId = id;
    }

    public void setEmail(String email){
        querySRRMAListDetailsRequest.contact.contactEmail = email;
    }

    public void setAccountId(String id){
        querySRRMAListDetailsRequest.contact.accountID = id;
    }

    public override String getApiName(){
        return 'Query SR/RMAList';
    }

    public override String getEndpoint(){
        return 'callout:Juniper_API_Gateway/v01/findrecentcases';
    }

    public override String getTransactionId(){
        return querySRRMAListDetailsRequest != null ? querySRRMAListDetailsRequest.caseInformation.customerUniqueTransactionID : '';
    }

    public QuerySR_RMAListRequest querySRRMAListDetailsRequest;

    public class QuerySR_RMAListRequest extends AbstractRequest{
        public CaseInformation caseInformation { get; set; }
        public Boolean srFlag { get; set; }
        public Boolean rmaFlag { get; set; }
        public Contact contact { get; set; }
    }

    public class CaseInformation{
        public String customerSourceID = 'juni-chatbot';
        public String customerUniqueTransactionID { get; set; }
        public String customerCaseNumber { get; set; }
        public String serviceRequestNumber { get; set; }
    }
}