/**
 * Created by NeuraFlash LLC on 11/11/19.
 */

@IsTest
private class NF_UpdateSessionTest {
    @IsTest
    static void test1() {
        String chatKey = '123';
        String value = 'No Response';

        Test.startTest();

        NF_UtilTest.insertBotSession(chatKey);

        NF_UpdateSession.Input input = new NF_UpdateSession.Input();
        input.chatKey = chatKey;
        input.sessionFieldApiName = 'Visitor_Feedback_Result__c';
        input.sessionValue = value;
        NF_UpdateSession.updateSession(new List<NF_UpdateSession.Input>{input});
        Test.stopTest();

        ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatKey);

        System.assertEquals(value, session.Visitor_Feedback_Result__c);
    }
}