/**
 * Created by mcasella on 2019-11-19.
 */

public with sharing class NF_ApiController {
    public static Bot_API_Settings__c settings;
    private static String API_SETTINGS_NAME = 'JuniBot CCare';

    public static String appId = '';
    public static String clientId = '';
    public static String clientSecret = '';
    public static Integer timeout = 0;
    public static Boolean enableApiLogging = false;

    public static Long tokenExpiry = 0;

    public static String authorization = '';
    private static String accessToken = '';
    private static String authenticationTokenType = '';

    private String chatKey = '';
    public String saltedId = '';
    public String email = '';
    private Long expiresIn = 0;
    private Boolean updateAccess = false;

    public List<ToReplace> toReplaces = new List<ToReplace>();

    private static List<NF_ApiLog> apiLogs = new List<NF_ApiLog>();

    public class API_RESULT_PSEUDO_ENUM {
        public Integer SUCCESS = 0;
        public Integer FAILURE = -1;
        public Integer NOT_FOUND = 1;
    }

    public static API_RESULT_PSEUDO_ENUM API_RESULT = new API_RESULT_PSEUDO_ENUM();

    public class HttpException extends Exception{}
    public class ConfigurationException extends Exception{}
    public class ApiException extends Exception{}

    public void initialize(String chatKey){
        this.chatKey = chatKey;

        getApiSettings();
        System.debug('>>NF_ApiController.initialize.initialize: API settings=' + settings);

        getAuthorization();
        System.debug('>>NF_ApiController.initialize: authorization=' + authorization);

        getSaltedId(chatKey);
        System.debug('>>NF_ApiController.initialize: saltedId=' + saltedId);
    }

    public NF_ApiResponse sendApiRequest(NF_ApiRequest apiRequest, Type responseType, Boolean updateDml){
        HttpResponse httpResponse;
        NF_ApiResponse apiResponse;
        NF_ApiLog apiLog = new NF_ApiLog(apiRequest);
        apiLog.chatKey = chatKey;
        apiLog.timeout = timeout;

        System.debug('>>NF_ApiController.sendApiRequest>>');
        System.debug('>>NF_ApiController ' + responseType + ' START>>');
        HttpRequest httpRequest = createHttpRequest(apiRequest);
        System.debug('>>NF_ApiController httpRequest.body=' + httpRequest.getBody());

        try {
            Http http = new Http();
            httpResponse = http.send(httpRequest);
            System.debug('>>NF_ApiController httpResponse='+httpResponse);
            System.debug('>>NF_ApiController response body=' + httpResponse.getBody());

            if(httpResponse == null || httpResponse.getBody() == null){// || httpResponse.getStatusCode() != 200){
                System.debug('httpResponse is null, throwing error');
                throw new HttpException('API Response is null');
            }

            if(httpResponse != null && httpResponse.getBody() != null){
                String jsonBody = httpResponse.getbody();
                for (ToReplace replaceUnit : toReplaces) {
                    jsonBody = jsonBody.replace('"'+replaceUnit.replaced+'" :', '"'+replaceUnit.replacedWith+'" :');
                }

                apiResponse = (NF_ApiResponse) JSON.deserialize(jsonBody, responseType);

                System.debug(responseType + ' apiResponse is: ' + apiResponse);

                if(apiResponse.isError()){
                    throw new ApiException('Error returned from API');
                }
            }
        }
        catch(Exception e){
            apiLog.setException(e);
            System.debug('NF_ApiController: Exception thrown=' + e);

            if(updateDml){
                logError(apiRequest, apiResponse, e);
            }
        }

        apiLog.setResponse(apiResponse);

        apiLogs.add(apiLog);

        //only allow for insertion of records if updateDml flag is true (if all other API calls have finished)
        if(updateDml){
            NF_Util.incrementApiRequestIndex(chatKey);

            if(updateAccess){
                updateToken(authorization, expiresIn);
            }

            if(enableApiLogging){
                for(NF_ApiLog log : apiLogs){
                    NF_LogAPICalls.logApiCall(log);
                }
            }
        }

        System.debug('>>NF_ApiController ' + responseType + ' END>>');
        return apiResponse;
    }

    public class ToReplace{
        public String replaced { get; set; }
        public String replacedWith { get; set; }
    }

    public static void updateToken(String newToken, Long expiry){
        try{
            settings = [SELECT Access_Token__c, Token_Expiry__c
            FROM Bot_API_Settings__c
            WHERE Name =: API_SETTINGS_NAME
            LIMIT 1];
        }
        catch(Exception e){
            NF_EinsteinBotLogger.populatedLog(e);
        }
        if(settings == null){
            system.debug('Throw Exception');
            throw new ConfigurationException('Unable to load JuniBot CCare API Settings');
        }

        Long newExpiry = Datetime.now().getTime() + (Long) (expiry * 0.9);

        settings.Token_Expiry__c = newExpiry;
        settings.Access_Token__c = newToken;

        update settings;
    }

    public static Bot_API_Settings__c getApiSettings(){
        if(settings == null){
            try{
                settings = [SELECT App_Id__c, Client_Id__c, Client_Secret__c, Callout_Timeout__c, Access_Token__c, Token_Expiry__c, Enable_API_Logging__c
                FROM Bot_API_Settings__c
                WHERE Name = :API_SETTINGS_NAME
                LIMIT 1];
            }
            catch(Exception e){
                NF_EinsteinBotLogger.populatedLog(e);
            }

            if(settings == null || String.isEmpty(settings.App_Id__c) ||
                    String.isEmpty(settings.Client_Id__c) ||
                    String.isEmpty(settings.Client_Secret__c)){
                throw new ConfigurationException('Unable to load JuniBot CCare API Settings');
            }

            //REQUIRED
            appId = settings.App_Id__c;
            clientId = settings.Client_Id__c;
            clientSecret = settings.Client_Secret__c;
            tokenExpiry = (Long)(settings.Token_Expiry__c);
            enableApiLogging = settings.Enable_API_Logging__c;

            if(String.isNotEmpty(settings.Access_Token__c)){
                authorization = settings.Access_Token__c;
                //accessToken = settings.Access_Token__c;
                //authenticationTokenType = settings.Authentication_Token_Type__c;
            }

            //OPTIONAL
            if(settings.Callout_Timeout__c != null){
                timeout = Integer.valueOf(settings.Callout_Timeout__c);
            }
        }

        return settings;
    }

    private void getAuthorization(){
        if(String.isEmpty(authorization) || Datetime.now().getTime() >= tokenExpiry){
            NF_GetAccessTokenRequest accessTokenRequest = new NF_GetAccessTokenRequest();
            System.debug('>>NF_ApiController.initialize: accessTokenRequest=' + accessTokenRequest);

            NF_GetAccessTokenResponse tokenResponse = (NF_GetAccessTokenResponse) sendApiRequest(accessTokenRequest, NF_GetAccessTokenResponse.class, false);
            System.debug('>>NF_ApiController.initialize: tokenResponse right after: ' + tokenResponse);

            if(String.isNotEmpty(tokenResponse.getAuthorization()) && tokenResponse.getTokenExpiry() > 0) {
                System.debug('>>NF_ApiController.initialize: post-tokenResponse tokenResponse: ' + tokenResponse);

                authorization = tokenResponse.getAuthorization();
                System.debug('>>NF_ApiController.initialize: post-getAuthorization authorization: ' + authorization);

                expiresIn = tokenResponse.getTokenExpiry();
                System.debug('>>NF_ApiController.initialize: post-getTokenExpiry expiresIn: ' + expiresIn);

                updateAccess = true;
            }
        }
        else{
            System.debug('>>NF_ApiController found valid access_token');
        }

        if(String.isEmpty(authorization)){
            throw new ConfigurationException('Unable to get API Access Token');
        }
    }

    private void logError(NF_ApiRequest apiRequest, NF_ApiResponse apiResponse, Exception e){
        try {
            String errorMessage = createApiErrorMessage(apiRequest, apiResponse, e);

            if (String.isNotEmpty(chatKey) && String.isNotEmpty(errorMessage)) {
//                ebotdata__Bot_Event__e evt = new ebotdata__Bot_Event__e(
//                        ebotdata__Type__c = NF_EinsteinBotEventTriggerHandler.EVENT_ADD_API_ERROR,
//                        API_Errors__c = errorMessage,
//                        ebotdata__Live_Agent_Session_Id__c = chatKey);
//                EventBus.publish(evt);

                addApiError(chatKey, errorMessage);

            }

            //NF_EinsteinBotLogger.populatedLog(e);
        }
        catch(Exception logException){
            System.debug('>>NF_ApiController: Unable to log error: '+e.getMessage());
            //if we can't log the error, fail silently
        }
    }

    private static String createApiErrorMessage(NF_ApiRequest apiRequest, NF_ApiResponse apiResponse, Exception e){
        String message = '';

        message += apiRequest.getApiName() + ': ';

        String errorDescription = e.getMessage();
        if(apiResponse != null && String.isNotEmpty(apiResponse.getDescription())){
            errorDescription = apiResponse.getDescription();
        }

        message += ' ' + errorDescription + ' ';

        if(String.isNotEmpty(apiRequest.getTransactionId())){
            message += '(transaction id: '+apiRequest.getTransactionId()+')';
        }
        else{
            message += '(no transaction id)';
        }

        return message;
    }

    public static void addApiError(String chatKey, String apiError) {
        try {
            System.debug('>>NF_EinsteinBotEventTriggerHandler.addApiError: chatKey='+ chatKey);

            ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatKey);
            system.debug('Session API error-->'+session.API_Errors__c);
            if(session != null){
                if(session.API_Errors__c == 'null' || session.API_Errors__c == null){
                    session.API_Errors__c = '';
                }

                System.debug('>>NF_EinsteinBotEventTriggerHandler: apiError=['+ apiError + ']');
                session.API_Errors__c += apiError;

                //if(session.API_Errors__c.endsWith('\n') == false){
                session.API_Errors__c += '\r\n';
                //}

                update session;
            }
        }
        catch (Exception ex) {
            NF_EinsteinBotLogger.populatedLog(ex);
        }
    }


    private HttpRequest createHttpRequest(NF_ApiRequest apiRequest){
        HttpRequest httpRequest = new HttpRequest();
        String endpoint = apiRequest.getEndpoint();
        System.debug('>>NF_ApiController: endpoint=' + endpoint);
        httpRequest.setEndpoint(endpoint);
        System.debug('>>NF_ApiController: set timeout='+timeout);
        httpRequest.setTimeout(timeout);
        String method = apiRequest.getMethod();
        System.debug('>>NF_ApiController: method='+method);
        httpRequest.setMethod(method);

        if('POST' == method && apiRequest.getBody() != null){
            httpRequest.setBody(apiRequest.getBody());
        }

        Map<String, String> headers = apiRequest.getHeaders();
        System.debug('>>NF_ApiController: headers=' + headers);

        if(headers != null) {
            for (String key : headers.keySet()) {
                httpRequest.setHeader(key, headers.get(key));
            }
        }

        //Additional Secret header
        if(String.isNotBlank(saltedId)) {
            httpRequest.setHeader('Secret', saltedId);
            System.debug('>>NF_ApiController: set additional header for saltedId=' + saltedId);
        }

        if(String.isNotBlank(email)){
            httpRequest.setHeader('Email', email);
            System.debug('>>NF_ApiController: set additional header for email=' + email);
        }

        return httpRequest;
    }

    private void getSaltedId(String chatKey){
        LiveChatTranscript transcript = NF_Util.getLiveChatTranscript(chatKey);

        if(transcript != null){
            this.saltedId = transcript.Salted_ID__c;
            this.email = transcript.C2CEmail__c;
        }
    }
}