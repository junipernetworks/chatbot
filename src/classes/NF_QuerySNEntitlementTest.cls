/**
 * Created by mcasella on 2019-11-17.
 */

@isTest
public with sharing class NF_QuerySNEntitlementTest {
    @testSetup
    static void initialize() {
        NF_UtilTest.insertApiSettings(true);
    }

    @IsTest
    static void testSuccess() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SuccessResponse());

        NF_QuerySNEntitlement.Input input = new NF_QuerySNEntitlement.Input();
        input.chatKey = 'abc';
        input.email = 'juniperwayjason@gmail.com';
        input.serialNumber = '1539851713000';
        input.contactId = '0201123987';

        List<NF_QuerySNEntitlement.Output> output = NF_QuerySNEntitlement.querySNEntitlement(new List<NF_QuerySNEntitlement.Input>{input});

        System.debug('OUTPUT RESPONSE IS: ' + output[0].response);

        System.assertEquals(nf_ApiController.API_RESULT.SUCCESS, output[0].returnCode);

        Test.stopTest();
    }

    static void accessResponseFields(NF_QuerySNEntitlementResponse res){
        res.getSNEntitlementDetailsResponse.customerCaseNumber = '';
        res.getSNEntitlementDetailsResponse.customerSourceID = '';
        res.getSNEntitlementDetailsResponse.customerUniqueTransactionID = '';
        res.getSNEntitlementDetailsResponse.responseDateTime = '';
        res.getSNEntitlementDetailsResponse.salesOrderNumber = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails = new NF_QuerySNEntitlementResponse.SNEntitlementDetail();
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.contact = new NF_QuerySNEntitlementResponse.Contact();
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.contact.contactEmail = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.contact.contactId = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo = new List<NF_QuerySNEntitlementResponse.SSRNInfo>{new NF_QuerySNEntitlementResponse.SSRNInfo()};
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].eolProductReplacement = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].itemEndDateFilterEos = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].itemEndDateFilterEol = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].serviceContractStatus = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].serviceProduct = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].entitlementEndDate = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].entitlementStartDate = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].serviceContractNumber = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].ssrn = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].itemNumber = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].serviceRequestNumber = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].customerPONumber = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].customerSONumber = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].entitlementSerialNumber = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].entitlementType = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].equipmentId = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].equipmentStatus = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].itemStartDateFilterEol = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].itemStartDateFilterEos = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].legacyContractNo = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].lineItemSubStatus = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].materialId = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].parentMaterialId = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].parentSerialNumber = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].recordCount = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].serviceProductDescription = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].accountList = new List<NF_QuerySNEntitlementResponse.Account>{new NF_QuerySNEntitlementResponse.Account()};
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].accountList[0].street = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].accountList[0].accountName = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].accountList[0].ibEndCustomerFlag = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].accountList[0].commonAccountId = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].accountList[0].serviceBillToFlag = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].accountList[0].serviceDistributorFlag = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].accountList[0].serviceEndUserFlag = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].accountList[0].serviceInstalledAtFlag = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].accountList[0].serviceManagerResellerFlag = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].accountList[0].serviceShipToFlag = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList = new List<NF_QuerySNEntitlementResponse.Product>{new NF_QuerySNEntitlementResponse.Product()};
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].installedAtState = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].installedAtCountry = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].installedAtCity = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].warrantyEndDate = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].warrantyStartDate = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].registeredDate = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].product = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].ibaseStatus = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].productPurchaseOrderNumber = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].productSalesOrderNumber = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].serviceDeclineReason = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].serviceEligible = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].serviceStatus = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].shipDate = '';
        res.getSNEntitlementDetailsResponse.snEntitlementDetails.ssrnInfo[0].productList[0].warrantyStartDate = '';
    }

    @IsTest
    static void testSuccessResponse() {
        NF_QuerySNEntitlementResponse response = new NF_QuerySNEntitlementResponse();
        //weirdness to get around test coverage
        JSON.serialize(response);
        response = (NF_QuerySNEntitlementResponse)JSON.deserialize(SUCCESS_RESPONSE, NF_QuerySNEntitlementResponse.class);

        //weirdness to get around test coverage
        NF_QuerySNEntitlementResponse cwa = new NF_QuerySNEntitlementResponse();
        cwa.getSNEntitlementDetailsResponse = new NF_QuerySNEntitlementResponse.QuerySNEntitlementResponse();


        accessResponseFields(cwa);

        System.assertEquals(false, response.isError());
        //System.assertEquals('12345678', response.getSNEntitlementDetailsResponse.customerCaseNumber);
        System.assertEquals('Successfully processed the request', response.getDescription());
    }

    @IsTest
    static void testUnauthorized(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UnauthorizedResponse());

        NF_QuerySNEntitlement.Input input = new NF_QuerySNEntitlement.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_QuerySNEntitlement.Output> output = NF_QuerySNEntitlement.querySNEntitlement(new List<NF_QuerySNEntitlement.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testUnauthorizedResponse() {
        NF_QuerySNEntitlementResponse response = (NF_QuerySNEntitlementResponse)JSON.deserialize(UNAUTHORIZED_RESPONSE, NF_QuerySNEntitlementResponse.class);

        System.assertEquals(true, response.isError());
        System.assertEquals('Key not authorised', response.getDescription());
    }

    @IsTest
    static void testError(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ErrorResponse());

        NF_QuerySNEntitlement.Input input = new NF_QuerySNEntitlement.Input();
        input.chatKey = 'abc';
        input.email = 'test@neuraflash.com';

        List<NF_QuerySNEntitlement.Output> output = NF_QuerySNEntitlement.querySNEntitlement(new List<NF_QuerySNEntitlement.Input>{input});

        System.assertEquals(nf_ApiController.API_RESULT.FAILURE, output[0].returnCode);

        Test.stopTest();
    }

    @IsTest
    static void testErrorResponse() {
        NF_QuerySNEntitlementResponse response = (NF_QuerySNEntitlementResponse)JSON.deserialize(ERROR_RESPONSE, NF_QuerySNEntitlementResponse.class);

        System.assertEquals(true, response.isError());
        //System.assertEquals('\r\n[775] rmaNumber - R00002098 - is not associated with serviceRequestNumber - 2015-0811-T-0007', response.getDescription());
        System.assertEquals(400, response.getStatusCode());
    }

    private static String SUCCESS_RESPONSE = '{ "getSNEntitlementDetailsResponse": { "customerCaseNumber": null, "customerSourceID": null, "customerUniqueTransactionID": null, "salesOrderNumber": null, "contractId": null, "responseDateTime": "2019-12-12T21:06:07.196Z", "statusCode": "200", "status": "Success", "message": "Successfully processed the request", "snEntitlementDetails": { "contact": { "contactId": "201123987", "contactEmail": "juniperwayjason@gmail.com" }, "ssrnInfo": [ { "serviceRequestNumber": null, "entitlementStartDate": "20171023", "entitlementEndDate": "20201121", "entitlementType": "Service", "serviceProductDescription": "1G vSRX App Security 3Y", "lineItemSubStatus": "N/A", "serviceProduct": "VSRX-1G-ASEC-3", "serviceContractStatus": "Active", "entitlementSerialNumber": "SUB00022959138", "serviceContractNumber": "0065066236", "recordCount": "N/A", "itemNumber": "120", "ssrn": "1539851713000", "materialId": "VSRX-1G-ASEC-3", "equipmentId": "000000030001377621", "legacyContractNo": null, "itemStartDateFilterEol": "00000000", "itemStartDateFilterEos": "00000000", "itemEndDateFilterEol": "00000000", "itemEndDateFilterEos": "00000000", "parentSerialNumber": null, "parentMaterialId": null, "equipmentStatus": "Installed", "customerSONumber": "15398517", "customerPONumber": "PO1051299", "eolProductReplacement": null, "accountList": [ { "serviceBillToFlag": null, "serviceInstalledAtFlag": "Y", "serviceManagerResellerFlag": null, "serviceResellerFlag": null, "serviceShipToFlag": null, "accountName": "MILKY WAY MODULES", "commonAccountId": "0101352660", "serviceDistributorFlag": null, "serviceEndUserFlag": null, "ibEndCustomerFlag": null, "street": "BOEINGAVENUE 240" }, { "serviceBillToFlag": null, "serviceInstalledAtFlag": null, "serviceManagerResellerFlag": null, "serviceResellerFlag": null, "serviceShipToFlag": null, "accountName": "MILKY WAY MODULES", "commonAccountId": "0101352660", "serviceDistributorFlag": null, "serviceEndUserFlag": null, "ibEndCustomerFlag": "Y", "street": "BOEINGAVENUE 240" }, { "serviceBillToFlag": null, "serviceInstalledAtFlag": null, "serviceManagerResellerFlag": null, "serviceResellerFlag": null, "serviceShipToFlag": "Y", "accountName": "AT&T CORP", "commonAccountId": "0100530430", "serviceDistributorFlag": null, "serviceEndUserFlag": null, "ibEndCustomerFlag": null, "street": "10 S CANAL ST 17TH FL" }, { "serviceBillToFlag": "Y", "serviceInstalledAtFlag": null, "serviceManagerResellerFlag": null, "serviceResellerFlag": null, "serviceShipToFlag": null, "accountName": "AT&T CORP.", "commonAccountId": "0100496799", "serviceDistributorFlag": null, "serviceEndUserFlag": null, "ibEndCustomerFlag": null, "street": "PO BOX 66960" }, { "serviceBillToFlag": null, "serviceInstalledAtFlag": null, "serviceManagerResellerFlag": null, "serviceResellerFlag": null, "serviceShipToFlag": null, "accountName": "AT&T MANAGED SERVICE OPERATIONS", "commonAccountId": "0101271547", "serviceDistributorFlag": null, "serviceEndUserFlag": "Y", "ibEndCustomerFlag": null, "street": "200 LAUREL AVE" } ], "productList": [ { "registeredDate": "20171023", "product": "VSRX-1G-ASEC-3", "warrantyStartDate": "00000000", "warrantyEndDate": "00000000", "installedAtCity": "SCHIPHOL-RIJK", "installedAtState": "Noord-Holland", "installedAtCountry": "Netherlands", "shipDate": "20171023", "serviceStatus": "N", "ibaseStatus": "Installed", "serviceEligible": "No", "serviceDeclineReason": null, "productSalesOrderNumber": "15398517", "productPurchaseOrderNumber": null } ] } ] } } }';
    private static String ERROR_RESPONSE = '{ "getSNEntitlementDetailsResponse": { "customerCaseNumber": null, "customerSourceID": "juni-chatbot", "customerUniqueTransactionID": "1234", "serviceRequestNumber": "2015-0811-T-0007", "responseDateTime": "2019-11-20T03:07:13.283Z", "statusCode": "400", "status": "Error", "message": "Error in processing the request", "fault": [ { "errorClass": "Processing", "errorType": "Error", "errorCode": "775", "errorMessage": "rmaNumber - R00002098 - is not associated with serviceRequestNumber - 2015-0811-T-0007." } ] } }';
    private static String UNAUTHORIZED_RESPONSE = '{ "error": "Key not authorised" }';

    public class SuccessResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(200, SUCCESS_RESPONSE); }
    }

    public class UnauthorizedResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(403, UNAUTHORIZED_RESPONSE); }
    }

    public class ErrorResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){ return NF_UtilTest.getHttpResponse(500, ERROR_RESPONSE); }
    }
}