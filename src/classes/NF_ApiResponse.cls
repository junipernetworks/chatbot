/**
 * Created by mcasella on 2019-11-18.
 */

public with sharing abstract class NF_ApiResponse {
    public String error = '';

    public class Fault{
        public String errorCode{get;set;}
        public String errorType{get;set;}
        public String errorMessage{get;set;}
        public String errorClass{get;set;}
    }

    public abstract class ApiResponse{
        public String status { get; set; }
        public String statusCode { get; set; }
        public String message { get; set; }
        public Fault[] fault { get; set; }

        public Boolean isError(){
            return statusCode != '200';
        }

        public Integer getStatusCode(){
            return String.isNotEmpty(this.statusCode) ? Integer.valueOf(this.statusCode) : -1;
        }

        public Integer getErrorCode(){
            Integer errorCode = -1;

            try{
                System.debug('NF_ApiResponse.getErrorCode: fault=['+fault+']');
                if(fault != null && fault.size() > 0 && String.isNotEmpty(fault[0].errorCode)){
                    System.debug('NF_ApiResponse.getErrorCode: fault[0].errorCode=['+fault[0].errorCode+']');
                    errorCode = Integer.valueOf(fault[0].errorCode);
                }
                System.debug('NF_ApiResponse.getErrorCode: errorCode=['+errorCode+']');
            }
            catch(Exception e){ }

            return errorCode;
        }

        public String getDescription(){
            String errorDescription = message;

            if(fault != null && fault.size() > 0){
                errorDescription = '';
                for(Fault f : fault){
                    errorDescription += '\r\n[' + f.errorCode + '] ' + f.errorMessage;
                }
            }

            return errorDescription;
        }

    }

    public abstract Boolean isError();
    public abstract Integer getStatusCode();
    public abstract Integer getErrorCode();
    public abstract String  getDescription();
}