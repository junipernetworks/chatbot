/**
 * Created by NeuraFlash LLC on 11/5/19.
 */

@IsTest
private class NF_ApiRequestTest {
    @testSetup
    static void initialize() {
        NF_UtilTest.insertApiSettings(true);
    }

    @IsTest
    static void createRequests() {
        NF_GetAccessTokenRequest request = new NF_GetAccessTokenRequest();
        request.getApiName();
        request.getEndpoint();
        request.getMethod();
        request.getHeaders();
        NF_CreateServiceRequestRequest request2 = new NF_CreateServiceRequestRequest('');
        NF_GetUserProfileRequest request3 = new NF_GetUserProfileRequest('');
        request3.getApiName();
        request3.getEndpoint();
        request3.getMethod();

        System.assertEquals('Create Admin Service Request', request2.getApiName());
    }
}