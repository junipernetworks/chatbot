/**
 * Created by Jyotika Jain 12/09/19.
 */
public class NF_ExceptionEventTriggerHandler {
    public static void logExceptionRecords(NF_Exception_Event__e exceptionEvent){
        Einstein_Bot_Debug_Log__c debugLogs = new Einstein_Bot_Debug_Log__c();
        debugLogs.Error_Message__c =  exceptionEvent.Error_Message__c ;
        debugLogs.Exception_Data__c = exceptionEvent.Exception_Data__c;
        debugLogs.Stack_Trace__c = exceptionEvent.Stack_Trace__c;
        debugLogs.User_Name__c = exceptionEvent.User_Name__c;
        debugLogs.User_Id__c = exceptionEvent.User_Id__c;
        debugLogs.Type__c = exceptionEvent.Type__c;

        try{
            insert debugLogs;
        }
        catch (Exception ex){}
    }
}