/**
 * Created by NeuraFlash LLC on 12/09/19.
 */
@IsTest
public class NF_ExceptionEventTriggerHandlerTest {
    
    @IsTest
    public static void createExceptionEvent(){
        NF_Exception_Event__e exceptionEvent = new NF_Exception_Event__e();
        exceptionEvent.Error_Message__c = 'exception msg';
        exceptionEvent.Exception_Data__c = 'exception data';
        exceptionEvent.Stack_Trace__c = 'stack trace msg';
        exceptionEvent.User_Name__c = 'user name';
        exceptionEvent.User_Id__c = 'exception user id';
        exceptionEvent.Type__c = 'dml exception';
        Test.startTest();
        EventBus.publish(exceptionEvent);
        Test.stopTest();

        NF_ExceptionEventTriggerHandler.logExceptionRecords(exceptionEvent);
    }
}