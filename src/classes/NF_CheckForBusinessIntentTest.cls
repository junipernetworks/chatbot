/**
 * Created by mcasella on 2019-12-05.
 */

@isTest
public with sharing class NF_CheckForBusinessIntentTest {

    static testMethod void checkForBusinessIntent(){
        String intentName = 'bus_start';
        String chatKey = 'abc';

        NF_CheckForBusinessIntent.Input input = new NF_CheckForBusinessIntent.Input();
        input.chatKey = chatKey;
        List<NF_CheckForBusinessIntent.Input> inputs = new List<NF_CheckForBusinessIntent.Input>{input};

        Test.startTest();
        ebotdata__Bot_Session__c session = new ebotdata__Bot_Session__c();
        insert session;

        ebotdata__Bot_Chat_Log__c log = new ebotdata__Bot_Chat_Log__c();
        log.ebotdata__Bot_Session__c = session.id;
        log.ebotdata__Live_Agent_Session_Id__c = chatKey;
        log.ebotdata__Current_Dialog_Name__c = intentName;
        //log.ebotdata__Intent_Name__c = 'bus_resend_start';
        insert log;
        Test.stopTest();

        NF_CheckForBusinessIntent.Output output = NF_CheckForBusinessIntent.getIntents(inputs)[0];
        System.assertEquals(true, output.hasBusinessIntent);
        System.assertEquals(intentName, output.lastBusinessIntent);
    }

    static testMethod void checkForBusinessIntent2(){
        NF_CheckForBusinessIntent.Input input = new NF_CheckForBusinessIntent.Input();
        input.chatKey = '5700x000000kK17AAE';
        List<NF_CheckForBusinessIntent.Input> inputs = new List<NF_CheckForBusinessIntent.Input>{input};

        Test.startTest();
        ebotdata__Bot_Session__c session = new ebotdata__Bot_Session__c();
        insert session;

        ebotdata__Bot_Chat_Log__c log = new ebotdata__Bot_Chat_Log__c();
        log.ebotdata__Bot_Session__c = session.id;
        log.ebotdata__Live_Agent_Session_Id__c = input.chatKey;
        log.ebotdata__Live_Agent_Session_Id__c = 'asdf';
        log.ebotdata__Current_Dialog_Name__c = 'start';
        insert log;
        Test.stopTest();

        NF_CheckForBusinessIntent.getIntents(inputs);
    }
}