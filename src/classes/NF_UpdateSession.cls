/**
 * Created by NeuraFlash LLC on 11/11/19.
 */

public with sharing class NF_UpdateSession {
    public class Input{
        @InvocableVariable(required=true)
        public String chatKey;

        @InvocableVariable(required=true)
        public String sessionFieldApiName;

        @InvocableVariable(required=true)
        public String sessionValue;
    }

    @InvocableMethod(label='Neuraflash - Update Bot Session')
    public static void updateSession(List<Input> input){
        String chatKey = input[0].chatKey;
        String fieldApiName = input[0].sessionFieldApiName;
        String fieldValue = input[0].sessionValue;
        try{
            // sObject types to describe
//            String[] types = new String[]{ 'ebotdata__Bot_Session__c' };
//            // Make the describe call
//            Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
//            System.debug('Got describe information for ' + results.size() + ' sObjects.');
//
//            String fieldTypeName = Schema.getGlobalDescribe().get('ebotdata__Bot_Session__c').getDescribe().fields.getMap().get(fieldApiName).getDescribe().getType().name().toUpperCase();
//            System.debug('*** fieldTypeName = ' + fieldTypeName);
//
//            if(results.size() > 0){
//                System.debug('label='+results[0].getLabel());
//
//            }

            ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatKey);
            session.put(fieldApiName, fieldValue);
            update session;

            //TODO: make platform event
//            ebotdata__Bot_Session__c session = NF_Util.getBotSession(chatKey);
//            session.Visitor_Feedback_Intent__c = lbi;
//            session.Visitor_Feedback_Result__c = (positive) ? 'Yes' : 'No';
//            session.Visitor_Offered_Feedback__c = true;

            //update session;
        }catch(Exception e){
            System.debug('NF_UpdateSession.updateSession Exception encountered ' + e.getMessage() + e.getStackTraceString());
        }
    }
}