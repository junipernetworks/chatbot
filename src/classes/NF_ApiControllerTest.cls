/**
 * Created by NeuraFlash LLC on 11/4/19.
 */

@IsTest
private class NF_ApiControllerTest {
    @IsTest
    static void testExistingToken() {

        NF_UtilTest.insertApiSettings(true);

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new MockAccessTokenResponse());
        NF_ApiController controller = new NF_ApiController();
        controller.initialize('asdf');
        NF_ApiController.updateToken('newtoken', 1000);
        Test.stopTest();

    }

    @IsTest
    static void testGetNewToken() {

        NF_UtilTest.insertApiSettings(false);

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new MockAccessTokenResponse());
        NF_ApiController controller = new NF_ApiController();

        controller.initialize('asdf');

        Test.stopTest();

    }

    public class MockAccessTokenResponse implements HttpCalloutMock{
        public HTTPResponse respond(HTTPRequest req){
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"access_token":"sample_access_token", "expires_in": 3600, "token_type": "bearer"}');
            res.setStatusCode(200);
            return res;
        }
    }
}