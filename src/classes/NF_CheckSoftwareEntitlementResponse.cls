public with sharing class NF_CheckSoftwareEntitlementResponse extends NF_ApiResponse {

    public SWEntitlementCheckResponse swEntitlementCheckResponse{ get;set; }

    public class SWEntitlementCheckResponse extends ApiResponse{
        public String customerUniqueTransactionID { get; set; }
        public String responseDateTime { get; set; }
        public String customerCaseNumber { get; set; }
        public String customerSourceID { get; set; }
        public OSDetails[] osDetails { get; set; }
        public String ssrnOrserialnumber { get; set; }
    }
    public override Boolean isError(){
        return swEntitlementCheckResponse == null || swEntitlementCheckResponse.isError();
    }

    public override Integer getStatusCode(){
        return swEntitlementCheckResponse != null ? swEntitlementCheckResponse.getStatusCode() : -1;
    }

    public override Integer getErrorCode(){
        return swEntitlementCheckResponse != null ? swEntitlementCheckResponse.getErrorCode() : -1;
    }

    public override String getDescription(){
        return swEntitlementCheckResponse != null ? swEntitlementCheckResponse.getDescription() : error;
    }

    public class OSDetails {
        public String modelShortName { get; set; }
        public String osId { get; set; }
        public String osDisplayName { get; set; }
        public String srId { get; set; }
    }

}