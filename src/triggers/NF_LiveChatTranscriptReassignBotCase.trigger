/**
 * Created by mcasella on 2019-11-01.
 */

trigger NF_LiveChatTranscriptReassignBotCase on LiveChatTranscript (after update) {
    System.debug('>> NF_LiveChatTranscriptReassignBotCase entered');

    NF_ReassignBotCase.reassignCases(Trigger.newMap);
}