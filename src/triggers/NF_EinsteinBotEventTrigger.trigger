trigger NF_EinsteinBotEventTrigger on ebotdata__Bot_Event__e (after insert) {
    for(ebotdata__Bot_Event__e evt : Trigger.new) {

        if(String.isNotEmpty(evt.ebotdata__Live_Agent_Session_Id__c)){
            String chatKey = evt.ebotdata__Live_Agent_Session_Id__c;

//            if(evt.ebotdata__Type__c == NF_EinsteinBotEventTriggerHandler.EVENT_ADD_API_ERROR) {
//                if(String.isNotEmpty(evt.API_Errors__c)){
//                    NF_EinsteinBotEventTriggerHandler.addApiError(chatKey, evt.API_Errors__c);
//                }
//            }

            if(evt.ebotdata__Type__c == NF_EinsteinBotEventTriggerHandler.EVENT_UPDATE_BOT_SESSION) {
                if(String.isNotEmpty(evt.Bot_Name__c)){
                    System.debug('>>>Entered to update bot name'+evt.Bot_Name__c);
                    NF_EinsteinBotEventTriggerHandler.updateBotSessionFields(chatKey, evt.Bot_Name__c);
                }
            }
//            if(evt.ebotdata__Type__c == NF_EinsteinBotEventTriggerHandler.EVENT_ADD_PAGE_URL) {
//                    NF_EinsteinBotEventTriggerHandler.updatePageURL(chatKey);
//            }

//            else if(evt.ebotdata__Type__c == NF_EinsteinBotEventTriggerHandler.UPDATE_API_INDEX){
//                if(evt.API_Request_Index__c != null){
//                    NF_EinsteinBotEventTriggerHandler.updateApiIndex(chatKey, evt.API_Request_Index__c);
//                }
//            }
        }
    }
}